//
//  ItemEvaluacionViewController.swift
//  i-Safety
//
//  Created by usuario on 9/20/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol EvaluacionItemProtocol {
    func sendData(object:Array<CategoriaSbcItem>)
}

class ItemEvaluacionViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<CategoriaSbcItem> = []
    var dataFilter:Array<CategoriaSbcItem> = []
    var idCategoria:Int?
    var indexSelect:Int = 0
    var evaluacionProtocol:EvaluacionItemProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        let filterData = dataItem.filter { (item) -> Bool in
            item.idsbcCategoria == idCategoria
        }
        
        self.dataFilter = filterData
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    func setupNavigationBarItem(){
        let cancelarButton =  UIBarButtonItem(title: "Atras", style: .plain, target: self, action: #selector(cancelarAccion))
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false,animated:true)
        navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func cancelarAccion(){
        evaluacionProtocol?.sendData(object: dataItem)
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgEvaluacionDetalle"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! DetalleEvaluacionViewController
                destination.evaluacion = dataFilter[indexPath.row]
                destination.detalleEvaluacion = self
            }
        }
    }
}

//MARK: - TABLE CONFIG
extension ItemEvaluacionViewController :UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemEveluacionTableViewCell", for: indexPath) as! ItemEveluacionTableViewCell
        cell.txtTitulo.text = dataFilter[indexPath.row].descripcionItem
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgEvaluacionDetalle", sender: nil)
    }
    
}

extension ItemEvaluacionViewController : DetalleEvaluacionProtocol{
    func sendData(object: CategoriaSbcItem) {
        if let index = dataItem.firstIndex(where: { $0.idsbcCategoriaItems == object.idsbcCategoriaItems }) {
            return dataItem[index] = object
        }
    }
}


