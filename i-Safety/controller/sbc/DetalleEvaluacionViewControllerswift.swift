//
//  DetalleEvaluacionViewController.swift
//  i-Safety
//
//  Created by usuario on 9/22/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol DetalleEvaluacionProtocol {
    func sendData(object:CategoriaSbcItem)
}

class DetalleEvaluacionViewController: UIViewController {
    
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var btnTipoCategoria: ButtonWithImage!
    @IBOutlet weak var edtComentario: UITextView!
    
    
    var evaluacion:CategoriaSbcItem?
    var detalleEvaluacion:DetalleEvaluacionProtocol?
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataItem = UtilMetodos.obtenerBarrera()
        setupNavigationBarItem()
        setupButton(button: btnTipoCategoria)
     
        if let data = evaluacion{
            txtNombre.text = data.descripcionItem
            segmented.selectedSegmentIndex = data.seguro ? 0 : 1
            btnTipoCategoria.isEnabled =  data.seguro ? false : true
            edtComentario.isEditable = data.seguro ? false : true
            edtComentario.isHidden = data.seguro ? true : false
            btnTipoCategoria.isHidden = data.seguro ? true : false
            
            edtComentario.text = data.observacionSbcDetalle
             if let index = dataItem.firstIndex(where: { $0.id == data.idBarrera}) {
                let nombre = "\(dataItem[index].descripcion ?? "") - \(dataItem[index].nombre ?? "")"
                btnTipoCategoria.setTitle(nombre, for: .normal)
            }
            
            if(data.seguro){
                if #available(iOS 13.0, *) {
                    segmented.selectedSegmentTintColor = UIColor(cgColor: #colorLiteral(red: 0, green: 0.5215686275, blue: 0.02352941176, alpha: 1))
                } else {
                    segmented.tintColor = UIColor(cgColor: #colorLiteral(red: 0, green: 0.5215686275, blue: 0.02352941176, alpha: 1))
                }
            }else{
                if #available(iOS 13.0, *) {
                    segmented.selectedSegmentTintColor = UIColor(cgColor: #colorLiteral(red: 0.8039215686, green: 0.1333333333, blue: 0.02745098039, alpha: 1))
                } else {
                    segmented.tintColor = UIColor(cgColor: #colorLiteral(red: 0.8039215686, green: 0.1333333333, blue: 0.02745098039, alpha: 1))
                }
            }
        }else{
            segmented.selectedSegmentIndex = 1
            btnTipoCategoria.isEnabled =  true
            edtComentario.isEditable = true
            edtComentario.isHidden = false
            btnTipoCategoria.isHidden = false
        }
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    
    @IBAction func tapSegmented(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
              switch (index) {
              case 0:
                evaluacion?.seguro = true
                evaluacion?.riesgoso = false
                btnTipoCategoria.isEnabled =  false
                edtComentario.isEditable = false
                edtComentario.isHidden = true
                btnTipoCategoria.isHidden = true
                if #available(iOS 13.0, *) {
                    sender.selectedSegmentTintColor = UIColor(cgColor: #colorLiteral(red: 0, green: 0.5215686275, blue: 0.02352941176, alpha: 1))
                } else {
                    sender.tintColor = UIColor(cgColor: #colorLiteral(red: 0, green: 0.5215686275, blue: 0.02352941176, alpha: 1))
                }
                break
              case 1:
                evaluacion?.riesgoso = true
                evaluacion?.seguro = false
                btnTipoCategoria.isEnabled =  true
                edtComentario.isEditable = true
                edtComentario.isHidden = false
                btnTipoCategoria.isHidden = false
                if #available(iOS 13.0, *) {
                    sender.selectedSegmentTintColor = UIColor(cgColor: #colorLiteral(red: 0.8039215686, green: 0.1333333333, blue: 0.02745098039, alpha: 1))
                } else {
                    sender.tintColor = UIColor(cgColor: #colorLiteral(red: 0.8039215686, green: 0.1333333333, blue: 0.02745098039, alpha: 1))
                }
              default:
                  print("Default")
              }
    }
    
    func setupNavigationBarItem(){
           title = "Evaluación"
           let guardarButton =  UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(guardarAccion))
           navigationController?.navigationBar.isTranslucent = false
           navigationController?.setNavigationBarHidden(false,animated:true)
           navigationItem.rightBarButtonItem = guardarButton
       }
       
       @objc func guardarAccion(){
            guard let data = evaluacion else{
                return
            }
            data.observacionSbcDetalle = edtComentario.text
            detalleEvaluacion?.sendData(object: data)
            navigationController?.popViewController(animated: true)
       }
       
       @objc func cancelarAccion(){
           navigationController?.popViewController(animated: true)
       }
    
    
    @IBAction func tapTipoCategoria(_ sender: ButtonWithImage) {
        self.buttonSelect = 8
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadGeneralSbcViewController") as! LoadGeneralSbcViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//MARK: - Implementacion de protocol lista spinner
extension DetalleEvaluacionViewController:LoadGeneralSBCSelect{
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 8:
            let nombre = "\(object.descripcion ?? "") - \(object.nombre ?? "")"
            btnTipoCategoria.setTitle(nombre, for: .normal)
            evaluacion?.idBarrera = object.id ?? 0
            break
        default:
            print("Algo salio mal")
        }
    }
}
