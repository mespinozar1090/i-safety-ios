//
//  GeneralSBCViewController.swift
//  i-Safety
//
//  Created by usuario on 7/8/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol GeneralSBCProtocol {
    func sendDataGeneral(object:InsertSBC)
}

class GeneralSBCViewController: UIViewController {
    
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var btnSedeProyectoO: ButtonWithImage!
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var btnExperienciaO: ButtonWithImage!
    @IBOutlet weak var btnEspecialidadO: ButtonWithImage!
    @IBOutlet weak var btnAreaTrabajo: ButtonWithImage!
    @IBOutlet weak var btnSedeProyecto: ButtonWithImage!
    @IBOutlet weak var txtActividadO: UITextField!
    @IBOutlet weak var btnHorarioO: ButtonWithImage!
    @IBOutlet weak var btnEmpresaO: ButtonWithImage!
    
    let datePicker = UIDatePicker()
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    var objectGeneral = InsertSBC()
    var delegate:GeneralSBCProtocol?
    
    //
    
    //DETALLE
    var datosGenerales:InsertSBC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton(button: btnSedeProyectoO)
        setupButton(button: btnExperienciaO)
        setupButton(button: btnEspecialidadO)
        setupButton(button: btnAreaTrabajo)
        setupButton(button: btnSedeProyecto)
        setupButton(button: btnHorarioO)
        setupButton(button: btnEmpresaO)
        showDatePicker()
        
        txtNombre.text = UserDefaults.standard.value(forKey: "nombreUsuario") as? String
        loadDetalle()
        
    }
    
    private func loadDetalle(){
        if let data = datosGenerales{
            objectGeneral = data
            txtActividadO.text = objectGeneral.actividad
            txtNombre.text = objectGeneral.nombreObservador
            txtFecha.text = objectGeneral.fechaRegistro
            loadSede()
            loadEmpresaObservador()
            loadHorarioO()
            loadTiempoExpe()
            loadEspecialidad()
            loadArea()
            loadSedeProyecto()
            
        }else{
            print("no tiene data")
        }
    }
    
    //MARK: - Load detelle
    private func loadSede(){
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.idSedeProyectoObservado }) {
                    self.btnSedeProyectoO.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadEmpresaObservador(){
        SbcViewModel.init(parent: self).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.idEmpresaObservador }) {
                    self.btnEmpresaO.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadHorarioO(){
        let data = UtilMetodos.loadHorario()
        if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.horarioObservacion }) {
            self.btnHorarioO.setTitle(data[index].nombre, for: .normal)
        }
    }
    
    private func loadTiempoExpe(){
        let data = UtilMetodos.loadTiempoExperiencia()
        if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.tiempoExpObservada }) {
            self.btnExperienciaO.setTitle(data[index].nombre, for: .normal)
        }
    }
    
    
    private func loadEspecialidad(){
        SbcViewModel.init(parent: self).obtenerEspecialidad(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.especialidadObservado }) {
                    self.btnEspecialidadO.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadArea(){
        SbcViewModel.init(parent: self).obtenerAreaInspeccion(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.idAreaTrabajo }) {
                    self.btnAreaTrabajo.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadSedeProyecto(){
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.datosGenerales?.idSedeProyecto }) {
                    self.btnSedeProyecto.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    @IBAction func tapSiguiente(_ sender: ButtonRadius) {
        objectGeneral.cargoObservador = "-"
        objectGeneral.idLugarTrabajo = "27"
        objectGeneral.descripcionAreaObservada = ""
        objectGeneral.actividad = txtActividadO.text
        objectGeneral.nombreObservador = txtNombre.text
        objectGeneral.fechaRegistro = txtFecha.text
        delegate?.sendDataGeneral(object: objectGeneral)
    }
    
    //MARK: - Configuracion
    func showDatePicker(){
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        txtFecha.inputAccessoryView = toolbar
        txtFecha.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtFecha.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    
    //MARK: - Consumo de servicios API
    @IBAction func mostrarSede(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 1
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    
    @IBAction func mostrarEmpresaO(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 2
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    
    @IBAction func mostrarHorarioO(_ sender: UIButton) {
        self.buttonSelect = 3
        self.dataItem = UtilMetodos.loadHorario()
        sendDataTable()
    }
    
    @IBAction func mostrarExperienciaO(_ sender: UIButton) {
        self.buttonSelect = 4
        self.dataItem = UtilMetodos.loadTiempoExperiencia()
        sendDataTable()
    }
    
    
    @IBAction func mostrarEspealidad(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerEspecialidad(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 5
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func mostrarArea(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerAreaInspeccion(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 6
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func mostrarSedeProyecto(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 7
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    func sendDataTable(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadGeneralSbcViewController") as! LoadGeneralSbcViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//MARK: - Implementacion de protocol lista spinner
extension GeneralSBCViewController:LoadGeneralSBCSelect{
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 1:
            btnSedeProyectoO.setTitle(object.nombre, for: .normal)
            objectGeneral.idSedeProyectoObservado = object.id
        case 2:
            btnEmpresaO.setTitle(object.nombre, for: .normal)
            objectGeneral.idEmpresaObservador = object.id
        case 3:
            btnHorarioO.setTitle(object.nombre, for: .normal)
            objectGeneral.horarioObservacion = object.id
        case 4:
            btnExperienciaO.setTitle(object.nombre, for: .normal)
            objectGeneral.tiempoExpObservada = object.id
        case 5:
            btnEspecialidadO.setTitle(object.nombre, for: .normal)
            objectGeneral.especialidadObservado = object.id
        case 6:
            btnAreaTrabajo.setTitle(object.nombre, for: .normal)
            objectGeneral.idAreaTrabajo = object.id
        case 7:
            btnSedeProyecto.setTitle(object.nombre, for: .normal)
            objectGeneral.idSedeProyecto = object.id
        default:
            print("Algo salio mal")
        }
    }
}
