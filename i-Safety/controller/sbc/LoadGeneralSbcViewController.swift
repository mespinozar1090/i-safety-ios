//
//  LoadGeneralSbcViewController.swift
//  i-Safety
//
//  Created by usuario on 7/9/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LoadGeneralSBCSelect {
    func sendData(object:ItemSpinnerModel)
}

class LoadGeneralSbcViewController: UIViewController {

    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    
    var items:Array<ItemSpinnerModel>?
    var itemSelect:LoadGeneralSBCSelect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataItem = items!
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
}

//MARK: - Extension Table Load
extension LoadGeneralSbcViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "LoadCrearSbcTableViewCell", for: indexPath) as! LoadCrearSbcTableViewCell
       
        if(dataItem[indexPath.row].descripcion == nil){
            cell.txtNombre.text = dataItem[indexPath.row].nombre
        }else{
            cell.txtNombre.text = "\(dataItem[indexPath.row].descripcion ?? "") - \(dataItem[indexPath.row].nombre ?? "")"
        }
        
       return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = ItemSpinnerModel()
        object.id = dataItem[indexPath.row].id
        object.nombre = dataItem[indexPath.row].nombre
        object.descripcion = dataItem[indexPath.row].descripcion
        itemSelect?.sendData(object: object)
       navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - volver a la vista anterior
// navigationController?.popViewController(animated: true)
//MARK: - volver a la vista raiz
// navigationController?.popToRootViewController(animated: true)
