//
//  EvaluacionSBCViewController.swift
//  i-Safety
//
//  Created by usuario on 7/9/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol EvaluacionSBCProtocol {
    func sendDataEvaluacion(lista:Array<CategoriaSbcItem>)
}

class EvaluacionSBCViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var delegate:EvaluacionSBCProtocol?
    var dataItem:Array<ItemSpinnerModel> = []
    var lstItemCategoria:Array<CategoriaSbcItem> = []
    var lstevaluacion:Array<CategoriaSbcItem>?
    var indexSelect:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obtenerCategoriaSBC()
        loadListaEvaluacion()
    }
    
    private func loadListaEvaluacion(){
        if let lista = lstevaluacion , lista.count > 0{
            lstItemCategoria = lista
        }else{

            self.obtenerCategoriaItemSBC()
        }

    }
    
    @IBAction func tapGuardarRegistro(_ sender: ButtonRadius) {
        delegate?.sendDataEvaluacion(lista: lstItemCategoria)
    }
    
    
    func obtenerCategoriaSBC(){
        SbcViewModel.init(parent: self).obtenerCategoriaSBC(callback: {(data,error) in
            if let lista = data {
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
    
    func obtenerCategoriaItemSBC(){
        SbcViewModel.init(parent: self).obtenerCategoriaItemSBC(callback: {(data,error) in
            if let lista = data {
                self.lstItemCategoria = lista
            }
        })
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgCategoriaItem"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! ItemEvaluacionViewController
                indexSelect = indexPath.row
                destination.idCategoria = dataItem[indexPath.row].id
                destination.dataItem = lstItemCategoria
                destination.evaluacionProtocol = self
            }
        }
    }
}

//MARK: - configuracion table
extension EvaluacionSBCViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EvaluacionSBCTableViewCell", for: indexPath) as! EvaluacionSBCTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgCategoriaItem", sender: self)
    }
}

extension EvaluacionSBCViewController :EvaluacionItemProtocol{
    func sendData(object: Array<CategoriaSbcItem>) {
        lstItemCategoria = object
    }
}
