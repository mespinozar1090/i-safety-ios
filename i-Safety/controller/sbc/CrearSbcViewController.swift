//
//  CrearSbcViewController.swift
//  i-Safety
//
//  Created by usuario on 7/8/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit


class CrearSbcViewController: UIViewController{
    
    @IBOutlet weak var controllerGeneral: UIView!
    @IBOutlet weak var controllerEvaluacion: UIView!
    @IBOutlet weak var segmented: UISegmentedControl!
    var notificar:Array<NotificarModel> = []
    var datosGenerales:InsertSBC?
   // var evaluaciones:Array<CategoriaSbcItem> = []
    var lstevaluacion:Array<CategoriaSbcItem>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Crear SBC"
        hideKeyboardWhenTappedAround()
        segmented.selectedSegmentIndex = 0
        controllerGeneral.alpha = 1
        controllerEvaluacion.alpha = 0
        setupNavigationBarItem()
        segmented.isEnabled = false
    }
    
    
    @IBAction func accionController(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            controllerGeneral.alpha = 1
            controllerEvaluacion.alpha = 0
            segmented.isEnabled = false
        case 1:
            controllerGeneral.alpha = 0
            controllerEvaluacion.alpha = 1
        default:
            print("Default")
        }
    }
    
    func setupNavigationBarItem(){
        
        let cancelarButton =  UIBarButtonItem(title: "cerrar", style: .plain, target: self, action: #selector(cancelarAccion))
        cancelarButton.tintColor = .white
        
        let correoImg = UIImage(named: "ico_email")
        let btnCorreo:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30, height: 20))
        btnCorreo.setImage(correoImg, for: .normal)
        btnCorreo.sizeToFit()
        btnCorreo.addTarget(self, action: #selector(addNotificador), for: UIControl.Event.touchUpInside)
        let notificarButton = UIBarButtonItem(customView: btnCorreo)
        
        self.navigationItem.rightBarButtonItem = notificarButton
        self.navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func addNotificador(){
        self.performSegue(withIdentifier: "sgNotificarSBC", sender: self)
    }
    
    @objc func cancelarAccion(){
      self.dismiss(animated: true, completion: nil)
    }
    
    func validarCampos() -> Bool{
        guard (datosGenerales?.idSedeProyectoObservado != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione sede/Proyecto de Observador", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let fecha = datosGenerales?.fechaRegistro, fecha != "" else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione una fecha", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.idEmpresaObservador != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione empresa observado", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.idEmpresaObservador != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione empresa observado", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.horarioObservacion != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione horario", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.tiempoExpObservada != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione tiempo experiencia", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.especialidadObservado != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione especialidad", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let actividad = datosGenerales?.actividad, actividad != "" else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione actividad de observado", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.idAreaTrabajo != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione area de trabajo", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (datosGenerales?.idSedeProyecto != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione sede/Proyecto de observado", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        return true
    }
    
    
    func crearRegistroSBC(listaEvaluacion:Array<CategoriaSbcItem>){
        if notificar.count > 0 {
            UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Guardando registro")
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            
            let filterNotificar = notificar.filter { (item) -> Bool in
                item.selectEmail == true
            }
            
            //NOTIFICACIONES
            var notificadores:Array<NSDictionary> = []
            for index in 0...filterNotificar.count-1 {
                let paramsNotificar:NSDictionary = [
                    "IdUser": (filterNotificar[index].idUsers ?? 0) as Int,
                    "Nombre": (filterNotificar[index].nombreUsuario ?? "")as String,
                    "Email": (filterNotificar[index].emailCorporativo ?? "") as String,
                ]
                notificadores.append(paramsNotificar)
            }
            
            //EVALUACIONES
            var evaluaciones:Array<NSDictionary> = []
            for index in 0...listaEvaluacion.count-1 {
                let paramsEvaluacion:NSDictionary = [
                    "Id_sbc_detalle": (listaEvaluacion[index].idSbcDetalle ?? 0) as Int,
                    "Id_sbc_Categoria": (listaEvaluacion[index].idsbcCategoria ?? 0 )as Int,
                    "Id_sbc_Categoria_Items": (listaEvaluacion[index].idsbcCategoriaItems ?? 0) as Int,
                    "Seguro": (listaEvaluacion[index].seguro ) as Bool,
                    "Riesgoso": (listaEvaluacion[index].riesgoso ) as Bool,
                    "Idbarrera": listaEvaluacion[index].idBarrera as Int,
                    "Observacion_sbc_detalle": (listaEvaluacion[index].observacionSbcDetalle ?? "") as String,
                    "Id_sbc": listaEvaluacion[index].idSbc ?? 0 as Int]
                evaluaciones.append(paramsEvaluacion)
            }
            
            let rolUsario:String = UserDefaults.standard.value(forKey: "Descripcion_Rol") as! String
            let nombreUsuario:String = UserDefaults.standard.value(forKey: "nombreUsuario") as! String
            let nombreFormato:String = "Inspección - \(rolUsario) - \(nombreUsuario) - \(formatter.string(from: Date()))";
            
            let params:NSDictionary = [
                 "Id_sbc": datosGenerales?.idSbc ?? 0 as Int,
                "Nombre_observador": (datosGenerales?.nombreObservador ?? "") as String ,
                "IdSede_Proyecto": (datosGenerales?.idSedeProyecto ?? 0) as Int,
                "Cargo_Observador": (datosGenerales?.cargoObservador ?? "") as String,
                "IdLugar_trabajo": Int(datosGenerales?.idLugarTrabajo ?? "0")! as Int,
                "Fecha_Registro_SBC": (datosGenerales?.fechaRegistro ?? "") as String,
                "Id_Empresa_Observadora": (datosGenerales?.idEmpresaObservador ?? 0) as Int,
                "Horario_Observacion": "\(datosGenerales?.horarioObservacion ?? 0)",
                "Tiempo_exp_observada": (datosGenerales?.tiempoExpObservada ?? 0) as Int,
                "Especialidad_Observado": (datosGenerales?.especialidadObservado ?? 0) as Int,
                "Actividad_Observada": (datosGenerales?.actividad ?? "") as String,
                "Id_Area_Trabajo": (datosGenerales?.idAreaTrabajo ?? 0) as Int,
                "Fecha_registro": formatter.string(from: Date()) as String,
                "Descripcion_Area_Observada": (datosGenerales?.descripcionAreaObservada ?? "") as String,
                "IdSede_Proyecto_Observado": (datosGenerales?.idSedeProyectoObservado ?? 0) as Int,
                "IdUsuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                "NombreFormato": nombreFormato,
            ]
            
        
            if (datosGenerales?.idSbc ?? 0 > 0){
                SbcViewModel.init(parent: self).editarRegistroSBS(params: params, notificar: notificadores, evaluacion: evaluaciones, callback: {(success,error) in
                    UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                    if let data = success, data == 1{
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                })
            }else{
                 print("Crear nuevo registro SBC")
                SbcViewModel.init(parent: self).crearRegistroSBC(params: params, notificar: notificadores, evaluacion: evaluaciones, callback: {(success,error) in
                    UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                    if let data = success, data == 1{
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
            
        }else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe seleccionar almenos 1 usuario a notificar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgNotificarSBC"){
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NotificarViewController
            destino.dataItemSelect = notificar
            destino.itemSelect = self
        }else if (segue.identifier == "sgContaineEvaluacion"){
            let destination = segue.destination as! EvaluacionSBCViewController
            destination.lstevaluacion = lstevaluacion
            destination.delegate = self
        }else if (segue.identifier == "sgContaineGeneral"){
            let destination = segue.destination as! GeneralSBCViewController
            destination.datosGenerales = datosGenerales
            destination.delegate = self
        }
    }
}

extension CrearSbcViewController : NotificarSelect{
    func sendData(lista: Array<NotificarModel>) {
        notificar = lista
    }
}

extension CrearSbcViewController : EvaluacionSBCProtocol{
    func sendDataEvaluacion(lista: Array<CategoriaSbcItem>) {
        crearRegistroSBC(listaEvaluacion: lista)
    }
}

extension CrearSbcViewController : GeneralSBCProtocol{
    func sendDataGeneral(object: InsertSBC) {
        datosGenerales = object
        if(validarCampos()){
            segmented.isEnabled = true
            segmented.selectedSegmentIndex = 1
            controllerGeneral.alpha = 0
            controllerEvaluacion.alpha = 1
        }
        
    }
}
