//
//  SbcViewController.swift
//  i-Safety
//
//  Created by usuario on 7/8/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class SbcViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<SbcModel> = []
    var evaluaciones:Array<CategoriaSbcItem> = []
    var datosGenerales = InsertSBC()
    var idRolUsario:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        idRolUsario = UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int
    }
    
    override func viewDidAppear(_ animated: Bool) {
        listarSBC()
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.topItem?.title = "SBC"
        let crearButton =  UIBarButtonItem(title: "crear", style: .plain, target: self, action: #selector(crearSbc))
        crearButton.tintColor = .white
        navigationItem.rightBarButtonItem = crearButton
    }
    
    @objc func crearSbc(){
        datosGenerales = InsertSBC()
        evaluaciones.removeAll()
        self.performSegue(withIdentifier: "sgCrearsbc", sender: self)
    }
    
    func listarSBC(){
        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                                   "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
                                   "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        SbcViewModel.init(parent: self).listarSBC(params: params,callback: {(data,error) in
            if let lista = data {
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
    
    func obtenerRegistroSBC(idSBC:Int){
        SbcViewModel.init(parent: self).obtenerRegistroSBC(idSBC: idSBC, callback: {(data,categoria,error) in
            if let result = data {
                self.datosGenerales = result
                if let evaluacion = categoria{
                    self.evaluaciones = evaluacion
                }
                self.performSegue(withIdentifier: "sgCrearsbc", sender: self)
            }
        })
    }
    
    private func showModalEliminar(index:IndexPath){
        
        let alert = UIAlertController(title: "Información", message: "¿Está seguro de eliminar el registro de SBC?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) -> Void in
            self.eliminarSBC(indexPath: index)
        })
        alert.addAction(ok)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func eliminarSBC(indexPath:IndexPath){
        let params:NSDictionary = ["idSBC": self.dataItem[indexPath.row].idSbc ?? 0]
        SbcViewModel.init(parent: self).eliminarSBC(params:params, callback: {(success,error)in
            if let result = success, result{
                self.dataItem.remove(at: indexPath.row)
                self.tblItem.beginUpdates()
                self.tblItem.deleteRows(at: [indexPath], with: .automatic)
                self.tblItem.endUpdates()
            }else{
                print("Ocurrio un error no elimino")
            }
        })
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgCrearsbc"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                // let destination = segue.destination as! CrearSbcViewController
                let nav = segue.destination as! UINavigationController
                let destination = nav.topViewController as! CrearSbcViewController
                
                destination.datosGenerales = datosGenerales
                destination.lstevaluacion = evaluaciones
            }
        }
    }
}


//MARK: - configuracion table
extension SbcViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sbcTableViewCell", for: indexPath) as! SbcTableViewCell
        cell.txtCodigo.text = dataItem[indexPath.row].codigoSBC
        cell.txtFormato.text = dataItem[indexPath.row].nombreFormato
        cell.txtObservacion.text =  dataItem[indexPath.row].nombreObservador
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        obtenerRegistroSBC(idSBC: dataItem[indexPath.row].idSbc!)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
           if(idRolUsario == 1){
                return .delete
           }
           return .none
       }
       
       func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
           if editingStyle == .delete {
               print("debe eliminar")
               showModalEliminar(index: indexPath)
           }
       }
}
