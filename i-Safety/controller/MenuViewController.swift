//
//  MenuViewController.swift
//  i-Safety
//
//  Created by usuario on 8/6/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol CerrarSessionProtocol {
    func closeApp(success:Bool)
}

class MenuViewController: UIViewController {

    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var txtEmpresa: UILabel!
    var delegate: CerrarSessionProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if ((UserDefaults.standard.value(forKey: "idUsers") as! Int) > 0){
            txtNombre.text = (UserDefaults.standard.value(forKey: "usuario") as! String)
            txtEmpresa.text = (UserDefaults.standard.value(forKey: "Descripcion_Rol") as! String)
        }
    
    }
    

    @IBAction func cerrarSesion(_ sender: UIButton) {
        let message:String = "¿Seguro de cerrar la sesión?"
        
        let alert = UIAlertController(title: "cerrar sesion", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Salir", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
            self.delegate?.closeApp(success: true)
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action) -> Void in
            print("Cancel")
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

}
