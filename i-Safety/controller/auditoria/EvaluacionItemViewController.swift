//
//  EvaluacionItemViewController.swift
//  i-Safety
//
//  Created by usuario on 9/29/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LineamientoSubItemProtocol {
    func sendData(lista:Array<LineamientoSubItem>)
}

class EvaluacionItemViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var idAuditItems:Int?
    var lineamientoSubItem:Array<LineamientoSubItem> = []
    
    var lstLineamientoSub:Array<LineamientoSubItem>?
    var dataFilter:Array<LineamientoSubItem> = []
    var delegate:LineamientoSubItemProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
      
        let filterData = lineamientoSubItem.filter { (item) -> Bool in
            item.idAuditItems == idAuditItems
             
        }
        print("filterData :",filterData.count)
        self.dataFilter = filterData
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    func setupNavigationBarItem(){
        let cancelarButton =  UIBarButtonItem(title: "Atras", style: .plain, target: self, action: #selector(cancelarAccion))
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false,animated:true)
        navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func cancelarAccion(){
        delegate?.sendData(lista: lineamientoSubItem)
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgLineamientoItem"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! DetalleItemLineamientoViewController
                destination.lineamiento = dataFilter[indexPath.row]
                destination.delegate = self
            }
        }
    }
    
}

extension EvaluacionItemViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EvaluacionItemTableViewCell", for: indexPath) as! EvaluacionItemTableViewCell
        cell.txtNombre.text = dataFilter[indexPath.row].descripcionItems
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgLineamientoItem", sender: nil)
    }
    
}
extension EvaluacionItemViewController:LineamientoItemProtocol{
    func senData(object: LineamientoSubItem) {
        if let index = lineamientoSubItem.firstIndex(where: { $0.idAuditLinSubItems == object.idAuditLinSubItems }) {
            return lineamientoSubItem[index] = object
        }
    }
    
}


