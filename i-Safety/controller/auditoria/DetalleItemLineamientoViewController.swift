//
//  DetalleItemLineamientoViewController.swift
//  i-Safety
//
//  Created by usuario on 9/29/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LineamientoItemProtocol {
    func senData(object:LineamientoSubItem)
}

class DetalleItemLineamientoViewController: UIViewController {
    
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var btnCalificacion: ButtonWithImage!
    @IBOutlet weak var btnLugar: ButtonWithImage!
    @IBOutlet weak var imgCaptura: UIImageView!
    @IBOutlet weak var tfComentario: UITextView!
    
    var imagePicker = UIImagePickerController()
    var lineamiento:LineamientoSubItem?
    var delegate:LineamientoItemProtocol?
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        hideKeyboardWhenTappedAround()
        imgCaptura.isHidden = true
        setupButton(button: btnCalificacion)
        setupButton(button: btnLugar)
        
        if let data = lineamiento{
            txtNombre.text = data.descripcionItems
            
            if let index = UtilMetodos.obtenerCalificacion().firstIndex(where: { $0.id == lineamiento?.idCalificacion }) {
                btnCalificacion.setTitle(UtilMetodos.obtenerCalificacion()[index].nombre, for: .normal)
            }
            
            if let index2 = UtilMetodos.obtenerLugar().firstIndex(where: { $0.id == lineamiento?.idLugar }) {
                btnLugar.setTitle(UtilMetodos.obtenerLugar()[index2].nombre, for: .normal)
            }
            
            tfComentario.text = data.mensage ?? ""
            if let imgBase64 = data.imgbase64,!imgBase64.isEmpty{
                imgCaptura.image = UtilMetodos.convertBase64StringToImage(imageBase64String: imgBase64)
                imgCaptura.isHidden = false
            }
        }
    }
    
    func setupNavigationBarItem(){
        let btnCapturar = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(capturarImagen))
        navigationItem.rightBarButtonItem = btnCapturar
    }
    
    private func setupButton(button:ButtonWithImage){
           button.backgroundColor = .clear
           button.layer.cornerRadius = 5
           button.layer.borderWidth = 1
           button.layer.borderColor = UIColor.gray.cgColor
       }
    
    @objc func capturarImagen(){
        takePhoto()
    }
    
    func takePhoto() {
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        //  presentingViewController(imagePicker,animte)
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func saveData(_ sender: ButtonWithImage) {
        
        guard (lineamiento!.idCalificacion != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione calificación", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return
        }
        
        if let idCalificacion = lineamiento?.idCalificacion,idCalificacion != 4 {
           
            guard (lineamiento!.idLugar != nil && lineamiento!.idLugar != 0) else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione lugar", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return
            }
        
            guard !tfComentario.text.isEmpty else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Ingresar Notas del Auditor", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return
            }
        }
        lineamiento?.mensage = tfComentario.text ?? ""
        delegate?.senData(object: lineamiento!)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapCalificacion(_ sender: ButtonWithImage) {
        self.buttonSelect = 3
        self.dataItem = UtilMetodos.obtenerCalificacion()
        sendDataTable()
    }
    
    @IBAction func tapLugar(_ sender: ButtonWithImage) {
        self.buttonSelect = 4
        self.dataItem = UtilMetodos.obtenerLugar()
        sendDataTable()
    }
    
    func sendDataTable(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadAuditoriaViewController") as! LoadAuditoriaViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension DetalleItemLineamientoViewController : LoadCrearAuditoriaSelect {
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 3:
            btnCalificacion.setTitle(object.nombre, for: .normal)
            lineamiento?.idCalificacion = object.id
            if(object.id == 4){
                btnLugar.setTitle("Seleccione", for: .normal)
                lineamiento!.idLugar = 0
                tfComentario.text = ""
            }
        case 4:
            btnLugar.setTitle(object.nombre, for: .normal)
            lineamiento?.idLugar = object.id
        default:
            print("Algo salio mal")
        }
    }
}

extension DetalleItemLineamientoViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[.originalImage] as? UIImage {
            let newImage = resizeImage(image: image, newWidth: 200)
            imgCaptura.image = newImage
          //  lineamiento?.imgbase64 = "data:image/jpeg;base64,\(UtilMetodos.convertImageToBase64(image: newImage!))"
            lineamiento?.imgbase64 = UtilMetodos.convertImageToBase64(image: newImage!)
            imgCaptura.isHidden = false
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
