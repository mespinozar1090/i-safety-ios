//
//  EvaluacionViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol EvaluacionAuditoriaProcotol {
    func sendData(lista:Array<LineamientoSubItem>)
}
class EvaluacionViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var delegate :EvaluacionAuditoriaProcotol?
    var dataItem:Array<ItemSpinnerModel> = []
    var lineamientoSubItem:Array<LineamientoSubItem> = []
    var lstLineamientoSub:Array<LineamientoSubItem>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obtenerLineaminetos()
        loadDetalleLineamiento()
    }
    
    private func loadDetalleLineamiento(){
        if let lista = lstLineamientoSub, lista.count > 0{
            print("entro a detalle",lista.count)
            lineamientoSubItem = lista
        }else{
             print("no tiene detalle")
            obtenerLineaminetosSub()
        }
    }
    
    func obtenerLineaminetos(){
        AuditoriaViewModel.init(parent: self).obtenerLineaminetos(callback: {(data,error)in
            if let lista = data{
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
    
    func obtenerLineaminetosSub(){
        AuditoriaViewModel.init(parent: self).obtenerLineaminetosSub(callback: {(data,error) in
            if let lista = data{
                self.lineamientoSubItem = lista
            }
        })
    }
    
    @IBAction func tapCrearAuditoria(_ sender: UIButton) {
        sender.isEnabled = false
        delegate?.sendData(lista: lineamientoSubItem)
        sender.isEnabled = true
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if(segue.identifier == "sgLineamientoSub"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! EvaluacionItemViewController
                destination.idAuditItems = dataItem[indexPath.row].id
                destination.lineamientoSubItem = lineamientoSubItem
                destination.delegate = self
            }
        }
    }
}

//MARK: - configuracion table
extension EvaluacionViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EvaluacionTableViewCell", for: indexPath) as! EvaluacionTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgLineamientoSub", sender: nil)
    }
}

extension EvaluacionViewController : LineamientoSubItemProtocol{
    func sendData(lista: Array<LineamientoSubItem>) {
        lineamientoSubItem = lista
    }
}
