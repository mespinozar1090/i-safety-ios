//
//  DatosGeneralesAudiViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol GeneralAuditoriaProcotol {
    func sendData(object:InsertAuditoriaModel)
}

class DatosGeneralesAudiViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnEmpresa: ButtonWithImage!
    @IBOutlet weak var btnSedeProyecto: ButtonWithImage!
    @IBOutlet weak var txtContrato: SDCTextField!
    @IBOutlet weak var txtSupervisor: SDCTextField!
    @IBOutlet weak var txtResponsable: SDCTextField!
    
    var delegate : GeneralAuditoriaProcotol?
    var objectGeneral = InsertAuditoriaModel()
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    
    //Detalle
    var datosGenerales:InsertAuditoriaModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtContrato.delegate = self
        txtContrato.maxLength = 20
        txtSupervisor.delegate = self
        txtSupervisor.maxLength = 20
        txtResponsable.delegate = self
        txtResponsable.maxLength = 20
        
        setupButton(button: btnEmpresa)
        setupButton(button: btnSedeProyecto)
        loadDetalle()
    }
    
    private func loadDetalle(){
        if let data = datosGenerales{
            objectGeneral = data
            txtContrato.text = objectGeneral.nroContrato
            txtSupervisor.text = objectGeneral.supervisorContrato
            txtResponsable.text = objectGeneral.responsableContratista
            loadEmpresa()
            loadSede()
        }
    }
    
    //MARK: - load detalle
    private func loadEmpresa(){
        SbcViewModel.init(parent: self).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.objectGeneral.idEmpresaContratista }) {
                    self.btnSedeProyecto.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadSede(){
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.objectGeneral.sede }) {
                    self.btnEmpresa.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func mostrarEmpresa(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 1
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func mostrarSede(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 2
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    func sendDataTable(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadAuditoriaViewController") as! LoadAuditoriaViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func tapSiguiente(_ sender: UIButton) {
        if(validarCampos()){
            objectGeneral.nroContrato = txtContrato.text
            objectGeneral.supervisorContrato = txtSupervisor.text
            objectGeneral.responsableContratista = txtResponsable.text
            delegate?.sendData(object: objectGeneral)
        }
    }
    
    func validarCampoRegex(enteredEmail:String) -> Bool {
        let emailFormat = ".*[^A-Za-z0-9/\\s].*"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func validarCampos() -> Bool{
        guard (objectGeneral.idEmpresaContratista != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione empresa contratista", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (objectGeneral.sede != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione sede/proyecto", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let contrato = txtContrato.text, !contrato.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo Número del contrato es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
//        guard !validarCampoRegex(enteredEmail: txtContrato.text ?? "") else {
//            let alert = UtilMetodos.getAlert(title: "Información", message: "No se permite caracteres especiales, en el campo Número de contrato", titleAction: "Aceptar")
//            self.present(alert,animated: true,completion: nil)
//            return false
//        }
        
        guard let supervisor = txtSupervisor.text, !supervisor.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo supervisor es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
//        guard !validarCampoRegex(enteredEmail: txtSupervisor.text ?? "") else {
//            let alert = UtilMetodos.getAlert(title: "Información", message: "No se permite caracteres especiales, en el campo Supervisor del contrato", titleAction: "Aceptar")
//            self.present(alert,animated: true,completion: nil)
//            return false
//        }
        
        guard let responsable = txtResponsable.text, !responsable.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo Responsable STSOMARS es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
//        guard !validarCampoRegex(enteredEmail: txtSupervisor.text ?? "") else {
//            let alert = UtilMetodos.getAlert(title: "Información", message: "No se permite caracteres especiales, en el campo Responsable STSOMARS", titleAction: "Aceptar")
//            self.present(alert,animated: true,completion: nil)
//            return false
//        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
}

extension DatosGeneralesAudiViewController : LoadCrearAuditoriaSelect {
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 1:
            btnEmpresa.setTitle(object.nombre, for: .normal)
            objectGeneral.idEmpresaContratista = object.id
        case 2:
            btnSedeProyecto.setTitle(object.nombre, for: .normal)
            objectGeneral.sede = object.id
            
        default:
            print("Algo salio mal")
        }
    }
}
