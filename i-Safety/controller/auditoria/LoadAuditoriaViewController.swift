//
//  LoadAuditoriaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LoadCrearAuditoriaSelect {
    func sendData(object:ItemSpinnerModel)
}

class LoadAuditoriaViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    
    var items:Array<ItemSpinnerModel>?
    var itemSelect:LoadCrearAuditoriaSelect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataItem = items!
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
}

//MARK: - Extension Table Load
extension LoadAuditoriaViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoadAuditoriaTableViewCell", for: indexPath) as! LoadAuditoriaTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre?.capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = ItemSpinnerModel()
        object.id = dataItem[indexPath.row].id
        object.nombre = dataItem[indexPath.row].nombre
        itemSelect?.sendData(object: object)
        navigationController?.popViewController(animated: true)
    }
    
}
