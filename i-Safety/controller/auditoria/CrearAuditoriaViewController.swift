//
//  CrearAuditoriaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit



class CrearAuditoriaViewController: UIViewController {
    
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var generalController: UIView!
    @IBOutlet weak var evaluacionController: UIView!
    var notificar:Array<NotificarModel> = []
    var datosGenerales:InsertAuditoriaModel?
    var lstLineamientoSub:Array<LineamientoSubItem>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupNavigationBarItem()
        segmented.selectedSegmentIndex = 0
        generalController.alpha = 1
        evaluacionController.alpha = 0
        segmented.isEnabled = false
    }
    
    func setupNavigationBarItem(){
        title = "Auditoria"
       let cancelarButton =  UIBarButtonItem(title: "cerrar", style: .plain, target: self, action: #selector(cancelarAccion))
               cancelarButton.tintColor = .white
               
               let correoImg = UIImage(named: "ico_email")
               let btnCorreo:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30, height: 20))
               btnCorreo.setImage(correoImg, for: .normal)
               btnCorreo.sizeToFit()
               btnCorreo.addTarget(self, action: #selector(addNotificador), for: UIControl.Event.touchUpInside)
               let notificarButton = UIBarButtonItem(customView: btnCorreo)
               
               self.navigationItem.rightBarButtonItem = notificarButton
               self.navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func addNotificador(){
        self.performSegue(withIdentifier: "sgNotificarAuditoria", sender: self)
    }
    
    @objc func cancelarAccion(){
      self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func accionController(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            segmented.isEnabled = false
            generalController.alpha = 1
            evaluacionController.alpha = 0
        case 1:
            generalController.alpha = 0
            evaluacionController.alpha = 1
        default:
            print("Default")
        }
    }
    
    
    private func crearRegistroAuditoria(lstLineamientoSub:Array<LineamientoSubItem>){
        if notificar.count > 0 {
            UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Guardando registro...")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            
            let filterNotificar = notificar.filter { (item) -> Bool in
                item.selectEmail == true
            }
            
            //NOTIFICACIONES
            var notificadores:Array<NSDictionary> = []
            for index in 0...filterNotificar.count-1 {
                let paramsNotificar:NSDictionary = [
                    "IdUser": (filterNotificar[index].idUsers ?? 0) as Int,
                    "Nombre": (filterNotificar[index].nombreUsuario ?? "")as String,
                    "Email": (filterNotificar[index].emailCorporativo ?? "") as String,
                ]
                notificadores.append(paramsNotificar)
            }
            
            //EVALUACIONES
            var evaluaciones:Array<NSDictionary> = []
            for index in 0...lstLineamientoSub.count-1 {
                let paramsEvaluacion:NSDictionary = [
                    "Id_Audit_Detalle": (lstLineamientoSub[index].idAuditDetalle ?? 0) as Int,
                    "Id_Audit_Items": (lstLineamientoSub[index].idAuditItems ?? 0 )as Int,
                    "Id_Audit_Lin_SubItems": (lstLineamientoSub[index].idAuditLinSubItems ?? 0) as Int,
                    "Imagen": (lstLineamientoSub[index].imgbase64 ?? "" ) as String,
                    "Calificacion": (lstLineamientoSub[index].idCalificacion ?? 0 ) as Int,
                    "Lugar": lstLineamientoSub[index].idLugar ?? 0 as Int,
                    "Notas": lstLineamientoSub[index].mensage ?? "" as String]
                evaluaciones.append(paramsEvaluacion)
            }
            
            //DATOS GENERALES
            let rolUsario:String = UserDefaults.standard.value(forKey: "Descripcion_Rol") as! String
            let nombreUsuario:String = UserDefaults.standard.value(forKey: "nombreUsuario") as! String
            let nombreFormato:String = "AUDIT-\(rolUsario)-\(nombreUsuario)-\(formatter.string(from: Date()))";
            
            let params:NSDictionary = [
                "Id_Audit": datosGenerales?.idAuditoria ?? 0 as Int,
                "Estado_Audit": (datosGenerales?.estadoAuditoria ?? 0) as Int ,
                "Fecha_Audit": formatter.string(from: Date()),
                "Id_Empresa_contratista": (datosGenerales?.idEmpresaContratista ?? 0) as Int,
                "IdUsers": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                "Nombre_Formato": nombreFormato,
                "Nro_Contrato": datosGenerales?.nroContrato ?? "" as String,
                "Sede": (datosGenerales?.sede ?? 0) as Int,
                "Supervisor_Contrato": (datosGenerales?.supervisorContrato ?? "") as String,
                "Responsable_Contratista": (datosGenerales?.responsableContratista ?? "") as String]
            
         
            if (datosGenerales?.idAuditoria ?? 0 > 0){
                AuditoriaViewModel.init(parent: self).editarRegistroAuditoria(params: params, notificar: notificadores, evaluacion: evaluaciones, callback: {(success,error)in
                    if let data = success, data == 1{
                        self.dismiss(animated: true, completion: nil)
                    }
                    UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                })
                 
            }else{
                AuditoriaViewModel.init(parent: self).crearRegistroAuditoria(params: params, notificar: notificadores, evaluacion: evaluaciones, callback: {(success,error)in
                    if let data = success, data == 1{
                        self.dismiss(animated: true, completion: nil)
                    }
                    UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                })
                
            }
        }else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe seleccionar almenos 1 usuario a notificar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
             UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgNotificarAuditoria"){
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NotificarViewController
            destino.dataItemSelect = notificar
            destino.itemSelect = self
        }else if (segue.identifier == "sgGeneralAuditoria"){
            let destination = segue.destination as! DatosGeneralesAudiViewController
            destination.datosGenerales = datosGenerales
            destination.delegate = self
        }else if (segue.identifier == "sgEvaluacionAuditoria"){
            let destination = segue.destination as! EvaluacionViewController
            destination.lstLineamientoSub = lstLineamientoSub
            destination.delegate = self
        }
    }
    
    
}

//MARK: - Configuracion extension
extension CrearAuditoriaViewController : NotificarSelect{
    func sendData(lista: Array<NotificarModel>) {
        notificar = lista
    }
}

extension CrearAuditoriaViewController : GeneralAuditoriaProcotol{
    func sendData(object: InsertAuditoriaModel) {
        datosGenerales = object
        segmented.isEnabled = true
        segmented.selectedSegmentIndex = 1
        generalController.alpha = 0
        evaluacionController.alpha = 1
    }
}

extension CrearAuditoriaViewController : EvaluacionAuditoriaProcotol{
    func sendData(lista: Array<LineamientoSubItem>) {
        lstLineamientoSub = lista
        crearRegistroAuditoria(lstLineamientoSub:lista)
    }
    
    
}

