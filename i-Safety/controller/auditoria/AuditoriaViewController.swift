//
//  AuditoriaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class AuditoriaViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<AuditoriaModel> = []
    var datosGenerales = InsertAuditoriaModel()
    var lineamientos:Array<LineamientoSubItem> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        obtenerAuditoria()
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.topItem?.title = "Auditoria"
        let crearButton =  UIBarButtonItem(title: "crear", style: .plain, target: self, action: #selector(crearSbc))
        crearButton.tintColor = .white
        navigationItem.rightBarButtonItem = crearButton
    }
    
    @objc func crearSbc(){
        datosGenerales = InsertAuditoriaModel()
        lineamientos.removeAll()
        self.performSegue(withIdentifier: "sgCrearAuditoria", sender: self)
    }
    
    
    func obtenerAuditoria(){
        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                                   "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
                                   "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        print(params)
        AuditoriaViewModel.init(parent: self).listaAuditoriaV2(params: params, callback: {(data,error) in
            if let lista = data{
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
    
    private func obtenerRegistroAuditoria(idAuditoria:Int){
        AuditoriaViewModel.init(parent: self).obtenerRegistroAuditoria(idAuditoria: idAuditoria, callback: {(success,lstLineamiento,error)in
            if let data = success{
                self.datosGenerales = data
                if let lista = lstLineamiento {
                    self.lineamientos = lista
                }
                self.performSegue(withIdentifier: "sgCrearAuditoria", sender: self)
            }
        })
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgCrearAuditoria"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let nav = segue.destination as! UINavigationController
                let destination = nav.topViewController as! CrearAuditoriaViewController
                destination.datosGenerales = datosGenerales
                destination.lstLineamientoSub = lineamientos
            }
        }
    }
}

// MARK:- Configuracion table
extension AuditoriaViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuditoriaTableViewCell", for: indexPath) as! AuditoriaTableViewCell
        cell.txtCodigo.text = dataItem[indexPath.row].codigoAudit
        cell.txtFormato.text =  dataItem[indexPath.row].nombreFormato
        cell.txtFecha.text =  dataItem[indexPath.row].fechaReg
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        obtenerRegistroAuditoria(idAuditoria: dataItem[indexPath.row].idAudit!)
        print("idAuditoria : \(dataItem[indexPath.row].idAudit ?? 0)")
    }
}
