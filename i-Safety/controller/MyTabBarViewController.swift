//
//  MyTabBarViewController.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class MyTabBarViewController: UITabBarController,UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTabBar()
        // Do any additional setup after loading the view.
    }
    func initTabBar(){
        
        let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InspeccionViewController") as? InspeccionViewController
        
        let homeIcon = UITabBarItem(title: "Inspección", image: UIImage(named: "ico_inspeccion"), selectedImage: UIImage(named: "ico_inspeccion"))
        homeVC!.tabBarItem =  homeIcon
        
        let sbcVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SbcViewController") as? SbcViewController
        
        let sbcIcon = UITabBarItem(title: "SBC", image: UIImage(named: "ico_sbc"), selectedImage: UIImage(named: "ico_sbc"))
        sbcVC!.tabBarItem =  sbcIcon
        
        let interventoriaVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InterventoriaViewController") as? InterventoriaViewController
        
        let interventoriaIcon = UITabBarItem(title: "Interventoria", image: UIImage(named: "ico_interventoria"), selectedImage: UIImage(named: "ico_interventoria"))
        interventoriaVC!.tabBarItem =  interventoriaIcon
        
        
        let auditoriaVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AuditoriaViewController") as? AuditoriaViewController
        
        let auditoriaIcon = UITabBarItem(title: "Auditoría", image: UIImage(named: "ico_auditoria"), selectedImage: UIImage(named: "ico_auditoria"))
        auditoriaVC!.tabBarItem =  auditoriaIcon
        
        
         let incidenteVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IndicenteViewController") as? IndicenteViewController
            
         let incidenteIcon = UITabBarItem(title: "Incidente", image: UIImage(named: "ico_incidente"), selectedImage: UIImage(named: "ico_incidente"))
           incidenteVC!.tabBarItem =  incidenteIcon
        
        
//         let menuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
//
//        let menuIcon = UITabBarItem(title: "Menu", image: UIImage(named: "ico_menu"), selectedImage: UIImage(named: "ico_menu"))
//        menuVC!.tabBarItem =  menuIcon
        
        let homeController = UINavigationController(rootViewController: homeVC!)
        let incidenteController = UINavigationController(rootViewController: incidenteVC!)
        let sbcController = UINavigationController(rootViewController: sbcVC!)
        let interventoriaController = UINavigationController(rootViewController: interventoriaVC!)
        let auditoriaController = UINavigationController(rootViewController: auditoriaVC!)
      //  let menuController = UINavigationController(rootViewController: menuVC!)
        
       // self.viewControllers = [homeController,sbcController,menuController]
        self.viewControllers = [homeController,sbcController,interventoriaController,auditoriaController,incidenteController]
    }
    
 
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
