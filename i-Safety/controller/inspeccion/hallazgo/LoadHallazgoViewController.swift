//
//  LoadHallazgoViewController.swift
//  i-Safety
//
//  Created by usuario on 7/6/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol HallazhoSpinnerSelect {
    func sendData(object:ItemSpinnerModel)
}

class LoadHallazgoViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    
    var items:Array<ItemSpinnerModel>?
    var itemSelect:HallazhoSpinnerSelect?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataItem = items!
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
}

extension LoadHallazgoViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemSpinnerHallazgo", for: indexPath) as! LoadHallazgoTableViewCell
            cell.txtNombre.text = dataItem[indexPath.row].nombre?.capitalized
           return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = ItemSpinnerModel()
        object.id = dataItem[indexPath.row].id
        object.nombre = dataItem[indexPath.row].nombre
        itemSelect?.sendData(object: object)
        self.dismiss(animated: true, completion: nil)
    }
    
}
