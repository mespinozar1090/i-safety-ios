//
//  ImgHallazgoViewController.swift
//  i-Safety
//
//  Created by usuario on 11/18/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class ImgHallazgoViewController: UIViewController {

    @IBOutlet weak var imgForo: UIImageView!
    @IBOutlet weak var lblFechaImg: UILabel!
    var imgBase64:String?
    var txtfecha:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let img = imgBase64,img.contains("base64,") {
            let fullNameArr = img.components(separatedBy: "base64,")
            imgForo.image = UtilMetodos.convertBase64StringToImage(imageBase64String: fullNameArr[1])
        }else{
            if let imgUrl = imgBase64{
                imgForo.downloaded(from: imgUrl)
            }
        }
        
        if let date = txtfecha {
            lblFechaImg.text = date
        }
    }
    

    @IBAction func accionClose(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
