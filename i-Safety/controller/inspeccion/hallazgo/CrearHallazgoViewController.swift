//
//  CrearHallazgoViewController.swift
//  i-Safety
//
//  Created by usuario on 7/2/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

class CrearHallazgoViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnGestion: ButtonWithImage!
    @IBOutlet weak var btnActoSubEstandar: ButtonWithImage!
    @IBOutlet weak var btnCondicionSubEstandar: ButtonWithImage!
    
    @IBOutlet weak var edtResponsable: SDCTextField!
    @IBOutlet weak var tvDescripcion: UITextView!
    @IBOutlet weak var edtFecha: UITextField!
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var segmentedTipoAC: UISegmentedControl!
    // @IBOutlet weak var imgFoto: UIImageView!
    @IBOutlet weak var imgFoto: UIButton!
    @IBOutlet weak var viewImg: UIView!
    
    //MARK: - Variable cerrar hallazgo - contratista
    @IBOutlet weak var titleMitigadora: UILabel!
    @IBOutlet weak var tvAccionMitigadora: UITextView!
    @IBOutlet weak var capturaMitigadora: ButtonRadius!
    @IBOutlet weak var showCapturaMitigadora: ButtonRadius!
    @IBOutlet weak var viewCierre: UIView!
    var capturaGalleria:Bool = false
    var selectCaptureImagen:Int?
    
    var imagePicker = UIImagePickerController()
    let datePicker = UIDatePicker()
    var dataItem:Array<ItemSpinnerModel> = []
    var itemHallazgoModel:ItemHallazgoModel?
    var selectButton:Int?
    var idGestion:Int?
    var idBajo,idMedio,idAlto,estadoInspeccion:Int?
    var idHallazgo:Int?
    var imgBase64,imgBase64Mitigadora:String?
    var idInspeccion,selectACCS,idTipoHallazgo: Int?
    var fechaImage,fechaImageMitigadora:String?
    var idActoSubEstandar,idCondicionSubEstandar:Int?
    var showImage:Int = 0;
    
    //button camara
    var usuarioRolHallazgo:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupNavigationBarItem()
        title = "Crear Hallazgo"
        showCapturaMitigadora.isHidden = true
        edtResponsable.delegate = self
        edtResponsable.maxLength = 25
        viewCierre.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        edtFecha.text = formatter.string(from: datePicker.date)
        
        setupButton(button: btnGestion)
        setupButton(button: btnActoSubEstandar)
        setupButton(button: btnCondicionSubEstandar)
        
        tvDescripcion.layer.borderWidth = 1
        tvDescripcion.layer.borderColor = UIColor.black.cgColor
        tvDescripcion.layer.cornerRadius = 3
        
        tvAccionMitigadora.layer.borderWidth = 1
        tvAccionMitigadora.layer.borderColor = UIColor.black.cgColor
        tvAccionMitigadora.layer.cornerRadius = 3
        
        showDatePicker()
        segmented.selectedSegmentIndex = 0
        segmentedTipoAC.selectedSegmentIndex = 0
        idBajo = 1
        idMedio = 0
        idAlto = 0
        estadoInspeccion = 1
        selectACCS = 1
        
        
        if let itemHallazgoModel = itemHallazgoModel {
            title = "Editar Hallazgo"
            idHallazgo = itemHallazgoModel.id;
            
            edtFecha.text = itemHallazgoModel.plazo
            edtResponsable.text = itemHallazgoModel.resposableAreadetalle
            tvDescripcion.text = itemHallazgoModel.actoSubestandar
            
            switch itemHallazgoModel.riesgo{
            case 1:
                segmented.selectedSegmentIndex = 0
                idBajo = 1
                idMedio = 0
                idAlto = 0
            case 2:
                segmented.selectedSegmentIndex = 1
                idBajo = 0
                idMedio = 1
                idAlto = 0
            case 3:
                segmented.selectedSegmentIndex = 2
                idBajo = 0
                idMedio = 0
                idAlto = 1
            default:
                print("error")
            }
            
            if let message = itemHallazgoModel.accionMitigadora{
                tvAccionMitigadora.text = message.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            if(itemHallazgoModel.idActoSubestandar != 0){
                segmentedTipoAC.selectedSegmentIndex = 0
                selectACCS = 1
            }
            
            if(itemHallazgoModel.idCondicionSubestandar != 0){
                segmentedTipoAC.selectedSegmentIndex = 1
                selectACCS = 2
            }
            
            loadGestion()
            loadActoCondicion()
            
            if let img64 = itemHallazgoModel.imgFotoHallazgo{
                imgBase64 = img64
                viewImg.isHidden = false
                imgFoto.isHidden = false
            }
            
            if let dateImg = itemHallazgoModel.fechaFoto{
                fechaImage = dateImg
            }
            
            if let imgCierre = itemHallazgoModel.imgCierre{
                imgBase64Mitigadora = imgCierre
                showCapturaMitigadora.isHidden = false
            }
            
            if let dateImgCierre = itemHallazgoModel.fechaImgCierre{
                fechaImageMitigadora = dateImgCierre
            }
            
            if let usuarioRol = itemHallazgoModel.usuarioRol{
                print("usuarioRol \(usuarioRol)")
                usuarioRolHallazgo = usuarioRol
                validarCreadorHallazgo(idUsuarioRol: usuarioRol)
            }
        }
        
        validarRolCreacion()
    }
    
    private func validarCreadorHallazgo(idUsuarioRol:Int){
        let rolUsuarioLogin = UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int
        
        switch idUsuarioRol {
        case 1:
            if(idUsuarioRol != rolUsuarioLogin ){
                
                btnGestion.isEnabled = false
                btnActoSubEstandar.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                
                edtResponsable.isEnabled = false
                tvDescripcion.isEditable = false
                edtFecha.isEnabled = false
                segmented.isEnabled = false
                segmentedTipoAC.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                
                if(rolUsuarioLogin == 3 || rolUsuarioLogin == 1){
                    viewCierre.isHidden = false
                }
                
            }else if(rolUsuarioLogin == 1){
                viewCierre.isHidden = false
            }
            break
        case 2:
            if (idUsuarioRol != rolUsuarioLogin && rolUsuarioLogin != 1 ){
                btnGestion.isEnabled = false
                btnActoSubEstandar.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                
                edtResponsable.isEnabled = false
                tvDescripcion.isEditable = false
                edtFecha.isEnabled = false
                segmented.isEnabled = false
                segmentedTipoAC.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                if(rolUsuarioLogin == 3 || rolUsuarioLogin == 1){
                    viewCierre.isHidden = false
                }
            }
            break
        case 3:
            if(idUsuarioRol != rolUsuarioLogin && rolUsuarioLogin != 1){
                btnGestion.isEnabled = false
                btnActoSubEstandar.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                
                edtResponsable.isEnabled = false
                tvDescripcion.isEditable = false
                edtFecha.isEnabled = false
                segmented.isEnabled = false
                segmentedTipoAC.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
            }else{
                viewCierre.isHidden = false
            }
            
            break
        case 4:
            if (idUsuarioRol != rolUsuarioLogin && rolUsuarioLogin != 1 ){
                btnGestion.isEnabled = false
                btnActoSubEstandar.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                
                edtResponsable.isEnabled = false
                tvDescripcion.isEditable = false
                edtFecha.isEnabled = false
                segmented.isEnabled = false
                segmentedTipoAC.isEnabled = false
                btnCondicionSubEstandar.isEnabled = false
                if(rolUsuarioLogin == 3 || rolUsuarioLogin == 1){
                    viewCierre.isHidden = false
                }
            }
            break
        default:
            print("Default")
        }
    }
    
    private func validarRolCreacion(){
        if (UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int == 2 || UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int == 4){
            viewCierre.isHidden = true
        }else{
            viewCierre.isHidden = false
        }
    }
    
    func loadGestion(){
        let gestion = UtilMetodos.obtenerGestion();
        if((gestion.count) > 0){
            for index in 0...gestion.count-1 {
                if(gestion[index].id == self.itemHallazgoModel?.idTipoGestion){
                    btnGestion.setTitle(gestion[index].nombre, for: .normal)
                    idGestion = gestion[index].id
                }
            }
        }
        loadTipoHallazgo()
    }
    
    func loadTipoHallazgo(){
        if let idGestion = idGestion {
            let tipoHallazgo = UtilMetodos.tipoHallazgo(idGestion: idGestion)
            for index in 0...tipoHallazgo.count-1 {
                if(tipoHallazgo[index].id == self.itemHallazgoModel?.idHallazgo){
                    btnActoSubEstandar.setTitle(tipoHallazgo[index].nombre, for: .normal)
                    idTipoHallazgo = tipoHallazgo[index].id
                }
            }
        }
    }
    
    func loadActoCondicion(){
        if let idGestion = idGestion {
            if let actoCondiccion = selectACCS{
                let accs = (idGestion == Constante.SST) ? UtilMetodos.actoSubEstandarSST(idASCS: actoCondiccion) : UtilMetodos.actoSubEstandarMA(idASCS: actoCondiccion)
                
                for index in 0...accs.count-1 {
                    if(itemHallazgoModel?.idActoSubestandar != 0){
                        if(accs[index].id == self.itemHallazgoModel?.idActoSubestandar){
                            idActoSubEstandar = accs[index].id
                            btnCondicionSubEstandar.setTitle(accs[index].nombre, for: .normal)
                        }
                    }
                    
                    if(itemHallazgoModel?.idCondicionSubestandar != 0){
                        if(accs[index].id == self.itemHallazgoModel?.idCondicionSubestandar){
                            idCondicionSubEstandar = accs[index].id
                            btnCondicionSubEstandar.setTitle(accs[index].nombre, for: .normal)
                        }
                    }
                }
            }
        }
    }
    
    func validarDetaller()->Bool{
        return itemHallazgoModel != nil ? true:false
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        
        self.navigationController?.navigationBar.topItem?.title = "Hallazgo"
        
        let cancelarButton =  UIBarButtonItem(title: "cerrar", style: .plain, target: self, action: #selector(cancelarAccion))
        
        let infoImage = UIImage(named: "img_foto")
        let button:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 20, height: 20))
        button.setImage(infoImage, for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: #selector(captureImagen), for: UIControl.Event.touchUpInside)
        let searchButton = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = searchButton
        navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func cancelarAccion(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func captureImagen(){
        
        let rolUsuarioLogin = UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int
        if (itemHallazgoModel?.usuarioRol) != nil {
            print("no entrara")
            guard let rol = usuarioRolHallazgo, rol == rolUsuarioLogin ||  rolUsuarioLogin == 1 else{
                return
            }
        }
        
        
        selectCaptureImagen = 1
        showCaptureImagen()
    }
    @IBAction func capturaMitigadora(_ sender: ButtonRadius) {
        selectCaptureImagen = 2
        showCaptureImagen()
    }
    
    @IBAction func showModalImagen(_ sender: ButtonRadius) {
        showImage = 2
        self.performSegue(withIdentifier: "sgImgHallazgo", sender: self)
    }
    
    @IBAction func showImagen(_ sender: UIButton) {
        showImage = 1
        self.performSegue(withIdentifier: "sgImgHallazgo", sender: self)
    }
    
    func takePhoto() {
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    func getPhotoGallery() {
        imagePicker.sourceType = .photoLibrary // or .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func segmendTipoAC(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            selectACCS = 1
            break
        case 1 :
            selectACCS = 2
            break;
        default:
            print("Default")
        }
    }
    
    
    @IBAction func nivelRiego(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            idBajo = 1
            idMedio = 0
            idAlto = 0
            break
        case 1:
            idBajo = 0
            idMedio = 1
            idAlto = 0
            break
        case 2:
            idBajo = 0
            idMedio = 0
            idAlto = 1
            break
        default:
            print("Default")
        }
    }
    
    @IBAction func obtenerGestion(_ sender: UIButton) {
        selectButton = 1
        dataItem = UtilMetodos.obtenerGestion();
        self.performSegue(withIdentifier: "sgItemHallazgo", sender: self)
    }
    
    @IBAction func obtenerTipoHallazgo(_ sender: UIButton) {
        guard let idGestion = idGestion else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe seleccionar Gestion", titleAction: "Aceptar")
            self.present(alert, animated: true, completion: nil)
            return
        }
        selectButton = 2
        dataItem = UtilMetodos.tipoHallazgo(idGestion: idGestion)
        self.performSegue(withIdentifier: "sgItemHallazgo", sender: self)
        
    }
    
    @IBAction func obtenerTipoAC(_ sender: UIButton) {
        selectButton = 3
        if let idGestion = idGestion {
            if let actoCondiccion = selectACCS{
                dataItem = (idGestion == Constante.SST) ? UtilMetodos.actoSubEstandarSST(idASCS: actoCondiccion) : UtilMetodos.actoSubEstandarMA(idASCS: actoCondiccion)
                self.performSegue(withIdentifier: "sgItemHallazgo", sender: self)
            }
        }else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe seleccionar Gestion", titleAction: "Aceptar")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnGuardar(_ sender: UIButton) {
        crearHallazgo()
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cerrar", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        edtFecha.inputAccessoryView = toolbar
        edtFecha.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        //formatter.dateFormat = "MM/D/YYYY"
        edtFecha.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    func crearHallazgo(){
        if(validarCampos()){
            if(!tvAccionMitigadora.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty  || imgBase64Mitigadora != nil){
                guard !tvAccionMitigadora.text.isEmpty else{
                    let alert = UtilMetodos.getAlert(title: "Información", message: "Agrege Acción mitigadora ", titleAction: "Aceptar")
                    self.present(alert,animated: true,completion: nil)
                    return
                }
                
                guard imgBase64Mitigadora != nil && !imgBase64Mitigadora!.isEmpty else{
                    let alert = UtilMetodos.getAlert(title: "Información", message: "Agrege una foto en Accion mitigadora", titleAction: "Aceptar")
                    self.present(alert,animated: true,completion: nil)
                    return
                }
            }
            
            if(nil != itemHallazgoModel){
                UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Editar Hallazgo")
            }else{
                UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Crear Hallazgo")
            }
            
            let params:NSDictionary = [
                "Id_Inspecciones_reporte_detalle": idHallazgo ?? 0 as Int ,
                "Id_Inspeccion": idInspeccion ?? 0 as Int,
                "idTipoGestion": idGestion ?? 0 as Int,
                "id_ActoSubestandar": (idActoSubEstandar ?? 0) as Int,
                "id_CondicionSubestandar": (idCondicionSubEstandar ?? 0) as Int,
                "Acto_Subestandar": tvDescripcion.text ?? "" as String,
                "Riesgo_A": (idAlto ?? 0) as Int,
                "Riesgo_M": (idMedio ?? 0) as Int,
                "Riesgo_B": (idBajo ?? 0) as Int,
                "idHallazgo": (idTipoHallazgo ?? 0) as Int,
                "id_NoConformidad": 0,
                "id_SubFamiliaAmbiental": 0,
                "Resposable_Area_detalle": edtResponsable.text ?? "" as String,
                "EvidenciaFotograficaObservacion": (imgBase64 ?? "") as String,
                "Accion_Mitigadora": tvAccionMitigadora.text ?? "" as String,
                "PlazoString": edtFecha.text ?? "" as String,
                "Estado_DetalleInspeccion": (estadoInspeccion ?? 0) as Int,
                "Evidencia_Cierre": (imgBase64Mitigadora ?? "") as String,
                "fechaImgHallazgo": (fechaImage ?? "") as String,
                "fechaImgMitigadora": (fechaImageMitigadora ?? "") as String,
                "Id_Usuario_Registro": UserDefaults.standard.value(forKey: "idUsers") as! Int
            ]
            
            InspeccionViewModel.init(parent: self).crearHallazgo(params: params, callback: {(success,error) in
                if(success ?? false){
                    UtilMetodos.hideActivityIndicator(uiView: self.view)
                    self.navigationController?.popViewController(animated: true)
                }else{
                    UtilMetodos.hideActivityIndicator(uiView: self.view)
                }
            })
        }
    }
    
    func validarCaracterEspecial(enteredEmail:String) -> Bool {
        let emailFormat = ".*[^A-Za-z0-9\\s].*"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func validarCampos() -> Bool{
        
        guard !edtResponsable.text!.isEmpty else {
            UtilMetodos.tfSetError(field: edtResponsable, placeholder:"Campo obligatorio")
            return false
        }
        
        guard !tvDescripcion.text!.isEmpty else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe ingresar un decripción del hallazgo", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard tvDescripcion.text.count <= 60 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe ingresar un máximo de 60 caracteres en el campo descripción", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let base64 = imgBase64,!base64.isEmpty else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Falta adjuntar una evidencia fotográfica", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        //           guard !validarCaracterEspecial(enteredEmail: tvDescripcion.text ?? "") else {
        //               let alert = UtilMetodos.getAlert(title: "Información", message: "No se permite caracteres especiales, en el campo Descripción", titleAction: "Aceptar")
        //               self.present(alert,animated: true,completion: nil)
        //               return false
        //           }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    private func showCaptureImagen(){
        let alert = UIAlertController(title: "Seleccione", message: "Evidencia Fotográfica", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camara", style: .default, handler: {(accion)in
            print("selecciono camara")
            self.capturaGalleria = false
            self.takePhoto()
        }))
        alert.addAction(UIAlertAction(title: "Galeria", style: .default, handler: {(accion)in
            self.capturaGalleria = true
            self.getPhotoGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: {(accion)in
            print("selecciono cancelar")
        }))
        
        self.present(alert,animated: true)
        
    }
    
    @available(iOS 14, *)
    func presentPicker(filter:PHPickerFilter){
        var configuration = PHPickerConfiguration()
        configuration.filter = filter
        configuration.selectionLimit = 1
        
        let picker = PHPickerViewController(configuration: configuration)
        picker.delegate = self
        present(picker,animated: true,completion: nil)
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgItemHallazgo"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! LoadHallazgoViewController
            destination.items = dataItem
            destination.itemSelect = self
        }else if(segue.identifier == "sgImgHallazgo"){
            let destination = segue.destination as! ImgHallazgoViewController
            destination.imgBase64 = (showImage == 1 ? imgBase64 : imgBase64Mitigadora)
            destination.txtfecha = (showImage == 1 ? fechaImage : fechaImageMitigadora)
        }
    }
}

extension CrearHallazgoViewController : HallazhoSpinnerSelect{
    func sendData(object: ItemSpinnerModel) {
        switch selectButton {
        case 1:
            btnGestion.setTitle(object.nombre, for: .normal)
            idGestion = object.id
            
            btnActoSubEstandar.setTitle("Seleccionar", for: .normal)
            idTipoHallazgo = 0
            btnCondicionSubEstandar.setTitle("Seleccionar", for: .normal)
            idCondicionSubEstandar = 0
            idActoSubEstandar = 0
        case 2:
            btnActoSubEstandar.setTitle(object.nombre, for: .normal)
            idTipoHallazgo = object.id
        case 3:
            btnCondicionSubEstandar.setTitle(object.nombre, for: .normal)
            if(selectACCS == 1){
                idCondicionSubEstandar = 0
                idActoSubEstandar =  object.id
            }else if (selectACCS == 2){
                idActoSubEstandar = 0
                idCondicionSubEstandar =  object.id
            }
        default:
            print("Algo salio mal")
        }
    }
    
    
}


@available(iOS 14, *)
extension CrearHallazgoViewController : PHPickerViewControllerDelegate{
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true, completion: nil)
    }
}

extension CrearHallazgoViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[.originalImage] as? UIImage {
            var asset: PHAsset?
            let newImage = resizeImage(image: image, newWidth: 512)
            if(selectCaptureImagen == 1){
                imgBase64 = "data:image/jpeg;base64,\(UtilMetodos.convertImageToBase64(image: newImage!))"
                imgFoto.isHidden = false
                viewImg.isHidden = false
            }else if(selectCaptureImagen == 2){
                imgBase64Mitigadora =  "data:image/jpeg;base64,\(UtilMetodos.convertImageToBase64(image: newImage!))"
                showCapturaMitigadora.isHidden = false
            }
            
            
            if(capturaGalleria){
                if #available(iOS 11.0, *) {
                    asset = info[.phAsset] as? PHAsset
                } else {
                    if let url = info[.referenceURL] as? URL {
                        let result = PHAsset.fetchAssets(withALAssetURLs: [url], options: nil)
                        asset = result.firstObject
                    }
                }
                if let asset = asset {
                    let imageManager = PHImageManager.default()
                    imageManager.requestImageDataAndOrientation(for: asset, options: nil, resultHandler: {
                        (data, responseString, imageOriet, info) -> Void in
                        let imageData: NSData = data! as NSData
                        if let imageSource = CGImageSourceCreateWithData(imageData, nil) {
                            let imageDict = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil)! as NSDictionary
                            let exifDict_ = imageDict.value(forKey: "{Exif}") as! NSDictionary
                            let dateTimeOriginal = exifDict_.value(forKey:kCGImagePropertyExifDateTimeOriginal as String) as! NSString
                            if(self.selectCaptureImagen == 1){
                                if dateTimeOriginal != ""{
                                    print("Entro a original")
                                    self.fechaImage = "\(dateTimeOriginal)"
                                }else{
                                    print("Entro a date locate")
                                    self.fechaImage = "\(Date().localDate())"
                                }
                                
                            }else{
                                if dateTimeOriginal != ""{
                                    print("Entro a original")
                                    self.fechaImageMitigadora = "\(dateTimeOriginal)"
                                }else{
                                    print("Entro a date locate")
                                    self.fechaImageMitigadora = "\(Date().localDate())"
                                }
                            }
                        }
                        
                    })
                    
//                    imageManager.requestImageData(for: asset , options: nil, resultHandler:{
//                        (data, responseString, imageOriet, info) -> Void in
//                        let imageData: NSData = data! as NSData
//                        if let imageSource = CGImageSourceCreateWithData(imageData, nil) {
//                            let imageDict = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil)! as NSDictionary
//                            let exifDict_ = imageDict.value(forKey: "{Exif}") as! NSDictionary
//                            let dateTimeOriginal = exifDict_.value(forKey:kCGImagePropertyExifDateTimeOriginal as String) as! NSString
//                            if(self.selectCaptureImagen == 1){
//                                if dateTimeOriginal != ""{
//                                    self.fechaImage = "\(dateTimeOriginal)"
//                                }else{
//                                    self.fechaImage = "\(Date().localDate())"
//                                }
//
//                            }else{
//                                if dateTimeOriginal != ""{
//                                    self.fechaImageMitigadora = "\(dateTimeOriginal)"
//                                }else{
//                                    self.fechaImageMitigadora = "\(Date().localDate())"
//                                }
//                            }
//                        }
//
//                    })
                }else{
                    if(self.selectCaptureImagen == 1){
                        self.fechaImage = "\(Date().localDate())"
                        print("\(Date().localDate())")
                    }else{
                        self.fechaImageMitigadora = "\(Date().localDate())"
                        print("\(Date().localDate())")
                    }
                }
            }else{
                if(self.selectCaptureImagen == 1){
                    self.fechaImage = "\(Date().localDate())"
                    print("\(Date().localDate())")
                }else{
                    self.fechaImageMitigadora = "\(Date().localDate())"
                    print("\(Date().localDate())")
                }
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        //   let scale = newWidth / image.size.width
        //   let newHeight = image.size.height * scale
        let newHeight = newWidth
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension UIImage {
    
    func getExifData() -> CFDictionary? {
        var exifData: CFDictionary? = nil
        if let data = self.jpegData(compressionQuality: 1.0) {
            data.withUnsafeBytes {
                let bytes = $0.baseAddress?.assumingMemoryBound(to: UInt8.self)
                if let cfData = CFDataCreate(kCFAllocatorDefault, bytes, data.count),
                   let source = CGImageSourceCreateWithData(cfData, nil) {
                    exifData = CGImageSourceCopyPropertiesAtIndex(source, 0, nil)
                }
            }
        }
        return exifData
    }
}



// Friday, January 29, 2016"
