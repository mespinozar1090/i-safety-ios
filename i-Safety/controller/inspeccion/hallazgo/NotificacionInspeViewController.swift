//
//  NotificacionInspeViewController.swift
//  i-Safety
//
//  Created by usuario on 10/28/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class NotificacionInspeViewController: UIViewController {
    
    @IBOutlet weak var btnEnviar: UIButton!
    lazy var searchBar:UISearchBar = UISearchBar()
    var dataItem:Array<NotificarModel> = []
    var dataSearch:Array<NotificarModel> = []
    var idInspeccion:Int?
    var nombreproyecto:String?
    var codigoInspeccion:String?
    
    @IBOutlet weak var tblItem: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpsearchBar()
        obtenerUsuarioNotificar()
    }
    
    func obtenerUsuarioNotificar(){
        InspeccionViewModel.init(parent: self).obtenerUsuarioNotificar(callback: {(success,data,error)in
            if(success!){
                self.dataItem = data!
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
                self.dataSearch = self.dataItem
            }
        })
    }
    
    private func setUpsearchBar(){
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = "Buscar"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .gray
        navigationItem.titleView = searchBar
        
        let btnAceptar =  UIBarButtonItem(title: "Cerrar", style: .plain, target: self, action: #selector(closeView))
        btnAceptar.tintColor = .white
        navigationItem.rightBarButtonItem = btnAceptar
    }
    
    @objc func closeView(){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func enviarNotificacion(_ sender: UIButton) {
        
        var contador = 0;
        var notificadores:Array<NSDictionary> = []
        for index in 0...dataItem.count-1 {
            if(dataItem[index].selectEmail!){
                contador = contador + 1
                let params:NSDictionary = [
                    "IdUser": (dataItem[index].idUsers ?? 0) as Int,
                    "Nombre": (dataItem[index].nombreUsuario ?? "")as String,
                    "Email": (dataItem[index].emailCorporativo ?? "") as String,
                ]
                notificadores.append(params)
            }
        }
        
        guard contador > 0 else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione almenos un contacto a notificar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return
        }
        
        let params:NSDictionary = [
            "nombreproyecto": nombreproyecto ?? "",
            "codigoInspeccion": codigoInspeccion ?? "",
            "Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
            "Id_Inspecciones": idInspeccion ?? ""]
        
        
        
        InspeccionViewModel.init(parent: self).enviarNotificacion(params: params, notificar: notificadores, callback: {(success,error) in
            if let result = success{
                print("result is: \(result)")
                
                let message:String = "Se notifico a los usuarios seleccionados"
                let alert = UIAlertController(title: "Información", message: message, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) -> Void in
                    self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: configuration table searchBar
extension NotificacionInspeViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            dataItem = dataSearch
            tblItem.reloadData()
            return
        }
        dataItem = dataSearch.filter({ (object) -> Bool in
            guard let text = searchBar.text else {return false}
            return (object.nombreUsuario?.localizedCaseInsensitiveContains(text))!
        })
        tblItem.reloadData()
    }
}

//MARK: configuration table
extension NotificacionInspeViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificacionInspeTableViewCell", for: indexPath) as! NotificacionInspeTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombreUsuario?.capitalized
        cell.txtCorreo.text = dataItem[indexPath.row].emailCorporativo
        
        if(dataItem[indexPath.row].selectEmail ?? false){
            cell.btnImagen.setImage(UIImage(named: "ico_checkgreen"), for: .normal)
        }else{
            cell.btnImagen.setImage(UIImage(named: "ico_checkgris"), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let select = dataItem[indexPath.row].selectEmail!
        if let cell = tableView.cellForRow(at: indexPath) as? NotificacionInspeTableViewCell {
            if(select){
                cell.btnImagen.setImage(UIImage(named: "ico_checkgris"), for: .normal)
                dataItem[indexPath.row].selectEmail = false
            }else{
                cell.btnImagen.setImage(UIImage(named: "ico_checkgreen"), for: .normal)
                dataItem[indexPath.row].selectEmail = true
            }
        }
    }
}
