//
//  LoadInspeccionViewController.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol ItemSpinnerSelect {
    func sendData(object:ItemSpinnerModel)
}

class LoadInspeccionViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    var dataSearch:Array<ItemSpinnerModel> = []
    lazy var searchBar:UISearchBar = UISearchBar()
    var items:Array<ItemSpinnerModel>?
    var itemSelect:ItemSpinnerSelect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpsearchBar()
        dataItem.removeAll()
        dataItem = items!
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
        self.dataSearch = self.dataItem
    }
    
    private func setUpsearchBar(){
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = "Buscar"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .gray
        navigationItem.titleView = searchBar
    }
}

//MARK: configuration table searchBar
extension LoadInspeccionViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            dataItem = dataSearch
            tblItem.reloadData()
            return
        }
        dataItem = dataSearch.filter({ (object) -> Bool in
            guard let text = searchBar.text else {return false}
            return (object.nombre?.localizedCaseInsensitiveContains(text))!
        })
        tblItem.reloadData()
    }
}

extension LoadInspeccionViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemInspeccion", for: indexPath) as! ItemInspeccionTableViewCell
        if let descripcion = dataItem[indexPath.row].descripcion, descripcion != "" {
            cell.lblNombre.text = "\(dataItem[indexPath.row].nombre ?? "") \(descripcion)"
        }else{
            cell.lblNombre.text = dataItem[indexPath.row].nombre ?? ""
        }
        
        print("id es marin : \(dataItem[indexPath.row].id)")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = ItemSpinnerModel()
        object.id = dataItem[indexPath.row].id
        object.nombre = dataItem[indexPath.row].nombre
        object.descripcion = dataItem[indexPath.row].descripcion
          print("id es rodri : \(dataItem[indexPath.row].id)")
        itemSelect?.sendData(object: object)
       
        self.dismiss(animated: true, completion: nil)
    }
    
}

