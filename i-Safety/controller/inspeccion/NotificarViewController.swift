//
//  NotificarViewController.swift
//  i-Safety
//
//  Created by usuario on 8/5/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol NotificarSelect {
    func sendData(lista:Array<NotificarModel>)
}

class NotificarViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    lazy var searchBar:UISearchBar = UISearchBar()
    
    var dataItem:Array<NotificarModel> = []
    var dataSearch:Array<NotificarModel> = []
    var dataItemSelect:Array<NotificarModel> = []
    var itemSelect:NotificarSelect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpsearchBar()
        
        if(dataItemSelect.count > 0){
            self.dataItem = dataItemSelect
            dataSearch = dataItem
            DispatchQueue.main.async {
                self.tblItem.reloadData()
            }
        }else{
            obtenerUsuarioNotificar()
        }
    }
    
    func obtenerUsuarioNotificar(){
        InspeccionViewModel.init(parent: self).obtenerUsuarioNotificar(callback: {(success,data,error)in
            if(success!){
                self.dataItem = data!
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
                self.dataSearch = self.dataItem
            }
        })
    }
    
    private func setUpsearchBar(){
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = "Buscar"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .gray
        navigationItem.titleView = searchBar
        
        let btnAceptar =  UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(addNotificadores))
        btnAceptar.tintColor = .white
        navigationItem.rightBarButtonItem = btnAceptar
    }
    
    @objc func addNotificadores(){
        var contador:Int = 0
        for index in 0...dataItem.count-1 {
            if(dataItem[index].selectEmail!){
                contador = contador + 1
            }
        }
        
        guard contador > 0 else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione almenos un contacto a notificar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return
        }
        
        itemSelect?.sendData(lista: dataItem)
        dismiss(animated: true, completion: nil)
       
       // navigationController?.popViewController(animated: true)
    }
}

//MARK: configuration table searchBar
extension NotificarViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            dataItem = dataSearch
            tblItem.reloadData()
            return
        }
        dataItem = dataSearch.filter({ (object) -> Bool in
            guard let text = searchBar.text else {return false}
            return (object.nombreUsuario?.localizedCaseInsensitiveContains(text))!
        })
        tblItem.reloadData()
    }
}

//MARK: configuration table
extension NotificarViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificarTableViewCell", for: indexPath) as! NotificarTableViewCell
        cell.lblNombre.text = dataItem[indexPath.row].nombreUsuario?.capitalized
        cell.lblCorreo.text = dataItem[indexPath.row].emailCorporativo
        
        if(dataItem[indexPath.row].selectEmail ?? false){
            cell.btnImagen.setImage(UIImage(named: "ico_checkgreen"), for: .normal)
        }else{
            cell.btnImagen.setImage(UIImage(named: "ico_checkgris"), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let select = dataItem[indexPath.row].selectEmail!
        if let cell = tableView.cellForRow(at: indexPath) as? NotificarTableViewCell {
            if(select){
                cell.btnImagen.setImage(UIImage(named: "ico_checkgris"), for: .normal)
                dataItem[indexPath.row].selectEmail = false
            }else{
                cell.btnImagen.setImage(UIImage(named: "ico_checkgreen"), for: .normal)
                dataItem[indexPath.row].selectEmail = true
            }
        }
    }
}


//MARK: - volver a la vista anterior
// navigationController?.popViewController(animated: true)
//MARK: - volver a la vista raiz
// navigationController?.popToRootViewController(animated: true)
