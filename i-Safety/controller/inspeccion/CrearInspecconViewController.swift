//
//  CrearInspecconViewController.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol InspeccionProtocol {
    func sendData(loadTable:Bool)
}

class CrearInspecconViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnUsuario: ButtonWithImage!
    @IBOutlet weak var btnProyecto: ButtonWithImage!
    @IBOutlet weak var btnContratista: ButtonWithImage!
    @IBOutlet weak var btnTipoUbicacion: ButtonWithImage!
    @IBOutlet weak var btnLugar: ButtonWithImage!
    @IBOutlet weak var edtTorre: SDCTextField!
    @IBOutlet weak var edtResponsableArea: SDCTextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var dataItemSelect:Array<NotificarModel> = []
    var practica:Array<BuenaPracticaModel>?
    var selectItem : InspeccionProtocol?
    
    var idProyecto,idLugar,idContratista,idTipoUbicacion,idAreaInpeccionada,idInspeccion,idEstado,idUsuarioResponsable:Int?
    var isBuenaPractica:Bool = false
    var isNotificacion:Bool = false
    var areaInspeccionada,nombreLugar,usuarioResponsable:String?
    var selectButton = 0;
    var detalleInspeccion:DetalleInspeccionModel?
    var dataItem:Array<ItemSpinnerModel> = []
    var dataBuenaPractica:Array<BuenaPracticaModel> = []
    var empresaProyecto:EmpresaProyectoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Crear Inspección"
        hideKeyboardWhenTappedAround()
        setupButton(button: btnProyecto)
        setupButton(button: btnContratista)
        setupButton(button: btnTipoUbicacion)
        setupButton(button: btnLugar)
        //setupButton(button: btnAreaResponsable)
        setupButton(button: btnUsuario)
        setupNavigationBarItem()
        segmentedControl.selectedSegmentIndex = 0
        idEstado = 1
        
        edtTorre.delegate = self
        edtTorre.maxLength = 25
        edtResponsableArea.delegate = self
        edtResponsableArea.maxLength = 25
        
        if let data = detalleInspeccion {
            title = "Editar Inspección"
            idTipoUbicacion = data.idTipoUbicacion
            idProyecto = data.idProyecto
            idLugar = data.idLugar
            idInspeccion = data.idInspecciones
            edtTorre.text = data.torre
            edtResponsableArea.text = data.responsableAreaInspeccion
            segmentedControl.selectedSegmentIndex = (data.tipoInspeccion == 1) ? 0:1
            areaInspeccionada = data.areaInspecionada
            loadObtenerProyecto()
            loadUsuario()
           
        }
        
        if let buenaPractica = practica{
             dataBuenaPractica = buenaPractica
        }
    }
    
    func validarDetalle()->Bool{
        return detalleInspeccion != nil ? true : false
    }
    
    @IBAction func selectEstado(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            idEstado = 3
        case 1:
            idEstado = 1
        default:
            print("Default")
        }
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        let cancelarButton =  UIBarButtonItem(title: "cerrar", style: .plain, target: self, action: #selector(cancelarAccion))
        cancelarButton.tintColor = .white
        
        let infoImage = UIImage(named: "ico_blog")
        let button:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30, height: 20))
        button.setImage(infoImage, for: .normal)
        button.sizeToFit()
        button.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(addBuenaPractica), for: UIControl.Event.touchUpInside)
        let searchButton = UIBarButtonItem(customView: button)
        
        navigationItem.rightBarButtonItems = [searchButton]
        navigationItem.leftBarButtonItem = cancelarButton
    }

    
    @objc func cancelarAccion(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func addBuenaPractica(){
       self.performSegue(withIdentifier: "sgBuenaPractica", sender: self)
    }
    
    //MARK: - Load detalle de inspeccion - WS
    func loadObtenerProyecto(){
        let params:NSDictionary = ["Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        InspeccionViewModel.init(parent: self).obtenerProyecto(params: params, callback: {(data,error) in
            if data != nil{
                if((data?.count)! > 0){
                    for index in 0...data!.count-1 {
                        if(data![index].id == self.detalleInspeccion?.idProyecto){
                            self.btnProyecto.setTitle(data![index].nombre, for: .normal)
                            self.idProyecto = data![index].id
                            self.obtenerEmpresaProyecto(idProyecto:self.idProyecto!)
                        }
                    }
                    self.loadObtenerContratista()
                    self.loadObtenerTipoUbicacion()
                    self.loadObtenerLugar()
                    
                }
            }
        })
    }
    
    func loadObtenerContratista(){
        InspeccionViewModel.init(parent: self).obtenerContratista(idProyecto: idProyecto!,callback: {(data,error) in
            if data != nil{
                if((data?.count)! > 0){
                    for index in 0...data!.count-1 {
                        if(data![index].id == self.detalleInspeccion?.idEmpresaContratista){
                            self.btnContratista.setTitle(data![index].nombre, for: .normal)
                            self.idContratista = data![index].id
                        }
                    }
                }
            }
        })
    }
    
    func loadObtenerTipoUbicacion(){
        InspeccionViewModel.init(parent: self).obtenerTipoUbicacion(idProyecto: idProyecto!,callback: {(data,error) in
            if data != nil{
                if((data?.count)! > 0){
                    for index in 0...data!.count-1 {
                        if(data![index].id == self.detalleInspeccion?.idTipoUbicacion){
                            self.btnTipoUbicacion.setTitle(data![index].nombre, for: .normal)
                            self.idTipoUbicacion = data![index].id
                            self.edtTorre.isEnabled = data![index].id == 2  ? true : false
                        }
                    }
                }
            }
        })
    }
    
    func loadObtenerLugar(){
        InspeccionViewModel.init(parent: self).obtenerLugar(idTipoUbicacion: idTipoUbicacion!,callback: {(data,error) in
            if data != nil{
                if((data?.count)! > 0){
                    for index in 0...data!.count-1 {
                        if(data![index].id == self.detalleInspeccion?.idLugar){
                            self.btnLugar.setTitle(data![index].nombre, for: .normal)
                            self.nombreLugar = data![index].nombre
                            self.idLugar = data![index].id
                        }
                    }
                }
            }
        })
    }
    
    func loadUsuario(){
       
        self.idUsuarioResponsable = self.detalleInspeccion?.idUsuario
        self.usuarioResponsable = self.detalleInspeccion?.nombreResponsableInspeccion
        self.btnUsuario.setTitle( self.detalleInspeccion?.nombreResponsableInspeccion, for: .normal)
        
//        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
//                                          "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
//                                          "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
//        InspeccionViewModel.init(parent: self).obtenerUsuarios(params: params, callback: {(data,error) in
//            if let data = data {
//                for index in 0...data.count-1 {
//                    if(data[index].id == self.detalleInspeccion?.idUsuario){
//                        self.btnUsuario.setTitle(data[index].nombre, for: .normal)
//                        self.usuarioResponsable = "\(data[index].nombre ?? "")  \(data[index].descripcion ?? "")"
//                        self.idUsuarioResponsable = data[index].id
//                    }
//                }
//            }
//        })
    }
    
    
    //MARK: - obtener item desde spinner
    func obtenerProyecto(){
        let params:NSDictionary = ["Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        InspeccionViewModel.init(parent: self).obtenerProyecto(params: params, callback: {(data,error) in
            if data != nil{
                if((data?.count)! > 0){
                    self.dataItem = data!
                    self.performSegue(withIdentifier: "sgItemInspeccion", sender: self)
                }
            }
        })
    }
    
    func obtenerContratista(){
        if let idProyecto = idProyecto{
            InspeccionViewModel.init(parent: self).obtenerContratista(idProyecto: idProyecto,callback: {(data,error) in
                if data != nil{
                    if((data?.count)! > 0){
                        self.dataItem = data!
                        self.performSegue(withIdentifier: "sgItemInspeccion", sender: self)
                    }
                }
            })
        }else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione un proyecto", titleAction: "Aceptar")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func obtenerTipoUbicacion(){
        if let idProyecto = idProyecto{
            InspeccionViewModel.init(parent: self).obtenerTipoUbicacion(idProyecto: idProyecto,callback: {(data,error) in
                if data != nil{
                    if((data?.count)! > 0){
                        self.dataItem = data!
                        self.performSegue(withIdentifier: "sgItemInspeccion", sender: self)
                    }
                }else{
                    let alert = UtilMetodos.getAlert(title: "Información", message: "Proyecto no contiene tipo ubicación", titleAction: "Aceptar")
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione un proyecto", titleAction: "Aceptar")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func obtenerLugar(){
        if let idTipoUbicacion = idTipoUbicacion{
            InspeccionViewModel.init(parent: self).obtenerLugar(idTipoUbicacion: idTipoUbicacion,callback: {(data,error) in
                if data != nil{
                    if((data?.count)! > 0){
                        self.dataItem = data!
                        self.performSegue(withIdentifier: "sgItemInspeccion", sender: self)
                    }
                }
            })
        }else{
            let alert = UtilMetodos.getAlert(title: "Información", message: "Proyecto no contiene lugar", titleAction: "Aceptar")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func obtenerEmpresaProyecto(idProyecto:Int){
        InspeccionViewModel.init(parent: self).empresaProyecto(idProyecto: idProyecto,callback: { (data, error) in
            if data != nil{
                self.empresaProyecto = data
            }
        })
    }
    
    @IBAction func obtenerProyecto(_ sender: UIButton) {
        self.selectButton = 1;
        obtenerProyecto()
    }
    
    @IBAction func obtenerContratista(_ sender: Any) {
        self.selectButton = 2;
        obtenerContratista()
    }
    
    @IBAction func obtenerUbicacion(_ sender: Any) {
        self.selectButton = 3;
        obtenerTipoUbicacion()
    }
    
    @IBAction func obtenerLugar(_ sender: UIButton) {
        self.selectButton = 4;
        obtenerLugar()
    }
    
    @IBAction func obtenerAreaInspeccion(_ sender: UIButton) {
        self.selectButton = 5;
        InspeccionViewModel.init(parent: self).obtenerAreaInspeccion(callback: {(data,error) in
            if data != nil{
                if((data?.count)! > 0){
                    self.dataItem = data!
                    self.performSegue(withIdentifier: "sgItemInspeccion", sender: self)
                }
            }
        })
    }
    
    @IBAction func obtenerUsuario(_ sender: UIButton) {
        
        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
        "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
        "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        
        self.selectButton = 6;
        InspeccionViewModel.init(parent: self).obtenerUsuarios(params: params, callback: {(data,error) in
            if let result = data {
                if((result.count) > 0){
                    self.dataItem = result
                    self.performSegue(withIdentifier: "sgItemInspeccion", sender: self)
                }
            }
        })
    }
    
    
    func validarCaracterEspecial(enteredEmail:String) -> Bool {
        let emailFormat = ".*[^A-Za-z0-9\\s].*"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    @IBAction func crearInspeccion(_ sender: UIButton) {
        
        if(validarCampos()){
            if(nil != detalleInspeccion){
                UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Editar inspección")
            }else{
                UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Crear inspección")
            }
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            
            let horaFormate = DateFormatter()
            horaFormate.dateFormat = "HH:mm:ss"
            let horaInspeccion = horaFormate.string(from: Date().localDate())
            
            
            let rolUsario:String = UserDefaults.standard.value(forKey: "Descripcion_Rol") as! String
            let nombreUsuario:String = UserDefaults.standard.value(forKey: "nombreUsuario") as! String
            let nombreFormato:String = "Inspección - \(rolUsario) - \(nombreUsuario) - \(formatter.string(from: Date().localDate()))";
            
            let params:NSDictionary = [
                "Id_Inspecciones": (idInspeccion ?? 0) as Int ,
                "idProyecto": idProyecto! as Int,
                "Id_Empresa_contratista": (idContratista ?? 0) as Int,
                "idTipoUbicacion": idTipoUbicacion ?? 0 as Int,
                "idLugar": idLugar ?? 0 as Int,
                "Torre": edtTorre.text ?? "" as String,
                "Id_Empresa_Observadora": (empresaProyecto?.idEmpresaMaestra! ?? 0) as Int,
                "RUC": (empresaProyecto?.ruc ?? "") as String,
                "Id_Actividad_Economica": (empresaProyecto?.idActividadEconomica ?? 0) as Int,
                "Domicilio_Legal": (empresaProyecto?.domicilioLegal ?? "") as String,
                "Nro_Trabajadores": (empresaProyecto?.nroTrabajadores ?? 0) as Int,
                "Tipo_inspeccion": idEstado ?? 3,
                "Fecha_Hora_Inspeccion": horaInspeccion as String,
                "Responsable_Inspeccion": (usuarioResponsable ?? "") as String,
                "Area_Inspeccionada": "\(nombreLugar ?? "") - \(edtTorre.text ?? "")" as String,
                "Responsable_Area_Inspeccion": edtResponsableArea.text ?? ""  as String,
                "Objectivo_Inspeccion_Interna": Constante.OBJECTICO_INSPECION_INTERNA as String,
                "Resultado_Inspeccion": Constante.RESULTADO_INSPECCION as String,
                "Conclusiones_Recomendaciones": Constante.CONCLUSIONES_RECOMENTACIONES as String,
                "Origen_Registro": "APP" as String,
                "Estado_Inspeccion": 1,
                "Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                "InspeccionInterna": 0,
                "nombreFormato": nombreFormato as String,
                "Id_Usuario_Registro": UserDefaults.standard.value(forKey: "idUsers") as! Int,]
            
            InspeccionViewModel.init(parent: self).insertarInspeccion(params: params, callback: {(data,error) in
                if let idInspeccion = data?.idInspecciones{
                    if(self.isBuenaPractica){
                        self.insertarBuenasPracticas(idInspeccion: "\(idInspeccion)")
                    }else{
                        self.selectItem?.sendData(loadTable: true)
                        UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    print("es null crearInspeccion")
                    UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                }
            })
        }
    }
    
    func validarCampos() -> Bool{
        
        guard let idproyecto = idProyecto, idproyecto > 0 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione un Proyecto", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let idcontratista = idContratista, idcontratista > 0 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione un Contratista", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let idlugar = idLugar, idlugar > 0 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione un Lugar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard !edtTorre.text!.isEmpty else {
            UtilMetodos.tfSetError(field: edtTorre, placeholder:"Campo obligatorio")
            return false
        }
        
        guard idUsuarioResponsable ?? 0 > 0 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione un Responsable de registro", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard !edtResponsableArea.text!.isEmpty else {
            UtilMetodos.tfSetError(field: edtResponsableArea, placeholder:"Campo obligatorio")
            return false
        }
        return true
    }
    
    
    func insertarBuenasPracticas(idInspeccion:String){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let fechaInspeccion = formatter.string(from: date)
        
        var listaParams:Array<NSDictionary> = []
        
        for index in 0...dataBuenaPractica.count-1 {
            if let idBuenaPractica = dataBuenaPractica[index].idBuenasPracticas{
                print("contiene idBuenaPractica  no va enviar \(idBuenaPractica)")
            }else{
               let params:NSDictionary = [
                    "idProyecto": (idProyecto ?? 0) as Int ,
                    "Id_Inspecciones": Int(idInspeccion) ?? 0,
                    "idCategoriaBuenaPractica": (dataBuenaPractica[index].idCategoriaBuenaPractica ?? 0) as Int,
                    "Id_Empresa_contratista": (empresaProyecto?.idEmpresaMaestra! ?? 0) as Int,
                    "Descripcion": (dataBuenaPractica[index].descripcion ?? "") as String,
                    "nombreImg": "Imagen" as String,
                    "fechaImg":fechaInspeccion as String,
                    "ImgB64": (dataBuenaPractica[index].imgBase64 ?? "") as String,
                ]
                listaParams.append(params)
            }
        }
        
        InspeccionViewModel.init(parent: self).insertarBuenaPractica(params: listaParams,callback: {(success,error) in
            if(success!){
                //SVProgressHUD.dismiss()
                self.selectItem?.sendData(loadTable: true)
                UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                self.dismiss(animated: true, completion: nil)
                //  self.navigationController?.popViewController(animated: true)
            }else{
                // SVProgressHUD.dismiss()
                UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                let alert = UtilMetodos.getAlert(title: "Información", message: "Ocurrio un error al agregar buenas practicas", titleAction: "Aceptar")
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgItemInspeccion"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! LoadInspeccionViewController
            destination.items = dataItem
            destination.itemSelect = self
        }else if(segue.identifier == "sgBuenaPractica"){
            let destination = segue.destination as! BuenaPracticaViewController
            destination.items = dataBuenaPractica
            destination.itemSelectBP = self
        }else if(segue.identifier == "sgNotificar"){
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NotificarViewController
            // let destino = segue.destination as? NotificarViewController
            destino.dataItemSelect = dataItemSelect
            destino.itemSelect = self
        }
    }
}

extension CrearInspecconViewController : NotificarSelect{
    func sendData(lista: Array<NotificarModel>) {
        isNotificacion = true
        dataItemSelect = lista
    }
}

extension CrearInspecconViewController : ItemSpinnerSelect{
    func sendData(object: ItemSpinnerModel) {
        switch selectButton {
        case 1:
            btnProyecto.setTitle(object.nombre, for: .normal)
            idProyecto = object.id
            obtenerEmpresaProyecto(idProyecto:idProyecto!)
            
            btnContratista.setTitle("Seleccionar", for: .normal)
            idContratista = 0
            
            btnTipoUbicacion.setTitle("Seleccionar", for: .normal)
            idTipoUbicacion = 0
            
            btnLugar.setTitle("Seleccionar", for: .normal)
            idLugar = 0
            
            break
        case 2:
            btnContratista.setTitle(object.nombre, for: .normal)
            idContratista = object.id
            
            btnTipoUbicacion.setTitle("Seleccionar", for: .normal)
            idTipoUbicacion = 0
            
            btnLugar.setTitle("Seleccionar", for: .normal)
            idLugar = 0
            break
        case 3:
            print("id = \(object.id ?? 0)")
            btnTipoUbicacion.setTitle(object.nombre, for: .normal)
            idTipoUbicacion = object.id
            //edtTorre.isEnabled = object.id == 2  ? true : false
            btnLugar.setTitle("Seleccionar", for: .normal)
            idLugar = 0
            break
        case 4:
            btnLugar.setTitle(object.nombre, for: .normal)
            idLugar = object.id
            nombreLugar = object.nombre
            break
        case 5:
           // btnAreaResponsable.setTitle(object.nombre, for: .normal)
            areaInspeccionada = object.nombre
            break
        case 6:
            idUsuarioResponsable = object.id
            usuarioResponsable = ("\(object.nombre ?? "") \(object.descripcion ?? "")")
            btnUsuario.setTitle(usuarioResponsable, for: .normal)
            break
        default:
            print("Algo salio mal")
        }
    }
}


extension CrearInspecconViewController : ItemBuenaPracticaSelect{
    func sendData(lista: Array<BuenaPracticaModel>) {
       
        if lista.count > 0{
            isBuenaPractica = true
            dataBuenaPractica = lista
        }
    }
}

extension UIResponder {
    public var parentViewController: UIViewController? {
        return next as? UIViewController ?? next?.parentViewController
    }
}

extension Date {
    func localDate() -> Date {
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC) else {return Date()}
        
        return localDate
    }
}
