//
//  HallazgoViewController.swift
//  i-Safety
//
//  Created by usuario on 7/2/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class HallazgoViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    @IBOutlet weak var txtProyecto: UILabel!
    @IBOutlet weak var txtUsuario: UILabel!
    @IBOutlet weak var txtSede: UILabel!
    @IBOutlet weak var txtContratista: UILabel!
    
    var dataItem:Array<ItemHallazgoModel> = []
    var buenaPractica:Array<BuenaPracticaModel> = []
    var idInspeccion:Int?
    var inspeccion:InspeccionModel?
    var detalleInspeccion:DetalleInspeccionModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        
        if let inspeccion = inspeccion {
            txtProyecto.text = inspeccion.codigo
            txtUsuario.text = inspeccion.nombreUsuario
            txtSede.text = inspeccion.sede
            txtContratista.text = inspeccion.empresaContratista
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let id = idInspeccion {
            obtenerHallazgo(idInspeccion: id)
        }
    }
    
    func setupNavigationBarItem(){
        title = "Hallazgos"
       
        let correoImg = UIImage(named: "ico_email")
        let btnCorreo:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30, height: 20))
        btnCorreo.setImage(correoImg, for: .normal)
        btnCorreo.sizeToFit()
        btnCorreo.addTarget(self, action: #selector(addNotificador), for: UIControl.Event.touchUpInside)
        let notificarButton = UIBarButtonItem(customView: btnCorreo)
        
        navigationController?.navigationBar.isTranslucent = false
               let cancelarButton =  UIBarButtonItem(title: "crear", style: .plain, target: self, action: #selector(crearHallazgo))
               cancelarButton.tintColor = .white
        
        navigationItem.rightBarButtonItems = [notificarButton,cancelarButton]
        
    }
    
    @objc func crearHallazgo(){
        self.performSegue(withIdentifier: "sgCrearHallazgo", sender: self)
    }
    
    @objc func addNotificador(){
       
         self.performSegue(withIdentifier: "sgNotifiInspec", sender: self)
    }
    
    @IBAction func detalleInspeccion(_ sender: UIButton) {
        self.performSegue(withIdentifier: "sgDetalleInspeccion", sender: self)
    }
    
    func obtenerHallazgo(idInspeccion:Int){
        UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Cargando...")
        InspeccionViewModel.init(parent: self).detalleInspeccion(idInspeccion: idInspeccion, callback: {(data,lista,practica,error) in
            if let data = data{
                if let lista = lista{
                    self.dataItem = lista
                    DispatchQueue.main.async {
                        self.tblItem.reloadData()
                    }
                }
                self.detalleInspeccion = data
                if let bPractica = practica{
                    self.buenaPractica = bPractica
                }
                UtilMetodos.hideActivityIndicator(uiView: self.view)
            }else{
                UtilMetodos.hideActivityIndicator(uiView: self.view)
                
            }
        })
    }
    
    @IBAction func obtenerPDF(_ sender: UIButton) {
        self.performSegue(withIdentifier: "sgvisorpdf", sender: self)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgCrearHallazgo"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                // let nav = segue.destination as! UINavigationController
                // let destination = nav.topViewController as! CrearHallazgoViewController
                let destination = segue.destination as! CrearHallazgoViewController
                destination.idInspeccion = idInspeccion
                destination.itemHallazgoModel = dataItem[indexPath.row]
            }else{
                let destination = segue.destination as! CrearHallazgoViewController
                destination.idInspeccion = idInspeccion
            }
        }else if(segue.identifier == "sgvisorpdf"){
            let destination = segue.destination as! VisorPdfViewController
            destination.idInspeccion = idInspeccion
        }else if(segue.identifier == "sgDetalleInspeccion"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! CrearInspecconViewController
            //   let destination = segue.destination as! CrearInspecconViewController
            destination.practica = buenaPractica
            destination.detalleInspeccion = detalleInspeccion
        }else if(segue.identifier == "sgNotifiInspec"){
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! NotificacionInspeViewController
            destination.idInspeccion = idInspeccion
            destination.nombreproyecto = inspeccion?.sede
            destination.codigoInspeccion = inspeccion?.codigo
        }
    }
}

extension HallazgoViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemHallazgo", for: indexPath) as! ItemHallazgoTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombreHallazgo?.trimmingCharacters(in: .whitespacesAndNewlines)
        cell.txtUsuario.text = dataItem[indexPath.row].nombreActoSubEstandar?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        cell.txtFecha.text = "Plazo : \(dataItem[indexPath.row].plazo ?? "")"
        cell.txtTipoGestion.text = dataItem[indexPath.row].nombreTipoGestion?.trimmingCharacters(in: .whitespacesAndNewlines)
        cell.txtTipoGestion.textColor = UIColor(cgColor: #colorLiteral(red: 0.05900000036, green: 0.1410000026, blue: 0.3019999862, alpha: 1))
    
        switch dataItem[indexPath.row].riesgo {
        case 1:
            cell.vFranja.backgroundColor = UIColor(cgColor: #colorLiteral(red: 0, green: 0.5215686275, blue: 0.02352941176, alpha: 1))
        case 2:
            cell.vFranja.backgroundColor = UIColor(cgColor: #colorLiteral(red: 0.862745098, green: 0.7764705882, blue: 0, alpha: 1))
        case 3:
            cell.vFranja.backgroundColor = UIColor(cgColor: #colorLiteral(red: 0.8039215686, green: 0.137150115, blue: 0.03121691928, alpha: 1))
        default:
            print("default")
        }
        
        cell.txtEstado.text = (dataItem[indexPath.row].estadoDetalleInspeccion ?? false ? "Cerrado" : "Abierto")
        cell.txtEstado.textColor = (dataItem[indexPath.row].estadoDetalleInspeccion ?? false ?  UIColor(cgColor: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)) :  UIColor(cgColor: #colorLiteral(red: 0.8039215686, green: 0.137150115, blue: 0.03121691928, alpha: 1)))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgCrearHallazgo", sender: self)
    }
}
