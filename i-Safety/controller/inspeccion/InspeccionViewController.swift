//
//  InspeccionViewController.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit
import SVProgressHUD

class InspeccionViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    @IBOutlet weak var btnFiltro: UIButton!
    var dataItem:Array<InspeccionModel> = []
    var dataItemOriginal:Array<InspeccionModel> = []
    var idRolUsario:Int?
    
    lazy var refresh: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor(cgColor: #colorLiteral(red: 0.9959999919, green: 0.3140000105, blue: 0, alpha: 1))
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadInspecciones()
        tblItem.refreshControl = refresh
        idRolUsario = UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int
    }
    
    
    func filtroProProyecto(idProyecto:Int){
        if(idProyecto == 9999){
            self.dataItem = dataItemOriginal
            self.tblItem.reloadData()
        }else{
            let filtered = dataItem.filter { (InspeccionModel) -> Bool in
                InspeccionModel.idProyecto == idProyecto
            }
            self.dataItem = filtered
            self.tblItem.reloadData()
        }
    }
    
    func loadInspecciones(){
        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                                   "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
                                   "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        
        InspeccionViewModel.init(parent: self).listaInpecciones(params: params, callback: {(data,error) in
            if let data = data{
                if((data.count) > 0){
                    self.dataItem = data
                    self.dataItemOriginal = data
                    DispatchQueue.main.async {
                        self.tblItem.reloadData()
                    }
                }
            }
        })
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.topItem?.title = "Inspecciones"
        let crearButton =  UIBarButtonItem(title: "crear", style: .plain, target: self, action: #selector(addNewTicket))
        let perfilButton =  UIBarButtonItem(title: "Menu", style: .plain, target: self, action: #selector(showMenu))
        
        crearButton.tintColor = .white
        navigationItem.rightBarButtonItem = crearButton
        navigationItem.leftBarButtonItem = perfilButton
    }
    
    @objc func showMenu(){
        self.performSegue(withIdentifier: "sgShowMenu", sender: self)
    }
    
    @objc func addNewTicket(){
        self.performSegue(withIdentifier: "sgcrearinspeccion", sender: self)
    }
    
    @objc func requestData(){
        let deadLine = DispatchTime.now() + .milliseconds(1000)
        DispatchQueue.main.asyncAfter(deadline: deadLine){
            self.loadInspecciones()
            self.btnFiltro.setTitle("TODO", for: .normal)
            self.refresh.endRefreshing()
        }
    }
    
    @IBAction func filtroProyecto(_ sender: UIButton) {
        self.performSegue(withIdentifier: "sgFiltro", sender: nil)
    }
    
    private func eliminarInspeccion(indexPath:IndexPath){
        let params:NSDictionary = ["idInspecion": self.dataItem[indexPath.row].id ?? 0]
        InspeccionViewModel.init(parent: self).eliminarInspeccion(params:params, callback: {(success,error)in
            if let result = success, result{
                self.dataItem.remove(at: indexPath.row)
                self.tblItem.beginUpdates()
                self.tblItem.deleteRows(at: [indexPath], with: .automatic)
                self.tblItem.endUpdates()
            }else{
                print("Ocurrio un error no elimino")
            }
        })
    }
    
    private func showModalEliminar(index:IndexPath){
        
        let alert = UIAlertController(title: "Información", message: "¿Está seguro de eliminar el registro de Inspección?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) -> Void in
            self.eliminarInspeccion(indexPath: index)
        })
        alert.addAction(ok)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgObtenerHallazgo"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! HallazgoViewController
                destination.idInspeccion = dataItem[indexPath.row].id
                destination.inspeccion = dataItem[indexPath.row]
                //destination.categoriaNombre = self.dataPendientes[indexPath.row].categoriaDocumentoNombre
            }
        }else if (segue.identifier == "sgFiltro"){
            let destination = segue.destination as! FiltroViewController
            destination.itemSelect = self
        }else if (segue.identifier == "sgcrearinspeccion"){
            //  let destination = segue.destination as! CrearInspecconViewController
            let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! CrearInspecconViewController
            destination.selectItem = self
        }else if (segue.identifier == "sgShowMenu"){
            let destination = segue.destination as! MenuViewController
            destination.delegate = self
            
        }
    }
}

extension InspeccionViewController : FiltroSelect{
    func sendData(object: ItemSpinnerModel) {
        btnFiltro.setTitle(object.nombre, for: .normal)
        filtroProProyecto(idProyecto: object.id!)
    }
}

extension InspeccionViewController : CerrarSessionProtocol{
    func closeApp(success: Bool) {
        if let bundle = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundle)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension InspeccionViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InspeccionCell", for: indexPath) as! InspeccionTableViewCell
        cell.txtCodigo.text = dataItem[indexPath.row].codigo?.capitalized
        cell.txtUsuario.text = dataItem[indexPath.row].nombreUsuario
        cell.txtFecha.text = dataItem[indexPath.row].fechaRegistro
        cell.txtFecha.textColor = .gray
        cell.txtEstado.text = dataItem[indexPath.row].estado == 1 ? "Abierto":"Cerrado"
        cell.txtEstado.textColor = dataItem[indexPath.row].estado == 1 ? UIColor(cgColor: #colorLiteral(red: 0.9960784314, green: 0.3137254902, blue: 0, alpha: 1)) : .gray
        cell.txtSede.text = dataItem[indexPath.row].sede
        cell.txtProyecto.text = dataItem[indexPath.row].empresaContratista
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgObtenerHallazgo", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if(idRolUsario == 1){
            return .delete
        }
        return .none
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("debe eliminar")
            showModalEliminar(index: indexPath)
        }
    }
    
}

extension InspeccionViewController : InspeccionProtocol{
    func sendData(loadTable: Bool) {
        print(" iniciar  loadInspecciones() ")
        loadInspecciones()
    }
}
