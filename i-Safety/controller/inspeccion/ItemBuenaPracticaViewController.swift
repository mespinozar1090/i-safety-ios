//
//  ItemBuenaPracticaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/7/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol ItemBPSelect {
    func sendData(object:BuenaPracticaModel,delete:Bool)
}

class ItemBuenaPracticaViewController: UIViewController {
    
    @IBOutlet weak var tfDescripcion: UITextView!
    @IBOutlet weak var imgFoto: UIImageView!
    @IBOutlet weak var btnSelectItem: UIButton!
    @IBOutlet weak var viewImage: UIView!
    
    var imagePicker = UIImagePickerController()
    var items = BuenaPracticaModel()
    var itemSelect:ItemBPSelect?
    var imagenBase64:String = ""
    var idCategoriaBuenaPractica:Int?
    var nombreCategoria:String?
    var object:BuenaPracticaModel?
    var isDelete:Bool = false
    var isNuevo:Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setupButton(button: btnSelectItem)
        print("es nuevo ? \(isNuevo)")
        if(!isNuevo){
            validarObject()
        }
       
    }
    
    private func validarObject(){
        if let data = object{
            isDelete = true
            imagenBase64 = data.imgBase64 ?? ""
            tfDescripcion.text = data.descripcion
            btnSelectItem.setTitle(data.nombre, for: .normal)
            idCategoriaBuenaPractica = data.idCategoriaBuenaPractica
            if let img = data.imgBase64,img.contains("base64,") {
                viewImage.isHidden = false
                let fullNameArr = img.components(separatedBy: "base64,")
                imgFoto.image = UtilMetodos.convertBase64StringToImage(imageBase64String: fullNameArr[1])
            }else{
                if let imgUrl = data.imgBase64{
                    imgFoto.downloaded(from: imgUrl)
                }
                viewImage.isHidden = false
            }
        }
    }
    
    private func setupButton(button:UIButton){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func accionGuardar(_ sender: UIButton) {
        if(validarCampos()){
            items.descripcion = tfDescripcion.text
                   items.imgBase64 = imagenBase64
                   items.idCategoriaBuenaPractica = idCategoriaBuenaPractica
                   items.nombre = nombreCategoria
                   itemSelect?.sendData(object: items,delete: isDelete)
                   navigationController?.popViewController(animated: true)
        }
       
    }
    
    @IBAction func accionSelectItem(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadBuenaPracticaViewController") as! LoadBuenaPracticaViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func captureFoto(_ sender: UIButton) {
        takePhoto()
    }
    
    @IBAction func tapSelectImage(_ sender: UIButton) {
        getPhotoGallery()
    }
    
    func takePhoto() {
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    func getPhotoGallery() {
       imagePicker.sourceType = .savedPhotosAlbum // or .photoLibrary
               imagePicker.delegate = self
               imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func validarCampos() -> Bool{
        guard (idCategoriaBuenaPractica != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione una categoria", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (tfDescripcion.text != nil && !tfDescripcion.text.isEmpty) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "El campo descripción es obligatorio", titleAction: "Aceptar")
              self.present(alert,animated: true,completion: nil)
            return false
        }
        
        return true
    }
}
extension ItemBuenaPracticaViewController : BuenaPracticaProtocol{
    func sendData(object: BuenaPracticaModel) {
        btnSelectItem.setTitle(object.nombre, for: .normal)
        idCategoriaBuenaPractica =  object.idCategoriaBuenaPractica
        nombreCategoria = object.nombre
    }
}

extension ItemBuenaPracticaViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[.originalImage] as? UIImage {
            let newImage = resizeImage(image: image, newWidth: 200)
            imgFoto.image = newImage
            imagenBase64 = "data:image/jpeg;base64,\(UtilMetodos.convertImageToBase64(image: newImage!))"
            viewImage.isHidden = false
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
