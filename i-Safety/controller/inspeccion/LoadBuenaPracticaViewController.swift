//
//  LoadBuenaPracticaViewController.swift
//  i-Safety
//
//  Created by usuario on 11/3/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol BuenaPracticaProtocol {
    func sendData(object:BuenaPracticaModel)
}

class LoadBuenaPracticaViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<BuenaPracticaModel> = []
    var delegate:BuenaPracticaProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obtenerBuenaPractica()
    }
    
    func obtenerBuenaPractica(){
        InspeccionViewModel.init(parent: self).obtenerBuenaPractica(callback: {(data,error) in
            if let result = data, result.count > 0 {
                self.dataItem = result
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
}

extension LoadBuenaPracticaViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoadBuenaPracticaTableViewCell", for: indexPath) as! LoadBuenaPracticaTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.sendData(object: dataItem[indexPath.row])
         navigationController?.popViewController(animated: true)
    }
}
