//
//  BuenaPracticaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol ItemBuenaPracticaSelect {
    func sendData(lista:Array<BuenaPracticaModel>)
}

class BuenaPracticaViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var tblItem:UITableView!
    var imagePicker = UIImagePickerController()
    var dataItem:Array<BuenaPracticaModel> = []
    var dataItemNuevo:Array<BuenaPracticaModel> = []
    var indexSelect:Int = 0
    var objectBP:BuenaPracticaModel?
    var isNuevo:Bool = false
    
    var items:Array<BuenaPracticaModel>?
    var itemSelectBP:ItemBuenaPracticaSelect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        if let data = items{
            dataItem = data
            DispatchQueue.main.async {
                self.tblItem.reloadData()
            }
        }
    }
    
    func setupNavigationBarItem(){
        title = "Buenas Prácticas"
        let guardarButton =  UIBarButtonItem(title: "Agregar", style: .plain, target: self, action: #selector(agrearAccion))
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false,animated:true)
        navigationItem.rightBarButtonItem = guardarButton
    }
    
    @objc func agrearAccion(){
        isNuevo = true
        self.performSegue(withIdentifier: "sgCrearBP", sender: nil)
    }
    
    @objc func accionShowImage(sender: UIButton) {
        indexSelect = sender.tag
        self.performSegue(withIdentifier: "sgShowImage", sender: nil)
    }
    @IBAction func accionSave(_ sender: UIButton) {
        itemSelectBP?.sendData(lista: dataItem)
        navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgShowImage" ){
            let destination = segue.destination as! ImageViewController
            destination.imgBase64 = dataItem[indexSelect].imgBase64
        }else if(segue.identifier == "sgCrearBP" ){
            let destination = segue.destination as! ItemBuenaPracticaViewController
            destination.object = objectBP
            destination.isNuevo = isNuevo
            destination.itemSelect = self
        }
    }
}

extension BuenaPracticaViewController: ItemBPSelect{
    func sendData(object: BuenaPracticaModel, delete: Bool) {
        dataItem.append(object)
        if(delete){
            dataItem.remove(at: indexSelect)
        }
        self.tblItem.reloadData()
    }
}

extension BuenaPracticaViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "buenaPracticaCell", for: indexPath) as! BuenaPracticaTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre
        cell.txtMessage.text =  dataItem[indexPath.row].descripcion
        cell.txtMessage.isEditable =  false
        cell.btnImage.tag = indexPath.row
        cell.btnImage.addTarget(self, action: #selector(accionShowImage(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isNuevo = false
        objectBP = dataItem[indexPath.row]
        indexSelect = indexPath.row
        self.performSegue(withIdentifier: "sgCrearBP", sender: nil)
    }
    
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}
