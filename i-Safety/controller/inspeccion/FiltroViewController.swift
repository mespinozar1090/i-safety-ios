//
//  FiltroViewController.swift
//  i-Safety
//
//  Created by usuario on 8/11/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol FiltroSelect {
    func sendData(object:ItemSpinnerModel)
}

class FiltroViewController: UIViewController {
    
    @IBOutlet weak var tblItem:UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    var itemSelect:FiltroSelect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadObtenerProyecto()
    }
    
    func loadObtenerProyecto(){
        let params:NSDictionary = ["Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        InspeccionViewModel.init(parent: self).obtenerProyecto2(params: params, callback: {(data,error) in
            if let array = data {
                self.dataItem = array
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
}

extension FiltroViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "FiltroTableViewCell", for: indexPath) as! FiltroTableViewCell
        cell.txtNombreProyecto.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemSelect?.sendData(object: dataItem[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }

}

