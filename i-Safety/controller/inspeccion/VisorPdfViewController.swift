//
//  VisorPdfViewController.swift
//  i-Safety
//
//  Created by usuario on 7/7/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit
import PDFKit

class VisorPdfViewController: UIViewController {
    
    @IBOutlet weak var pdfView:PDFView!
    var idInspeccion:Int?
    var urlDocumentoPdf:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
    }
    
    private func setupNavigationBarItem(){
        self.title = "Visor PDF"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.isTranslucent = false
    }
    
    @objc func cancelarAccion(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        urlDocumentoPdf = "\(Constante.BASE_URL_WEB)Inspeccion/VisualizarPDF?idInspeccion=\(idInspeccion ?? 0)"
        if let urldocumento = urlDocumentoPdf{
            DispatchQueue.main.async {
                let docUrl = URL.init(string: urldocumento)
                let pdfDocument = PDFDocument(url: docUrl!)
                self.pdfView.autoScales = true
                self.pdfView.displayMode = .singlePageContinuous
                self.pdfView.displayDirection = .vertical
                self.pdfView.document = pdfDocument
            }
        }
        
    }
}
