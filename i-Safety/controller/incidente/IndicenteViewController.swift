//
//  IndicenteViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class IndicenteViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<IncidenteModel> = []
    
    
    var datosGeneral = GeneralIncidenteModel()
    var datosEvento = EventoIncidenteModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupNavigationBarItem()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
         listarIncidente()
    }
    
    func listarIncidente(){
        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
                                         "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
                                         "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int,
                                         "Tipo_Formato": "PRELIMINAR"]
        
        IncidenteViewModel.init(parent: self).listarIncidente(params : params,callback: {(success,error) in
            if let lista =  success{
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.topItem?.title = "Incidente / Accidente"
        let crearButton =  UIBarButtonItem(title: "crear", style: .plain, target: self, action: #selector(crearIncidente))
        crearButton.tintColor = .white
        navigationItem.rightBarButtonItem = crearButton
    }
    
    @objc func crearIncidente(){
        datosGeneral = GeneralIncidenteModel()
        datosEvento = EventoIncidenteModel()
        self.performSegue(withIdentifier: "sgCrearIncidente", sender: self)
    }
    
    private func verRegistroIncidente(idIncidente:Int,tipoInforme:String){
        let params:NSDictionary = ["idIncidente": idIncidente, "tipoInforme": tipoInforme]
        IncidenteViewModel.init(parent: self).verRegistroIncidente(params: params, callback: {(dataGeneral,dataEvento,error)in
            if let general = dataGeneral{
                self.datosGeneral = general
                if let evento = dataEvento{
                    self.datosEvento = evento
                    self.performSegue(withIdentifier: "sgCrearIncidente", sender: self)
                }
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgCrearIncidente"){
           let nav = segue.destination as! UINavigationController
            let destination = nav.topViewController as! CrearIncidenteViewController
            destination.datosGenerales = datosGeneral
            destination.datosEvento = datosEvento
        }
    }
}

// MARK:- Configuracion table
extension IndicenteViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IncidenteTableViewCell", for: indexPath) as! IncidenteTableViewCell
       
        if let idIncidente = dataItem[indexPath.row].idIncidente{
            cell.txtCodigo.text = "INCI-\(idIncidente)"
        }
    
        cell.txtFormato.text =  dataItem[indexPath.row].gDescripcionFormato
        cell.txtFecha.text =  dataItem[indexPath.row].gFechaCreado
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        verRegistroIncidente(idIncidente: dataItem[indexPath.row].idIncidente ?? 0 ,tipoInforme: dataItem[indexPath.row].tipoInforme ?? "")
       
       // print("id",dataItem[indexPath.row].idIncidente)
    }
}
