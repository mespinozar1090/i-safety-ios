//
//  LoadEmpresaViewController.swift
//  i-Safety
//
//  Created by usuario on 10/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LoadEmpresaProtocol {
    func sendData(object:EmpresaModel)
}

class LoadEmpresaViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<EmpresaModel> = []
    var delegate: LoadEmpresaProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obtenerEmpresa()
    }
    
    func obtenerEmpresa(){
        IncidenteViewModel.init(parent: self).obtenerEmpresa(callback: {(success,data) in
            if let lista = success{
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
}

//MARK: - Extension Table Load
extension LoadEmpresaViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoadEmpresaTableViewCell", for: indexPath) as! LoadEmpresaTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].razonsocial
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = dataItem[indexPath.row]
        delegate?.sendData(object: object)
        navigationController?.popViewController(animated: true)
    }
    
}

