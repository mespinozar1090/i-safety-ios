//
//  EventoIncidenteViewController.swift
//  i-Safety
//
//  Created by usuario on 9/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol EventoIncidenteProcotol {
    func sendData(object:EventoIncidenteModel)
}

class EventoIncidenteViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnSedeProyecto: ButtonWithImage!
    @IBOutlet weak var btnTipoEvento: ButtonWithImage!
    @IBOutlet weak var btnHuboDanio: ButtonWithImage!
    @IBOutlet weak var txtNumeroTrabajadores: SDCTextField!
    @IBOutlet weak var txfFechaAccidente: UITextField!
    @IBOutlet weak var txtHoraAccidente: UITextField!
    @IBOutlet weak var txtLugar: UITextField!
    @IBOutlet weak var btnParteCuerpo: ButtonWithImage!
    @IBOutlet weak var btnEquipoAfectado: ButtonWithImage!
    
    let datePicker = UIDatePicker()
    let datePickerHora = UIDatePicker()
    var eventoIncidente = EventoIncidenteModel()
    var delegate:EventoIncidenteProcotol?
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    
    //Detalle
    var objectEvento:EventoIncidenteModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        showDatePicker()
        showDatePickerHora()
        setupButton(button: btnSedeProyecto)
        setupButton(button: btnTipoEvento)
        setupButton(button: btnHuboDanio)
        setupButton(button: btnParteCuerpo)
        setupButton(button: btnEquipoAfectado)
        
        txtNumeroTrabajadores.delegate = self
        txtNumeroTrabajadores.maxLength = 5
        loadDetalle()
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
        button.setTitle("Seleccione", for:.normal)
       
    }
    
    private func loadDetalle(){
        if let data = objectEvento , (data.numeroTrabajadoresAfectados != nil){
            eventoIncidente = data
            txtNumeroTrabajadores.text = data.numeroTrabajadoresAfectados
            btnSedeProyecto.setTitle(data.nombreSedeProyecto, for: .normal)
            btnTipoEvento.setTitle(data.nombreTipoEvento, for: .normal)
            btnHuboDanio.setTitle(data.huboDanioMaterial, for: .normal)
        
            txfFechaAccidente.text = data.fechaAccidente
            txtHoraAccidente.text =  data.horaAccidente
            txtLugar.text = data.lugarExacto
            btnParteCuerpo.setTitle(data.nombreParteAfectada, for: .normal)
            btnEquipoAfectado.setTitle(data.nombreEquipoAfectado, for: .normal)
        }
    }
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cerrar", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        txfFechaAccidente.inputAccessoryView = toolbar
        txfFechaAccidente.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        txfFechaAccidente.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showDatePickerHora(){
        datePickerHora.datePickerMode = .time
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePickerHora));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cerrar", style: .plain, target: self, action: #selector(cancelDatePickerHora));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        txtHoraAccidente.inputAccessoryView = toolbar
        txtHoraAccidente.inputView = datePickerHora
    }
    
    @objc func donedatePickerHora(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        txtHoraAccidente.text = formatter.string(from: datePickerHora.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePickerHora(){
        self.view.endEditing(true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    @IBAction func tapSedeProyecto(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 4
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func tapTipoEvento(_ sender: UIButton) {
        IncidenteViewModel.init(parent: self).obtenerComboMestro(parametro:"4",callback: {(success,data) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 5
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func tapHuboDanio(_ sender: UIButton) {
        self.dataItem = UtilMetodos.huboDanio()
        self.buttonSelect = 6
        self.sendDataTable()
    }
    
    @IBAction func tapParteCuerpo(_ sender: UIButton) {
        IncidenteViewModel.init(parent: self).obtenerComboMaestroGenerico(nombreTable:"tb_MAESTRA_ParteCuerpo",callback: {(success,data) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 7
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func tapEquipoAfectado(_ sender: UIButton) {
        IncidenteViewModel.init(parent: self).obtenerComboMaestroGenerico(nombreTable:"tb_MAESTRA_Equipos",callback: {(success,data) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 8
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func btnGuardar(_ sender: UIButton) {
        if(validarCampos()){
            eventoIncidente.numeroTrabajadoresAfectados = txtNumeroTrabajadores.text
            print(" numero : \(eventoIncidente.numeroTrabajadoresAfectados)")
            print(" idTipoEvento : \(eventoIncidente.idTipoEvento)")
            eventoIncidente.fechaAccidente = txfFechaAccidente.text
            eventoIncidente.horaAccidente = txtHoraAccidente.text
            eventoIncidente.lugarExacto = txtLugar.text
            eventoIncidente.fDescripcionIncidente = txfFechaAccidente.text
            eventoIncidente.trabajoDetalleCategoriaOcupacional = ""
            eventoIncidente.fechaInicioInvestigacion  = txfFechaAccidente.text
            delegate?.sendData(object: eventoIncidente)
        }
    }
    
    private func validarCampos()-> Bool{
        guard (eventoIncidente.idSedeProyecto != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione sede/proyecto", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (eventoIncidente.idTipoEvento != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione tipo evento", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (eventoIncidente.huboDanioMaterial != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione si hubo dañio material", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let nroTrabajadores = txtNumeroTrabajadores.text, !nroTrabajadores.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo numero de trabajadores es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let fechaAccidente = txfFechaAccidente.text, !fechaAccidente.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo fecha es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let horaAccidente = txtHoraAccidente.text, !horaAccidente.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo hora es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let lugar = txtLugar.text, !lugar.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo lugar es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (eventoIncidente.idParteAfectada != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione parte de cuerpo afectada", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (eventoIncidente.idEquipoAfectado != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione equipo afectado", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        return true
    }
    
    func sendDataTable(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadIncidenteViewController") as! LoadIncidenteViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension EventoIncidenteViewController :LoadIncidenteProtocol{
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 4:
            btnSedeProyecto.setTitle(object.nombre, for: .normal)
            eventoIncidente.idSedeProyecto = object.id
            eventoIncidente.descripcionSede = object.nombre
            break
        case 5:
            btnTipoEvento.setTitle(object.nombre, for: .normal)
            eventoIncidente.idTipoEvento = object.id
            break
        case 6:
            btnHuboDanio.setTitle(object.nombre, for: .normal)
            eventoIncidente.huboDanioMaterial = object.nombre
            break
        case 7:
            btnParteCuerpo.setTitle(object.nombre, for: .normal)
            eventoIncidente.idParteAfectada = object.id
            break
        case 8:
            btnEquipoAfectado.setTitle(object.nombre, for: .normal)
            eventoIncidente.idEquipoAfectado = object.id
            break
        default:
            print("Algo salio mal")
        }
    }
}
