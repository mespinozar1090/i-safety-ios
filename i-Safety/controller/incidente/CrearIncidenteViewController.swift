//
//  CrearIncidenteViewController.swift
//  i-Safety
//
//  Created by usuario on 9/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class CrearIncidenteViewController: UIViewController {
    
    @IBOutlet weak var eventoController: UIView!
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var datosGeneralController: UIView!
    var notificar:Array<NotificarModel> = []
    var datosGenerales:GeneralIncidenteModel?
    var datosEvento:EventoIncidenteModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmented.isEnabled = false
        hideKeyboardWhenTappedAround()
        setupNavigationBarItem()
        segmented.selectedSegmentIndex = 0
        datosGeneralController.alpha = 1
        eventoController.alpha = 0
        
    }
    
    @IBAction func tapSegmented(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            segmented.isEnabled = false
            datosGeneralController.alpha = 1
            eventoController.alpha = 0
        case 1:
            datosGeneralController.alpha = 0
            eventoController.alpha = 1
        default:
            print("Default")
        }
    }
    
    func setupNavigationBarItem(){
        title = "Incidente/Accidente"
        let cancelarButton =  UIBarButtonItem(title: "cerrar", style: .plain, target: self, action: #selector(cancelarAccion))
        cancelarButton.tintColor = .white
        
        let correoImg = UIImage(named: "ico_email")
        let btnCorreo:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30, height: 20))
        btnCorreo.setImage(correoImg, for: .normal)
        btnCorreo.sizeToFit()
        btnCorreo.addTarget(self, action: #selector(addNotificador), for: UIControl.Event.touchUpInside)
        let notificarButton = UIBarButtonItem(customView: btnCorreo)
        
        self.navigationItem.rightBarButtonItem = notificarButton
        self.navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func addNotificador(){
        self.performSegue(withIdentifier: "sgNotificarInci", sender: self)
    }
    
    @objc func cancelarAccion(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func crearRegistroIncidente(){
        guard notificar.count > 0 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe seleccionar almenos 1 usuario a notificar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return
        }
        UtilMetodos.showActivityIndicatorV2(uiView: self.view, title: "Guardando registro...")
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        let horaFormate = DateFormatter()
        horaFormate.dateFormat = "HH:mm:ss"
        
        let filterNotificar = notificar.filter { (item) -> Bool in
            item.selectEmail == true
        }
        
        let rolUsario:String = UserDefaults.standard.value(forKey: "Descripcion_Rol") as! String
        let nombreUsuario:String = UserDefaults.standard.value(forKey: "nombreUsuario") as! String
        let nombreFormato:String = "Incidente- \(rolUsario) - \(nombreUsuario) - \(formatter.string(from: Date()))";
        
        var notificadores:Array<NSDictionary> = []
        for index in 0...filterNotificar.count-1 {
            let paramsNotificar:NSDictionary = [
                "IdUser": filterNotificar[index].idUsers ?? 0 as Int,
                "Nombre": filterNotificar[index].nombreUsuario ?? "" as String,
                "Email": filterNotificar[index].emailCorporativo ?? "" as String,
            ]
            notificadores.append(paramsNotificar)
        }
        
        print("notificados : \(notificadores)")
        
        let params:NSDictionary = [
            "Id_Incidente": datosGenerales?.idIncidente ?? 0 as Int ,
            "Empleador_Tipo": (datosGenerales?.empleadorTipo ?? "") as String ,
            "F_DESCRIPCION_Incidente": (datosGenerales?.fDescripcionIncidente ?? "") as String,
            "F_Empleador_IdActividadEconomica": (datosGenerales?.fEmpleadorIdActividadEcomica ?? 0) as Int,
            "F_Empleador_IdEmpresa": (datosGenerales?.fEmpleadorIdEmpresa ?? 0 ) as Int,
            "F_Empleador_IdTamanioEmpresa": (datosGenerales?.fEmpleadorIdTamanioEmpresa ?? 0) as Int,
            "F_EmpleadorI_IdActividadEconomica": (datosGenerales?.fEmpleadorInterIdActividadEconomica ?? 0) as Int,
            "F_EmpleadorI_RazonSocial": (datosGenerales?.fEmpleadorInterRazonSocial ?? "") as String,
            "F_EmpleadorI_RUC": (datosGenerales?.fEmpleadorInterRuc ?? "") as String,
            "F_Empleador_RazonSocial": (datosGenerales?.fEmpleadorRazonSocial ?? "") as String,
            "F_Empleador_RUC": (datosGenerales?.fEmpleadorRuc ?? "") as String,
            "F_Trabajador_NombresApellidos": (datosGenerales?.fTrabajadorNombresApellidos ?? "") as String,
            
            "F_EVENTO_FechaAccidente": (datosEvento?.fechaAccidente ?? "") as String,
            "F_EVENTO_FechaInicioInvestigacion": (datosEvento?.fechaInicioInvestigacion ?? "") as String,
            "F_EVENTO_HoraAccidente": (datosEvento?.horaAccidente ?? "") as String,
            "F_EVENTO_HuboDanioMaterial": (datosEvento?.huboDanioMaterial ?? "") as String,
            "F_EVENTO_IdEquipoAfectado": (datosEvento?.idEquipoAfectado ?? 0) as Int,
            "F_EVENTO_IdParteAfectada": (datosEvento?.idParteAfectada ?? 0) as Int,
            "F_EVENTO_IdTipoEvento": (datosEvento?.idTipoEvento ?? 0) as Int,
            "F_EVENTO_LugarExacto": (datosEvento?.lugarExacto ?? "") as String,
            "F_EVENTO_NumTrabajadoresAfectadas": (datosEvento?.numeroTrabajadoresAfectados ?? "") as String,
            "F_Trabajador_DetalleCategoriaOcupacional": (datosEvento?.trabajoDetalleCategoriaOcupacional ?? "") as String,
            "G_Fecha_Creado": formatter.string(from: Date()) as String,
            "G_Fecha_Modifica": formatter.string(from: Date()) as String,
            "G_IdProyecto_Sede": (datosEvento?.idSedeProyecto ?? 0) as Int,
            "G_IdUsuario_Creado": UserDefaults.standard.value(forKey: "idUsers") as! Int,
            "G_IdUsuario_Modifica": UserDefaults.standard.value(forKey: "idUsers") as! Int,
            "Tipo_InformeP_F": validarTipoInforme(),
            "G_DescripcionFormato": nombreFormato as String]
        
        
        if(datosGenerales?.idIncidente ?? 0 > 0){
            IncidenteViewModel.init(parent: self).editarRegistroIncidente(params: params, notificar: notificadores, callback: {(success,data)in
                if let data = success, data == 1{
                      UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                    self.dismiss(animated: true, completion: nil)
                }else{
                      UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                }
            })
        }else{
            IncidenteViewModel.init(parent: self).crearRegistroIncidente(params: params, notificar: notificadores, callback: {(success,data)in
                if let data = success, data == 1{
                      UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                    self.dismiss(animated: true, completion: nil)
                }else{
                      UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                }
            })
        }
        
        
    }
    
    
    private func validarTipoInforme() ->String{
        if let idIncidente = datosGenerales?.idIncidente, idIncidente > 0 {
            if let tipoInforme = datosGenerales?.tipoInforme, tipoInforme == "PRELIMINAR" ||  tipoInforme == "FINAL" {
                return "FINAL"
            }
        }else{
            return "PRELIMINAR"
        }
        return "PRELIMINAR"
    }
    
    // MARK: - Navigation
    //sgGeneralIncidente
    // sgEventoIncidente
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgNotificarInci"){
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NotificarViewController
            destino.dataItemSelect = notificar
            destino.itemSelect = self
        }else if (segue.identifier == "sgGeneralIncidente"){
            let destination = segue.destination as! GeneralIncidenteViewController
            destination.datosGenerales = datosGenerales
            destination.delegate = self
        }else if (segue.identifier == "sgEventoIncidente"){
            let destination = segue.destination as! EventoIncidenteViewController
            destination.objectEvento = datosEvento
            destination.delegate = self
        }
    }
    
}

extension CrearIncidenteViewController : NotificarSelect{
    func sendData(lista: Array<NotificarModel>) {
        notificar = lista
    }
}

extension CrearIncidenteViewController : GeneralIncidenteProtocol{
    func sendData(object: GeneralIncidenteModel) {
        datosGenerales = object
        segmented.selectedSegmentIndex = 1
        segmented.isEnabled = true
        datosGeneralController.alpha = 0
        eventoController.alpha = 1
    }
}

extension CrearIncidenteViewController : EventoIncidenteProcotol{
    func sendData(object: EventoIncidenteModel) {
        datosEvento =  object
        crearRegistroIncidente()
    }
    
    
}
