//
//  GeneralIncidenteViewController.swift
//  i-Safety
//
//  Created by usuario on 9/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol GeneralIncidenteProtocol {
    func sendData(object:GeneralIncidenteModel)
}

class GeneralIncidenteViewController: UIViewController,UITextFieldDelegate {
    
    //REP
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var btnRazonSocialR: ButtonWithImage!
    @IBOutlet weak var tfRucR: SDCTextField!
    @IBOutlet weak var tfTamanioR: UITextField!
    @IBOutlet weak var tfActividadEcoR: UITextField!
    @IBOutlet weak var cardViewREP: UIView!
    
    //CONTRATISTA
    @IBOutlet weak var tfRazonSocialC: UITextField!
    @IBOutlet weak var tfRucC: SDCTextField!
    @IBOutlet weak var btnTamanioEmpresaC: ButtonWithImage!
    @IBOutlet weak var btnActividadEcoC: ButtonWithImage!
    @IBOutlet weak var cardViewContra: UIView!
    
    //INTERMEDIACION
    @IBOutlet weak var tfRazonSocialI: UITextField!
    @IBOutlet weak var tfRucI: SDCTextField!
    @IBOutlet weak var btnActividadEcoI: ButtonWithImage!
    @IBOutlet weak var tfNombreTrabajadorI: UITextField!
    @IBOutlet weak var tvDecripcionI: UITextView!
    
    var empresas:Array<EmpresaModel> = []
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    
    var objectGeneral = GeneralIncidenteModel()
    var delegate:GeneralIncidenteProtocol?
    
    var isREP:Bool = true
    
    //Detalle
    var datosGenerales:GeneralIncidenteModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton(button: btnTamanioEmpresaC)
        setupButton(button: btnActividadEcoC)
        setupButton(button: btnActividadEcoI)
        setupButton(button: btnRazonSocialR)
        self.objectGeneral.empleadorTipo = "REP"
        
        tfRucR.isEnabled = false
        tfTamanioR.isEnabled = false
        tfActividadEcoR.isEnabled = false
        
        cardViewREP.isHidden = false
        cardViewContra.isHidden = true
        tfRucI.delegate = self
        tfRucI.maxLength = 11
        
        tfRucC.delegate = self
        tfRucC.maxLength = 11
        
        loadDetalle()
    }
    
    private func loadDetalle(){
        if let data =  datosGenerales{
            objectGeneral = data
            if(data.empleadorTipo == "REP"){
                segmented.selectedSegmentIndex = 1
                btnRazonSocialR.setTitle(data.fEmpleadorRazonSocial, for:.normal)
                tfRucR.text = data.fEmpleadorRuc
                tfTamanioR.text = data.fNombreTamnioEmpresa
                tfActividadEcoR.text = data.nombreActividadEconomica
                isREP = true
                cardViewREP.isHidden = false
                cardViewContra.isHidden = true
            }else{
                segmented.selectedSegmentIndex = 0
                tfRucC.text = data.fEmpleadorRuc
                tfRazonSocialC.text = data.fEmpleadorRazonSocial
                btnTamanioEmpresaC.setTitle(data.fNombreTamnioEmpresa, for:.normal)
                btnActividadEcoC.setTitle(data.nombreActividadEconomica, for: .normal)
                isREP = false
                cardViewREP.isHidden = true
                cardViewContra.isHidden = false
            }
            
            tfRazonSocialI.text = data.fEmpleadorInterRazonSocial
            tfRucI.text =  data.fEmpleadorInterRuc
            btnActividadEcoI.setTitle(data.interNombreActividadEco, for: .normal)
            tfNombreTrabajadorI.text = data.fTrabajadorNombresApellidos
            tvDecripcionI.text = data.fDescripcionIncidente
        }else{
            segmented.selectedSegmentIndex = 1
            isREP = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let sdcTextField = textField as? SDCTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return false
    }
    
    @IBAction func segmentedTap(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            cardViewREP.isHidden = true
            cardViewContra.isHidden = false
            self.objectGeneral.empleadorTipo = "CONTRATISTA"
            isREP = false
        case 1:
            isREP = true
            cardViewREP.isHidden = false
            cardViewContra.isHidden = true
            self.objectGeneral.empleadorTipo = "REP"
            tfRazonSocialC.text = ""
            tfRucC.text = ""
        default:
            print("Default")
        }
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
        button.setTitle("Seleccione", for: .normal)
    }
    
    
    @IBAction func tapRazonSocialR(_ sender: UIButton) {
        sendDataEmpresa()
    }
    
    @IBAction func tapTamanioEmpresaC(_ sender: ButtonWithImage) {
        IncidenteViewModel.init(parent: self).obtenerComboMestro(parametro:"1",callback: {(success,data) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 1
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func tapActividadC(_ sender: UIButton) {
        IncidenteViewModel.init(parent: self).obtenerActividad(callback: {(success,data) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 2
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func tapActividadEcoI(_ sender: ButtonWithImage) {
        IncidenteViewModel.init(parent: self).obtenerActividad(callback: {(success,data) in
            if let data = success{
                self.dataItem = data
                self.buttonSelect = 3
                self.sendDataTable()
            }
        })
    }
    
    
    @IBAction func tapSeguiente(_ sender: UIButton) {
        if(validarCampos()){
            objectGeneral.gDescripcionFormato = tvDecripcionI.text
            objectGeneral.fDescripcionIncidente = tvDecripcionI.text
            objectGeneral.fEmpleadorInterRazonSocial = tfRazonSocialI.text
            objectGeneral.fEmpleadorInterRuc = tfRucI.text
            objectGeneral.fTrabajadorNombresApellidos = tfNombreTrabajadorI.text
            
            if(!isREP){
                objectGeneral.fEmpleadorRuc = tfRucC.text
                objectGeneral.fEmpleadorRazonSocial = tfRazonSocialC.text
            }
            
            delegate?.sendData(object: objectGeneral)
        }else{
            print("retonar validacion false")
        }
        
    }
    
    private func validarCampos()->Bool{
        if(isREP){
            
            guard (objectGeneral.fEmpleadorIdEmpresa != nil) else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione Razon social", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return false
            }
        }else{
            guard let razonSocialC = tfRazonSocialC.text, !razonSocialC.isEmpty  else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Campo razon social es requerido", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return false
            }
            
            guard let rucContratista = tfRucC.text, !rucContratista.isEmpty  else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Campo ruc es requerido", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return false
            }
            
            
            guard (objectGeneral.fEmpleadorIdTamanioEmpresa != nil) else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione tamaño de la empresa", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return false
            }
            
            guard (objectGeneral.fEmpleadorIdActividadEcomica != nil) else {
                let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione actividad economica", titleAction: "Aceptar")
                self.present(alert,animated: true,completion: nil)
                return false
            }
        }
        
        guard let razonSocialI = tfRazonSocialI.text, !razonSocialI.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo contrato es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let rucI = tfRucI.text, !rucI.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo contrato es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (objectGeneral.fEmpleadorInterIdActividadEconomica != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione actividad economica intermediación", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let nombreTrabajadorI = tfNombreTrabajadorI.text, !nombreTrabajadorI.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo contrato es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let descripcionI = tvDecripcionI.text, !descripcionI.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo contrato es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        return true
    }
    
    func sendDataEmpresa(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadEmpresaViewController") as! LoadEmpresaViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func obtenerTamanio(parametro:String,idTamanioEmpresa:Int){
        IncidenteViewModel.init(parent: self).obtenerComboMestro(parametro:parametro,callback: {(success,data) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == idTamanioEmpresa }) {
                    self.objectGeneral.fEmpleadorIdTamanioEmpresa = data[index].id
                    self.objectGeneral.fEmpleadorIdActividadEcomica = data[index].id
                    return self.tfTamanioR.text = data[index].nombre
                }
            }
        })
    }
    
    private func obtenerActividad(idActividadEconomica:Int){
        IncidenteViewModel.init(parent: self).obtenerActividad(callback: {(success,data) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == idActividadEconomica }) {
                    return self.tfActividadEcoR.text = data[index].nombre
                }
            }
        })
    }
    
    func sendDataTable(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadIncidenteViewController") as! LoadIncidenteViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
}
extension GeneralIncidenteViewController :LoadIncidenteProtocol{
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 1:
            btnTamanioEmpresaC.setTitle(object.nombre, for: .normal)
            objectGeneral.fEmpleadorIdTamanioEmpresa = object.id
            break
        case 2:
            btnActividadEcoC.setTitle(object.nombre, for: .normal)
            objectGeneral.fEmpleadorIdActividadEcomica = object.id
            break
        case 3:
            btnActividadEcoI.setTitle(object.nombre, for: .normal)
            objectGeneral.fEmpleadorInterIdActividadEconomica = object.id
            break
        default:
            print("Algo salio mal")
        }
    }
}

extension GeneralIncidenteViewController :LoadEmpresaProtocol{
    func sendData(object: EmpresaModel) {
        btnRazonSocialR.setTitle(object.razonsocial, for: .normal)
        tfRucR.text = String(object.ruc ?? 0)
        objectGeneral.fEmpleadorRuc = String(object.ruc ?? 0)
        objectGeneral.fEmpleadorIdEmpresa = object.idEmpresaMaestra
        objectGeneral.fEmpleadorRazonSocial = object.razonsocial
        obtenerTamanio(parametro: "1", idTamanioEmpresa: object.idTamanioEmpresa ?? 0)
        obtenerActividad(idActividadEconomica: object.idActividadEconomica ?? 0)
    }
    
    
}
