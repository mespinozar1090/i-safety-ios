//
//  LoadIncidenteViewController.swift
//  i-Safety
//
//  Created by usuario on 9/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LoadIncidenteProtocol {
    func sendData(object:ItemSpinnerModel)
}

class LoadIncidenteViewController: UIViewController {

    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
       
       var items:Array<ItemSpinnerModel>?
       var itemSelect:LoadIncidenteProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataItem = items!
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
}

//MARK: - Extension Table Load
extension LoadIncidenteViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "LoadIncidenteTableViewCell", for: indexPath) as! LoadIncidenteTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre?.capitalized
       return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = ItemSpinnerModel()
        object.id = dataItem[indexPath.row].id
        object.nombre = dataItem[indexPath.row].nombre
        itemSelect?.sendData(object: object)
       navigationController?.popViewController(animated: true)
    }
    
}
