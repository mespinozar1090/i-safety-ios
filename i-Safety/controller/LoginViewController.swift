//
//  LoginViewController.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var btnIngresar: UIButton!
    @IBOutlet weak var tfUsuario: UITextField!
    @IBOutlet weak var ftClave: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if UserDefaults.standard.value(forKey: "idUsers") != nil {
            if ((UserDefaults.standard.value(forKey: "idUsers") as! Int) > 0){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialVC = storyboard.instantiateViewController(withIdentifier: "MyTabBarViewController") as! MyTabBarViewController
                self.present(initialVC, animated: true, completion: nil)
            }
        }
    }
    
    func loginAcceso(usuario:String,clave:String){
        let params:NSDictionary = ["User": usuario, "Pass": clave]
        
        let loginViewModel = LoginViewModel.init(parent: self)
        loginViewModel.loginAcceso(params: params, callback: {(succes,error) in
            if(succes)!{
                UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                self.tfUsuario.text = ""
                self.ftClave.text = ""
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialVC = storyboard.instantiateViewController(withIdentifier: "MyTabBarViewController") as! MyTabBarViewController
                self.present(initialVC, animated: true, completion: nil)
            }else{
                UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
                let alert = UtilMetodos.getAlert(title: "Información", message: "Credenciales incorrectas", titleAction: "Aceptar")
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    func validarCampos(){
        UtilMetodos.showActivityIndicatorV2(uiView: (self.view), title: "Autenticando...")
        if(tfUsuario.text != "" && ftClave.text != ""){
            loginAcceso(usuario:tfUsuario.text ?? "", clave: ftClave.text ?? "")
        }else{
            UtilMetodos.hideActivityIndicator(uiView: (self.view)!)
            let alert = UtilMetodos.getAlert(title: "Información", message: "No se permite campos vacios", titleAction: "Aceptar")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func accionLogin(_ sender: UIButton) {
        validarCampos()
    }
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {
        print("Regreso de cerrar sesion!")
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleToFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
        
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleToFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
