//
//  DatosGeneralesIViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol GeneralIntevertoriaProcotol {
    func sendDataGeneral(object:InsertInterventoria)
}

class DatosGeneralesIViewController: UIViewController {
    
    @IBOutlet weak var btnSedeProyecto: ButtonWithImage!
    @IBOutlet weak var btnLinea: ButtonWithImage!
    @IBOutlet weak var txtNroPlanTrabajo: UITextField!
    @IBOutlet weak var txtActividad: UITextField!
    @IBOutlet weak var btnPlanTrabajo: ButtonWithImage!
    @IBOutlet weak var txtSupervisor: UITextField!
    @IBOutlet weak var txtSupervisorSustituto: UITextField!
    @IBOutlet weak var btnEmpresaIntervenida: ButtonWithImage!
    @IBOutlet weak var txtComentario: UITextView!
    
    var dataItem:Array<ItemSpinnerModel> = []
    var buttonSelect:Int?
    var objectGeneral = InsertInterventoria()
    var delegate:GeneralIntevertoriaProcotol?
    
    //Detalle
    var interventoriaItem:InsertInterventoria?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupButton(button: btnSedeProyecto)
        setupButton(button: btnLinea)
        setupButton(button: btnPlanTrabajo)
        setupButton(button: btnEmpresaIntervenida)
        loadDetalle()
    }
    
    private func loadDetalle(){
        if let data = interventoriaItem, data.idInteventoria != nil{
            objectGeneral = data
            txtNroPlanTrabajo.text = interventoriaItem?.nroPlanTrabajo
            txtActividad.text = interventoriaItem?.actividad
            txtComentario.text = interventoriaItem?.conclucion
            txtSupervisor.text = interventoriaItem?.supervisor
            txtSupervisorSustituto.text = interventoriaItem?.supervisorSustituto
            loadSedeProyecto()
            loadLineaSubEstacion()
            loadPlanTrabajo()
            loadEmpresaI()
        }else{
            print("no cargo datos")
        }
    }
    
    //MARK: - Load detalle
    private func loadSedeProyecto(){
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.interventoriaItem?.lugarZona }) {
                    self.btnSedeProyecto.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadLineaSubEstacion(){
        InterventoriaViewModel.init(parent: self).obtenerSubEstacion(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.interventoriaItem?.lineaSubEstacion }) {
                    self.btnLinea.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    private func loadPlanTrabajo(){
        let data = UtilMetodos.loadPlanTrabajo()
        if let index = data.firstIndex(where: { $0.id == self.interventoriaItem?.planTrabajo }) {
            self.btnPlanTrabajo.setTitle(data[index].nombre, for: .normal)
        }
    }
    
    private func loadEmpresaI(){
        SbcViewModel.init(parent: self).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let data = success{
                if let index = data.firstIndex(where: { $0.id == self.interventoriaItem?.empresaIntervenida }) {
                    self.btnEmpresaIntervenida.setTitle(data[index].nombre, for: .normal)
                }
            }
        })
    }
    
    //MARK: - Validar Campos
    func validarCampos() -> Bool{
        guard (objectGeneral.lugarZona != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione sede/Proyecto", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let planTrabajo = txtNroPlanTrabajo.text, !planTrabajo.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo plan de trabajo es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (objectGeneral.lineaSubEstacion != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione linea SubEstacion", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let actividad = txtActividad.text, !actividad.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo actividad es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard (objectGeneral.idPlanTrabajo != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione plan de trabajo", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        guard (objectGeneral.empresaIntervenida != nil) else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Seleccione empresa intervenida", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let supervisor = txtSupervisor.text, !supervisor.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo supervisor es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let supervisorSusti = txtSupervisorSustituto.text, !supervisorSusti.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo supervisor sustituto es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        guard let comentario = txtComentario.text, !comentario.isEmpty  else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Campo supervisor sustituto es requerido", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return false
        }
        
        return true
    }
    
    private func setupButton(button:ButtonWithImage){
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func mostrarSedeProyecto(_ sender: UIButton) {
        SbcViewModel.init(parent: self).obtenerSede(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 1
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func mostrarLinea(_ sender: UIButton) {
        InterventoriaViewModel.init(parent: self).obtenerSubEstacion(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 2
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    @IBAction func mostrarPlanTrabajo(_ sender: UIButton) {
        self.dataItem = UtilMetodos.loadPlanTrabajo()
        self.buttonSelect = 3
        self.sendDataTable()
    }
    
    @IBAction func mostrarEmpresaI(_ sender: Any) {
        SbcViewModel.init(parent: self).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let data = success{
                self.buttonSelect = 4
                self.dataItem = data
                self.sendDataTable()
            }
        })
    }
    
    func sendDataTable(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "LoadCrearInterViewController") as! LoadCrearInterViewController
        controller.items = dataItem
        controller.itemSelect = self
        self.navigationController?.pushViewController(controller, animated: true)
       // self.navigationController?.present(controller, animated:true, completion: nil)
    }
    
    @IBAction func tapContinuar(_ sender: ButtonRadius) {
        if(validarCampos()){
            objectGeneral.nroPlanTrabajo = txtNroPlanTrabajo.text
            objectGeneral.actividad = txtActividad.text
            objectGeneral.supervisor = txtSupervisor.text
            objectGeneral.supervisorSustituto = txtSupervisorSustituto.text
            objectGeneral.conclucion =  txtComentario.text
            delegate?.sendDataGeneral(object: objectGeneral)
        }
    }
}

extension DatosGeneralesIViewController: LoadCrearInterSelect{
    func sendData(object: ItemSpinnerModel) {
        switch buttonSelect {
        case 1:
            btnSedeProyecto.setTitle(object.nombre, for: .normal)
            objectGeneral.lugarZona = object.id
        case 2:
            btnLinea.setTitle(object.nombre, for: .normal)
            objectGeneral.lineaSubEstacion = object.id
        case 3:
            btnPlanTrabajo.setTitle(object.nombre, for: .normal)
            objectGeneral.idPlanTrabajo
                = object.id
        case 4:
            btnEmpresaIntervenida.setTitle(object.nombre, for: .normal)
            objectGeneral.empresaIntervenida = object.id
        default:
            print("Algo salio mal")
        }
    }
}
