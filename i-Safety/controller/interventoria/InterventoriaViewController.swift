//
//  InterventoriaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/9/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InterventoriaViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<InterventoriaModel> = []
    
    //Detalle registro
    var interventoriaItem = InsertInterventoria()
    var verificacionItem:Array<VerificacionSubItem> = []
    var controlPersonalItem:Array<ControlSubItem> = []
    var idRolUsario:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        idRolUsario = UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as? Int
    }
    
    override func viewDidAppear(_ animated: Bool) {
       listarInterventoria()
    }
    
    func setupNavigationBarItem(){
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false,animated:true)
        self.navigationController?.navigationBar.topItem?.title = "Interventoria"
        let crearButton =  UIBarButtonItem(title: "crear", style: .plain, target: self, action: #selector(crearSbc))
        crearButton.tintColor = .white
        navigationItem.rightBarButtonItem = crearButton
    }
    
    @objc func crearSbc(){
        interventoriaItem = InsertInterventoria()
        verificacionItem.removeAll()
        controlPersonalItem.removeAll()
        self.performSegue(withIdentifier: "sgCrearInterventoria", sender: self)
    }
    
    func listarInterventoria(){
        let params:NSDictionary = ["Id_Usuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
        "Id_Rol_Usuario": UserDefaults.standard.value(forKey: "Id_Rol_Usuario") as! Int,
        "Id_Rol_General": UserDefaults.standard.value(forKey: "Id_Rol_General") as! Int]
        InterventoriaViewModel.init(parent: self).listarInterventoriaV2(params: params,callback: {(data,error)in
            if let lista = data{
                print("data count \(lista.count)")
                self.dataItem = lista
                DispatchQueue.main.async {
                    self.tblItem.reloadData()
                }
            }
        })
    }
    
    private func obtenerRegistroInterventoria(idInterventoria:Int){
        InterventoriaViewModel.init(parent: self).obtenerRegistroInterventoria(idInterventoria: idInterventoria, callback: {(object,verificacion,controlPersonal,error) in
            print("data2: ",object?.idInteventoria ?? "es null")
            if let data = object{
                self.interventoriaItem = data
                
                if let verifi = verificacion , verifi.count > 0{
                     self.verificacionItem = verifi
                }
                
                if let control = controlPersonal , control.count > 0{
                     self.controlPersonalItem = control
                }
            }
             self.performSegue(withIdentifier: "sgCrearInterventoria", sender: self)
        })
    }
    
    private func showModalEliminar(index:IndexPath){
        
        let alert = UIAlertController(title: "Información", message: "¿Está seguro de eliminar el registro de Interventoria?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Aceptar", style: .default, handler: { (action) -> Void in
            self.eliminarInterventoria(indexPath: index)
        })
        alert.addAction(ok)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func eliminarInterventoria(indexPath:IndexPath){
        let params:NSDictionary = ["Id_Interventoria": self.dataItem[indexPath.row].idInterventoria ?? 0]
        InterventoriaViewModel.init(parent: self).eliminarInterventoria(params:params, callback: {(success,error)in
            if let result = success, result{
                self.dataItem.remove(at: indexPath.row)
                self.tblItem.beginUpdates()
                self.tblItem.deleteRows(at: [indexPath], with: .automatic)
                self.tblItem.endUpdates()
            }else{
                print("Ocurrio un error no elimino")
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if(segue.identifier == "sgCrearInterventoria"){
               if let indexPath = self.tblItem.indexPathForSelectedRow {
                  let nav = segue.destination as! UINavigationController
                  let destination = nav.topViewController as! CrearInterventoriaViewController
                    destination.datosGenerales = interventoriaItem
                    destination.lstVerificacion = verificacionItem
                   destination.controlSubItem = controlPersonalItem
               }
           }
       }
}

// MARK:- Configuracion table
extension InterventoriaViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InterventoriaTableViewCell", for: indexPath) as! InterventoriaTableViewCell
        cell.txtCodigo.text = dataItem[indexPath.row].codigo
        cell.txtUsuario.text =  dataItem[indexPath.row].nombreUsuario
        cell.txtNombreFormato.text =  dataItem[indexPath.row].nombreFormato
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        obtenerRegistroInterventoria(idInterventoria: dataItem[indexPath.row].idInterventoria ?? 0)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if(idRolUsario == 1){
             return .delete
        }
        return .none
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("debe eliminar")
            showModalEliminar(index: indexPath)
        }
    }
}
