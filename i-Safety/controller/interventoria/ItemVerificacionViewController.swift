//
//  ItemVerificacionViewController.swift
//  i-Safety
//
//  Created by usuario on 9/27/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol ItemVerificacionProtocol {
    func sendData(lista:Array<VerificacionSubItem>)
}

class ItemVerificacionViewController: UIViewController {
    
     @IBOutlet weak var tblItem: UITableView!
     var itemVerificacion:Array<VerificacionSubItem> = []
     var dataFilter:Array<VerificacionSubItem> = []
     var idVerificacion:Int?
     var delegate:ItemVerificacionProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        let filterData = itemVerificacion.filter { (item) -> Bool in
            item.idVerificacion == idVerificacion
        }
        
        self.dataFilter = filterData
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    func setupNavigationBarItem(){
          let cancelarButton =  UIBarButtonItem(title: "Atras", style: .plain, target: self, action: #selector(cancelarAccion))
          navigationController?.navigationBar.isTranslucent = false
          navigationController?.setNavigationBarHidden(false,animated:true)
          navigationItem.leftBarButtonItem = cancelarButton
      }
      
      @objc func cancelarAccion(){
          delegate?.sendData(lista: itemVerificacion)
          navigationController?.popViewController(animated: true)
      }

  
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgDetalleVerficacionI"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! DetalleVerficacionViewController
                destination.itemVerificacion = dataFilter[indexPath.row]
                destination.delegate = self
            }
        }
    }
    

}

//MARK: - configuracion table
extension ItemVerificacionViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return dataFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemVerificacionTableViewCell", for: indexPath) as! ItemVerificacionTableViewCell
        cell.txtNombre.text = dataFilter[indexPath.row].descripcion
              return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.performSegue(withIdentifier: "sgDetalleVerficacionI", sender: nil)
    }
}

extension ItemVerificacionViewController: DetalleVerficacionProtocol{
    func sendData(object: VerificacionSubItem) {
        if let index = itemVerificacion.firstIndex(where: { $0.idVerificacionSubitem == object.idVerificacionSubitem }) {
             return itemVerificacion[index] = object
        }
    }
    
}
