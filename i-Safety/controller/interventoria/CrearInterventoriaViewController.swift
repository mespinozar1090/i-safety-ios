//
//  CrearInterventoriaViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class CrearInterventoriaViewController: UIViewController {
    
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var datosGeneralController: UIView!
    @IBOutlet weak var verificacionController: UIView!
    @IBOutlet weak var controlPersonalController: UIView!
    var notificar:Array<NotificarModel> = []
    // var lstVerificacion:Array<VerificacionSubItem> = []
    // var controlSubItem:Array<ControlSubItem> = []
    var datosGenerales:InsertInterventoria?
    var lstVerificacion:Array<VerificacionSubItem>?
    var controlSubItem:Array<ControlSubItem>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Interventoria"
        hideKeyboardWhenTappedAround()
        setupNavigationBarItem()
        segmented.selectedSegmentIndex = 0
        datosGeneralController.alpha = 1
        verificacionController.alpha = 0
        controlPersonalController.alpha = 0
        segmented.isEnabled = false
    }
    
    func setupNavigationBarItem(){
        
        let cancelarButton =  UIBarButtonItem(title: "cerrar", style: .plain, target: self, action: #selector(cancelarAccion))
        cancelarButton.tintColor = .white
        
        let correoImg = UIImage(named: "ico_email")
        let btnCorreo:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30, height: 20))
        btnCorreo.setImage(correoImg, for: .normal)
        btnCorreo.sizeToFit()
        btnCorreo.addTarget(self, action: #selector(addNotificador), for: UIControl.Event.touchUpInside)
        let notificarButton = UIBarButtonItem(customView: btnCorreo)
        
        let evidenciaButton =  UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(crearEvidencia))
        
        self.navigationItem.rightBarButtonItems = [notificarButton,evidenciaButton]
        self.navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func crearEvidencia(){
       // self.performSegue(withIdentifier: "sgCrearHallazgo", sender: self)
    }
    
    @objc func addNotificador(){
        self.performSegue(withIdentifier: "sgNotificarInterventoria", sender: self)
    }
    
    @objc func cancelarAccion(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func accionController(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            datosGeneralController.alpha = 1
            verificacionController.alpha = 0
            controlPersonalController.alpha = 0
        case 1:
            datosGeneralController.alpha = 0
            verificacionController.alpha = 1
            controlPersonalController.alpha = 0
        case 2:
            datosGeneralController.alpha = 0
            verificacionController.alpha = 0
            controlPersonalController.alpha = 1
        default:
            print("Default")
        }
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgNotificarInterventoria"){
            let nav = segue.destination as! UINavigationController
            let destino = nav.topViewController as! NotificarViewController
            destino.dataItemSelect = notificar
            destino.itemSelect = self
        }else if (segue.identifier == "sgVerifiacionInterventoria"){
            let destination = segue.destination as! VerficacionViewController
            destination.lstVerificacion = lstVerificacion
            destination.delegate = self
        }else if (segue.identifier == "sgGeneralInterventoria"){
            let destination = segue.destination as! DatosGeneralesIViewController
            destination.interventoriaItem = datosGenerales
            destination.delegate = self
        }else if (segue.identifier == "sgControlInterventoria"){
            let destination = segue.destination as! ControlPersonalViewController
            destination.controlPersonalSubItem = controlSubItem
            destination.delegate = self
        }
    }
    
    private func creaRegistroInterventoria(){
        
        guard notificar.count > 0 else {
            let alert = UtilMetodos.getAlert(title: "Información", message: "Debe seleccionar almenos 1 usuario a notificar", titleAction: "Aceptar")
            self.present(alert,animated: true,completion: nil)
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        let horaFormate = DateFormatter()
        horaFormate.dateFormat = "HH:mm:ss"
        
        let filterNotificar = notificar.filter { (item) -> Bool in
            item.selectEmail == true
        }
        
        let rolUsario:String = UserDefaults.standard.value(forKey: "Descripcion_Rol") as! String
        let nombreUsuario:String = UserDefaults.standard.value(forKey: "nombreUsuario") as! String
        let nombreFormato:String = "INCI- \(rolUsario) - \(nombreUsuario) - \(formatter.string(from: Date()))";
        
        var notificadores:Array<NSDictionary> = []
        for index in 0...filterNotificar.count-1 {
            let paramsNotificar:NSDictionary = [
                "IdUser": (filterNotificar[index].idUsers ?? 0) as Int,
                "Nombre": (filterNotificar[index].nombreUsuario ?? "")as String,
                "Email": (filterNotificar[index].emailCorporativo ?? "") as String,
            ]
            notificadores.append(paramsNotificar)
        }
        
        var verificaciones:Array<NSDictionary> = []
        if(lstVerificacion?.count ?? 0 > 0){
            for index in 0...lstVerificacion!.count-1 {
                let paramsverificaciones:NSDictionary = [
                    "Id_Interventoria_Detalle": lstVerificacion?[index].idInterventoriaDetalle ?? 0 as Int,
                    "Id_Verificacion_Subitem": (lstVerificacion?[index].idVerificacionSubitem ?? 0) as Int,
                    "Id_Verificacion": (lstVerificacion?[index].idVerificacionItems ?? 0 ) as Int,
                    "Cumple": (lstVerificacion?[index].cumple ?? 0) as Int,
                    "Id_Interventoria": (lstVerificacion?[index].idVerificacionItems ?? 0 ) as Int]
                verificaciones.append(paramsverificaciones)
            }
        }
        
        var controles:Array<NSDictionary> = []
        if(controlSubItem?.count ?? 0 > 0){
            for index in 0...controlSubItem!.count-1 {
                let paramcontrol:NSDictionary = [
                    "Id_Control_Detalle": controlSubItem?[index].idControlDetalle ?? 0 as Int,
                    "Id_Control_item": (controlSubItem?[index].idControlitem ?? 0) as Int,
                    "Id_Control": (controlSubItem?[index].idControl ?? 0 ) as Int,
                    "Cantidad": (controlSubItem?[index].cantidad ?? 0) as Int]
                controles.append(paramcontrol)
            }
        }
        
        
        print(" idusuario : ",UserDefaults.standard.value(forKey: "idUsers") as! Int)
        
        let params:NSDictionary = [
            "Id_Interventoria": datosGenerales?.idInteventoria ?? 0 as Int ,
            "Actividad": (datosGenerales?.actividad ?? "") as String ,
            "Conclucion": (datosGenerales?.conclucion ?? "") as String,
            "Empresa_Intervenida": (datosGenerales?.empresaIntervenida ?? 0) as Int,
            "Empresa_Inteventor": (datosGenerales?.empresaInterventor ?? 0 ) as Int,
            "Estado": (datosGenerales?.estado ?? false) as Bool,
            "Fecha_Hora": horaFormate.string(from: Date()) as String,
            "Fecha_Registro": formatter.string(from: Date()) as String,
            "Id_Plan_Trabajo": (datosGenerales?.idPlanTrabajo ?? 0) as Int,
            "PlanTrabajo": (datosGenerales?.idPlanTrabajo ?? 0) as Int,
            "IdUsuario": UserDefaults.standard.value(forKey: "idUsers") as! Int,
            "Interventor": nombreUsuario,
            "Linea_Subestacion": (datosGenerales?.lineaSubEstacion ?? 0) as Int,
            "Lugar_Zona": (datosGenerales?.lugarZona ?? 0) as Int,
            "Nro_Plan_Trabajo": (datosGenerales?.nroPlanTrabajo ?? "") as String,
            "Supervisor": (datosGenerales?.supervisor ?? "") as String,
            "Supervisor_Sustituto": (datosGenerales?.supervisorSustituto ?? "") as String,
            "NombreFormato": nombreFormato as String]
        
        
        if (datosGenerales?.idInteventoria ?? 0 > 0){
            print("enviar a editar interventoria")
            InterventoriaViewModel.init(parent: self).editarRegistroInterventoria(params: params, notificar: notificadores, verficacion: verificaciones, controlpersonal: controles, callback: {(success,error) in
                if let result = success,result == 1{
                    print("Interventoria se registro correctamente")
                    self.dismiss(animated: true, completion: nil)
                }else{
                    print("No se registro interventoria")
                }
            })
            
        }else{
            print("Crear nuevo registro Interventoria")
            InterventoriaViewModel.init(parent: self).crearRegistroInterventoria(params: params, notificar: notificadores, verficacion: verificaciones, controlpersonal: controles, callback: {(success,error) in
                if let result = success,result == 1{
                    print("Interventoria se registro correctamente")
                    self.dismiss(animated: true, completion: nil)
                }else{
                    print("No se registro interventoria")
                }
            })
        }
        
    }
}


extension CrearInterventoriaViewController : NotificarSelect{
    func sendData(lista: Array<NotificarModel>) {
        notificar = lista
    }
}

extension CrearInterventoriaViewController : GeneralIntevertoriaProcotol{
    func sendDataGeneral(object: InsertInterventoria) {
        datosGenerales = object
      //  segmented.isEnabled = true
        segmented.selectedSegmentIndex = 1
        datosGeneralController.alpha = 0
        verificacionController.alpha = 1
        controlPersonalController.alpha = 0
    }
}

extension CrearInterventoriaViewController : VerificacionIProtocol{
    func sendData(lista: Array<VerificacionSubItem>) {
        lstVerificacion = lista
        segmented.selectedSegmentIndex = 2
        datosGeneralController.alpha = 0
        verificacionController.alpha = 0
        controlPersonalController.alpha = 1
    }
}

extension CrearInterventoriaViewController : ControlIProtocol{
    func sendData(lista: Array<ControlSubItem>) {
        controlSubItem = lista
        creaRegistroInterventoria()
    }
}
