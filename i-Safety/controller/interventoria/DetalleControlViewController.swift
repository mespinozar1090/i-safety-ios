//
//  DetalleControlViewController.swift
//  i-Safety
//
//  Created by usuario on 9/28/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol ItemControlIProtocol {
    func sendData(lista:Array<ControlSubItem>)
}

class DetalleControlViewController: UIViewController {

    @IBOutlet weak var tblItem: UITableView!
    var controlSubItem:Array<ControlSubItem> = []
    var dataFilter:Array<ControlSubItem> = []
    var idControl:Int?
    var delegate: ItemControlIProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        let filterData = controlSubItem.filter { (item) -> Bool in
            item.idControl == idControl
        }
        
        self.dataFilter = filterData
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    func setupNavigationBarItem(){
        let cancelarButton =  UIBarButtonItem(title: "Atras", style: .plain, target: self, action: #selector(cancelarAccion))
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false,animated:true)
        navigationItem.leftBarButtonItem = cancelarButton
    }
    
    @objc func cancelarAccion(){
        delegate?.sendData(lista: controlSubItem)
        navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func showCrearGrupo(data:ControlSubItem){
       
        let alert = UIAlertController(title: "Control Personal", message: data.descripcion, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: { (action) -> Void in}))
        let saveAction = UIAlertAction(title:"Aceptar", style: .default, handler: { (action) -> Void in
            if let index = self.controlSubItem.firstIndex(where: { $0.idControlitem == data.idControlitem }) {
                print("deberia entrar")
                return self.controlSubItem[index] = data
            }
           
        })
        alert.addAction(saveAction)
        saveAction.isEnabled = false
        alert.addTextField(configurationHandler: { (textField) in
            textField.text = String(data.cantidad)
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                
               
                if(self.validarCampo(enteredEmail: textField.text ?? "")){
                    data.cantidad = Int(textField.text!)!
                    saveAction.isEnabled = true
                }else{
                     saveAction.isEnabled = false
                }
            }
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func validarCampo(enteredEmail:String) -> Bool {
        let emailFormat = "^[0-9]+$"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
}

//MARK: - configuracion table
extension DetalleControlViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return dataFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetalleControlTableViewCell", for: indexPath) as! DetalleControlTableViewCell
               cell.txtNombre.text = dataFilter[indexPath.row].descripcion
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = dataFilter[indexPath.row]
        showCrearGrupo(data: object)
    }
    
}
