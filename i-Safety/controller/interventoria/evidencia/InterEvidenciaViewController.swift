//
//  InterEvidenciaViewController.swift
//  i-Safety
//
//  Created by usuario on 12/9/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InterEvidenciaViewController: UIViewController {

    @IBOutlet weak var tblItem: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItem()
        // Do any additional setup after loading the view.
    }
    
    func setupNavigationBarItem(){
        title = "Evidencia"
        let guardarButton =  UIBarButtonItem(title: "Agregar", style: .plain, target: self, action: #selector(agrearAccion))
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false,animated:true)
        navigationItem.rightBarButtonItem = guardarButton
    }
   
    @objc func agrearAccion(){
       // self.performSegue(withIdentifier: "sgCrearBP", sender: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension InterEvidenciaViewController : UITableViewDataSource,UITableViewDelegate{
//    
//}
