//
//  VerficacionViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol VerificacionIProtocol {
    func sendData(lista:Array<VerificacionSubItem>)
}

class VerficacionViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    var delegate:VerificacionIProtocol?
    var itemVerificacion:Array<VerificacionSubItem> = []
    var lstVerificacion:Array<VerificacionSubItem>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obtenerVerificacion()
        
        loadVerificacion()
    }
    
    private func loadVerificacion(){
        if let data = lstVerificacion, data.count > 0{
            print("tiene verificacion detalle")
            itemVerificacion = data
        }else{
            obtenerVerficacionItem()
            print("no tiene verificacion detalle")
        }
    }
    
    func obtenerVerificacion(){
        self.dataItem = UtilMetodos.loadVerificaciones()
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    func obtenerVerficacionItem(){
        itemVerificacion  = UtilMetodos.obtenerVerificacionItem()
    }
    
    @IBAction func tapSiguiente(_ sender: UIButton) {
        delegate?.sendData(lista: itemVerificacion)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "sgItemVerficacionI"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! ItemVerificacionViewController
                destination.idVerificacion = dataItem[indexPath.row].id
                destination.itemVerificacion = itemVerificacion
                destination.delegate = self
            }
        }
    }
}

//MARK: - configuracion table
extension VerficacionViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VerificacionTableViewCell", for: indexPath) as! VerificacionTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.performSegue(withIdentifier: "sgItemVerficacionI", sender: nil)
       }
}

extension VerficacionViewController : ItemVerificacionProtocol{
    func sendData(lista: Array<VerificacionSubItem>) {
        itemVerificacion = lista
    }
}
