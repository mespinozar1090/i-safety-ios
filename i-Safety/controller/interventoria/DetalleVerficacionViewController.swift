//
//  DetalleVerficacionViewController.swift
//  i-Safety
//
//  Created by usuario on 9/27/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol DetalleVerficacionProtocol {
    func sendData(object:VerificacionSubItem)
}

class DetalleVerficacionViewController: UIViewController {
    
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var segmented: UISegmentedControl!
    var delegate:DetalleVerficacionProtocol?
    var itemVerificacion:VerificacionSubItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = itemVerificacion{
            txtNombre.text = item.descripcion
            switch item.cumple {
            case 1:
                segmented.selectedSegmentIndex = 0
            case 2:
                segmented.selectedSegmentIndex = 1
            case 3:
                segmented.selectedSegmentIndex = 2
            default:
                print("Default")
            }
        }
    }
   
    
    @IBAction func tapSegmented(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        switch (index) {
        case 0:
            itemVerificacion?.cumple = 1
            delegate?.sendData(object: itemVerificacion!)
            navigationController?.popViewController(animated: true)
        case 1:
            itemVerificacion?.cumple = 2
            delegate?.sendData(object: itemVerificacion!)
            navigationController?.popViewController(animated: true)
        case 2:
            itemVerificacion?.cumple = 3
            delegate?.sendData(object: itemVerificacion!)
            navigationController?.popViewController(animated: true)
        default:
            print("Default")
        }
    }
}
