//
//  LoadCrearInterViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

protocol LoadCrearInterSelect {
    func sendData(object:ItemSpinnerModel)
}

class LoadCrearInterViewController: UIViewController {
    
    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    lazy var searchBar:UISearchBar = UISearchBar()
    var items:Array<ItemSpinnerModel>?
    var dataSearch:Array<ItemSpinnerModel> = []
    var itemSelect:LoadCrearInterSelect?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpsearchBar()
        if let data = items{
            dataItem = data
            dataSearch = dataItem
            DispatchQueue.main.async {
                self.tblItem.reloadData()
            }
        }
    }
    
    private func setUpsearchBar(){
        searchBar.delegate = self
        searchBar.endEditing(true)
        searchBar.placeholder = "Buscar"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .gray
        navigationItem.titleView = searchBar
        
    }
    
}

//MARK: configuration table searchBar
extension LoadCrearInterViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            dataItem = dataSearch
            tblItem.reloadData()
            return
        }
        dataItem = dataSearch.filter({ (object) -> Bool in
            guard let text = searchBar.text else {return false}
            return (object.nombre?.localizedCaseInsensitiveContains(text))!
        })
        tblItem.reloadData()
    }
}

//MARK: - Extension Table Load
extension LoadCrearInterViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoadCrearInterTableViewCell", for: indexPath) as! LoadCrearInterTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre?.capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = ItemSpinnerModel()
        object.id = dataItem[indexPath.row].id
        object.nombre = dataItem[indexPath.row].nombre
        itemSelect?.sendData(object: object)
        navigationController?.popViewController(animated: true)
    }
    
}
