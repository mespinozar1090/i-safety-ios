//
//  ControlPersonalViewController.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit
protocol ControlIProtocol {
    func sendData(lista:Array<ControlSubItem>)
}

class ControlPersonalViewController: UIViewController {

    @IBOutlet weak var tblItem: UITableView!
    var dataItem:Array<ItemSpinnerModel> = []
    var controlSubItem:Array<ControlSubItem> = []
    var delegate:ControlIProtocol?
    var controlPersonalSubItem:Array<ControlSubItem>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       obtenerControlPersonal()
       loadControlPersonal()
    }
    
    private func loadControlPersonal(){
        if let data = controlPersonalSubItem, data.count > 0{
            print("tiene control detalle")
            controlSubItem = data
        }else{
            obtenerControlItem()
            print("no tiene control detalle")
        }
    }
    
    func obtenerControlPersonal(){
      dataItem =  UtilMetodos.loadControlPersonal()
        DispatchQueue.main.async {
            self.tblItem.reloadData()
        }
    }
    
    func obtenerControlItem(){
        InterventoriaViewModel.init(parent: self).obtenerControlSubItem(callback: {(data,error) in
            if let lista = data {
                self.controlSubItem =  lista
            }
        })
    }

    @IBAction func tapGuardar(_ sender: UIButton) {
        delegate?.sendData(lista: controlSubItem)
    }
   
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if(segue.identifier == "sgItemControlI"){
            if let indexPath = self.tblItem.indexPathForSelectedRow {
                let destination = segue.destination as! DetalleControlViewController
                destination.idControl = dataItem[indexPath.row].id
                destination.controlSubItem = controlSubItem
                destination.delegate = self
            }
        }
    }
    
}

//MARK: - configuracion table
extension ControlPersonalViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ControlPersonalTableViewCell", for: indexPath) as! ControlPersonalTableViewCell
        cell.txtNombre.text = dataItem[indexPath.row].nombre
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         self.performSegue(withIdentifier: "sgItemControlI", sender: nil)
    }
}

extension ControlPersonalViewController:ItemControlIProtocol{
    func sendData(lista: Array<ControlSubItem>) {
        controlSubItem = lista
    }
}
