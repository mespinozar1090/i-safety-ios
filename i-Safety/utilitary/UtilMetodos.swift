//
//  UtilMetodos.swift
//  i-Safety
//
//  Created by usuario on 7/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import UIKit

class UtilMetodos {
    
    static var container: UIView = UIView()
    static var loadingView: UIView = UIView()
    static var labelIndicator:UILabel = UILabel()
    static var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    static func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    static func convertBase64StringToImage(imageBase64String:String) -> UIImage {
        //  let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        let dataDecoded : Data = Data(base64Encoded: imageBase64String, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage!
    }
    
    static func tfSetError(field: UITextField, placeholder: String, focus: Bool = true) {
        field.layer.borderColor = UIColor.red.cgColor
        field.layer.borderWidth = 1
        field.layer.cornerRadius = 5
        field.attributedPlaceholder = NSAttributedString(string: placeholder, attributes:[NSAttributedString.Key.foregroundColor: hexStringToUIColor(hex: "ED8277")])
        field.textColor = UIColor.red
        if(focus) {
            field.becomeFirstResponder()
        }
    }
    
    static func tfClearError(field: UITextField) {
        field.layer.borderWidth = 1
        field.clipsToBounds = true
        field.layer.cornerRadius = 5
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.gray.cgColor
        field.textColor = hexStringToUIColor(hex: "000000")
        field.attributedPlaceholder = NSAttributedString(string: "Campo requerido", attributes:[NSAttributedString.Key.foregroundColor: hexStringToUIColor(hex: "E0E0E0")])
    }
    
    static func obtenerGestion() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "SST"))
        dataItem.append(ItemSpinnerModel(id: 2, nombre: "MA"))
        return dataItem
    }
    
    
    static func showActivityIndicatorV2(uiView: UIView, title: String) {
        container.frame = uiView.frame
        container.center = uiView.center
        container = UIView(frame: UIScreen.main.bounds)
        container.backgroundColor = uiColorFromHex(rgbValue: 0x000000, alpha: 0.6)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 280, height: 80)
        loadingView.center = uiView.center
        loadingView.clipsToBounds = true
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
        activityIndicator.center.x = loadingView.frame.size.width / 2
        activityIndicator.color = hexStringToUIColor(hex: "FFFFFF")
        
        labelIndicator.frame = CGRect(x: 0, y: 55, width: 280, height: 25)
        labelIndicator.text = title
        labelIndicator.textAlignment = .center
        labelIndicator.textColor = hexStringToUIColor(hex: "FFFFFF")
        
        loadingView.addSubview(activityIndicator)
        loadingView.addSubview(labelIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    static func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    static func uiColorFromHex(rgbValue:UInt32, alpha:Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func loadVerificaciones() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "PLANIFICACIÓN Y REUNIÓN DE INICIO"))
        dataItem.append(ItemSpinnerModel(id: 2, nombre: "EJECUCIÓN DE LA ACTIVIDAD"))
        dataItem.append(ItemSpinnerModel(id: 3, nombre: "REUNIÓN DE CIERRE"))
        return dataItem
    }
    
    static func loadControlPersonal() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "MANO DE OBRA"))
        dataItem.append(ItemSpinnerModel(id: 2, nombre: "TRABAJOS PREVIOS"))
        dataItem.append(ItemSpinnerModel(id: 3, nombre: "VEHICULO"))
        dataItem.append(ItemSpinnerModel(id: 4, nombre: "OTROS VEHICULOS"))
        dataItem.append(ItemSpinnerModel(id: 5, nombre: "MATERIALES E INSUMOS"))
        dataItem.append(ItemSpinnerModel(id: 6, nombre: "EQUIPOS Y HERRAMIENTAS"))
        dataItem.append(ItemSpinnerModel(id: 7, nombre: "EQUIPOS Y HERRAMIENTAS ESPECIALES"))
        return dataItem
    }
    
    
    static func loadPlanTrabajo() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 0, nombre: "Nacional"))
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "Local"))
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "Sin Consignación"))
        return dataItem
    }
    
    static func loadHorario() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 0, nombre: "MAÑANA"))
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "TARDE"))
        return dataItem
    }
    
    static func loadTiempoExperiencia() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "Menor a 2 años"))
        dataItem.append(ItemSpinnerModel(id: 2, nombre: "Entre 2 a 5 años"))
        dataItem.append(ItemSpinnerModel(id: 3, nombre: "Entre 6 y 11 años"))
        dataItem.append(ItemSpinnerModel(id: 4, nombre: "Mayor a 12 años"))
        return dataItem
    }
    
    static func tipoHallazgo(idGestion:Int) -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        if(idGestion == Constante.SST){
            dataItem.append(ItemSpinnerModel(id:1, nombre: "Accesos"))
            dataItem.append(ItemSpinnerModel(id:2, nombre: "Almacenamiento / Apilamiento"))
            dataItem.append(ItemSpinnerModel(id:3, nombre: "Competencias del personal"))
            dataItem.append(ItemSpinnerModel(id:4, nombre: "Condiciones de Obra - Bienestar"))
            dataItem.append(ItemSpinnerModel(id:5, nombre: "EPP"))
            dataItem.append(ItemSpinnerModel(id:6, nombre: "Equipos estacionarios"));
            dataItem.append(ItemSpinnerModel(id:7, nombre: "Equipos móviles/(Liviano - pesado)"))
            dataItem.append(ItemSpinnerModel(id:8, nombre: "Ergonomía"))
            dataItem.append(ItemSpinnerModel(id:9, nombre: "Espacios confinados"))
            dataItem.append(ItemSpinnerModel(id:10, nombre: "Gestión Documentaria"))
            dataItem.append(ItemSpinnerModel(id:11, nombre: "Gestión Vial y Transporte"))
            dataItem.append(ItemSpinnerModel(id:12, nombre: "Herramientas manuales y de poder"))
            dataItem.append(ItemSpinnerModel(id:13, nombre: "Manipulación de cargas - Izaje"))
            dataItem.append(ItemSpinnerModel(id:14, nombre: "Materiales Peligrosos"))
            dataItem.append(ItemSpinnerModel(id:15, nombre: "Señalización"))
            dataItem.append(ItemSpinnerModel(id:16, nombre: "Sistema de Protección Colectiva"))
            dataItem.append(ItemSpinnerModel(id:17, nombre: "Trabajos en altura"))
            dataItem.append(ItemSpinnerModel(id:18, nombre: "Trabajos en caliente"))
            dataItem.append(ItemSpinnerModel(id:19, nombre: "Trabajos de excavación"))
            dataItem.append(ItemSpinnerModel(id:20, nombre: "Trabajos con Energía eléctrica"))
        }else{
            dataItem.append(ItemSpinnerModel(id:21,nombre: "Manejo y disposición de residuos"))
            dataItem.append(ItemSpinnerModel(id:22,nombre: "Requisitos para manejo o almacenamiento de MATPEL"))
            dataItem.append(ItemSpinnerModel(id:23,nombre: "Baños portátiles"))
            dataItem.append(ItemSpinnerModel(id:24,nombre: "Presencia de derrames o fugas (líquidos o gases)"))
            dataItem.append(ItemSpinnerModel(id:25,nombre: "Almacenamiento de residuos peligrosos"))
            dataItem.append(ItemSpinnerModel(id:26,nombre: "Control y mitigación de polvo"))
            dataItem.append(ItemSpinnerModel(id:27,nombre: "Presencia de escombros o desmontes"))
            dataItem.append(ItemSpinnerModel(id:28,nombre: "Excavaciones o desbroce"))
            dataItem.append(ItemSpinnerModel(id:29,nombre: "Revegetación"))
            dataItem.append(ItemSpinnerModel(id:30,nombre: "Control de ruido"))
            dataItem.append(ItemSpinnerModel(id:31,nombre: "Fugas de gases (aire acondicionado, SF6)"))
            dataItem.append(ItemSpinnerModel(id:32,nombre: "Controles para la colisión de aves"))
            dataItem.append(ItemSpinnerModel(id:33,nombre: "Generación de efluentes líquidos"))
            dataItem.append(ItemSpinnerModel(id:34,nombre: "Uso de recursos hídricos"))
            dataItem.append(ItemSpinnerModel(id:35,nombre: "Accesos"))
            dataItem.append(ItemSpinnerModel(id:36,nombre: "Uso de energía eléctrica"))
            dataItem.append(ItemSpinnerModel(id:37,nombre: "Reciclaje"))
            dataItem.append(ItemSpinnerModel(id:38,nombre: "Fauna silvestre en instalaciones"))
        }
        return dataItem
    }
    
    static func actoSubEstandarSST(idASCS:Int) -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
    
        if(idASCS == Constante.ACTO){
            dataItem.append(ItemSpinnerModel(id: 1, nombre: "Operar equipo sin autorización"))
            dataItem.append(ItemSpinnerModel(id: 2, nombre: "Omisión de advertir"))
            dataItem.append(ItemSpinnerModel(id: 3, nombre: "Omisión de asegurar"))
            dataItem.append(ItemSpinnerModel(id: 4, nombre: "Operar a velocidad no permitida"))
            dataItem.append(ItemSpinnerModel(id: 5, nombre: "Desactivar dispositivos de seguridad"))
            dataItem.append(ItemSpinnerModel(id: 6, nombre: "Usar equipos defectuosos"))
            dataItem.append(ItemSpinnerModel(id: 7, nombre: "Usar equipos/herramientas sin inspección"))
            dataItem.append(ItemSpinnerModel(id: 8, nombre: "No uso de EPP correctamente"))
            dataItem.append(ItemSpinnerModel(id: 9, nombre: "Carga incorrecta"))
            dataItem.append(ItemSpinnerModel(id: 10, nombre: "Ubicación incorrecta"))
            dataItem.append(ItemSpinnerModel(id: 11, nombre: "Levantar incorrectamente"))
            
            dataItem.append(ItemSpinnerModel(id: 12, nombre: "Posición inadecuada"))
            dataItem.append(ItemSpinnerModel(id: 13, nombre: "Dar mantenimiento a equipo en operación"))
            dataItem.append(ItemSpinnerModel(id: 14, nombre: "Jugueteo"))
            dataItem.append(ItemSpinnerModel(id: 15, nombre: "Trabajar bajo influencia Alcohol y drogas"))
            dataItem.append(ItemSpinnerModel(id: 16, nombre: "Uso inapropiado de equipos o herramientas"))
            dataItem.append(ItemSpinnerModel(id: 17, nombre: "No cumplió procedimiento / estándar establecido"))
            dataItem.append(ItemSpinnerModel(id: 18, nombre: "Inadecuada inspección preoperativa"))
            dataItem.append(ItemSpinnerModel(id: 19, nombre: "Incorrecta evaluación del riesgo"))
            dataItem.append(ItemSpinnerModel(id: 20, nombre: "Operar sin bloquear energía(LoTo, Reglas de oro, ACATESE)"))
            dataItem.append(ItemSpinnerModel(id: 21, nombre: "Otros actos subestándar"))
        }else{
            dataItem.append( ItemSpinnerModel(id:1,nombre:"Guardas o Barreras Inadecuadas"));
            dataItem.append( ItemSpinnerModel(id:2,nombre:"Equipo de protección incorrecto o Inadecuado"));
            dataItem.append( ItemSpinnerModel(id:3,nombre:"Herramientas, Equipo o Materiales defectuosos o hechizos"));
            dataItem.append( ItemSpinnerModel(id:4,nombre:"Congestión o Acción Restringida"));
            dataItem.append( ItemSpinnerModel(id:5,nombre:"Sistema de Advertencia Inadecuado"));
            dataItem.append( ItemSpinnerModel(id:6,nombre:"Peligros de Incendio y Explosión"));
            dataItem.append( ItemSpinnerModel(id:7,nombre:"Orden y Limpieza deficientes/ Desorden"));
            dataItem.append( ItemSpinnerModel(id:8,nombre:"Exposición al Ruido"));
            dataItem.append( ItemSpinnerModel(id:9,nombre:"Exposición a la Radiación"));
            dataItem.append( ItemSpinnerModel(id:10,nombre:"Temperaturas Extremas"));
            dataItem.append( ItemSpinnerModel(id:11,nombre:"Iluminación Deficiente o Excesiva"));
            dataItem.append( ItemSpinnerModel(id:12,nombre:"Ventilación Inadecuada"));
            dataItem.append( ItemSpinnerModel(id:13,nombre:"Condiciones Ambientales Peligrosas"));
            dataItem.append( ItemSpinnerModel(id:14,nombre:"Accesos inadecuados"));
            dataItem.append( ItemSpinnerModel(id:15,nombre:"Escaleras portátiles o rampas sub estándares"));
            dataItem.append( ItemSpinnerModel(id:16,nombre:"Andamios y plataformas sub estándares"));
            dataItem.append( ItemSpinnerModel(id:17,nombre:"Instalaciones eléctricas en mal estado, sin protección"));
            dataItem.append( ItemSpinnerModel(id:18,nombre:"Vehículos y maquinaria rodante sub estándares"));
            dataItem.append( ItemSpinnerModel(id:19,nombre:"Equipos/herramientas sub estándares o inadecuados/ sin inspeccion"));
            dataItem.append( ItemSpinnerModel(id:20,nombre:"Falta de señalización / señalización inadecuada"));
            dataItem.append( ItemSpinnerModel(id:21,nombre:"Documentación inexistente/ inadecuada/ inviable"));
            dataItem.append( ItemSpinnerModel(id:22,nombre:"Peligros ergonómicos"));
            dataItem.append( ItemSpinnerModel(id:23,nombre:"Baños sucios"));
            dataItem.append( ItemSpinnerModel(id:24,nombre:"Equipos de emergencia: incompletos, malas condiciones."));
            
        }
        
        return dataItem
    }
    
    static func actoSubEstandarMA(idASCS:Int) -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
    
        if(idASCS == Constante.ACTO){
            dataItem.append(ItemSpinnerModel(id: 22, nombre: "Inadecuada segregación de residuos"))
            dataItem.append(ItemSpinnerModel(id: 23, nombre: "No completa el reporte mensual interno de cantidad de residuos"))
            dataItem.append(ItemSpinnerModel(id: 24, nombre: "Almacenamiento de materiales incompatibles"))
            dataItem.append(ItemSpinnerModel(id: 25, nombre: "Fugas o derrames no reportados o mal reportados"))
            dataItem.append(ItemSpinnerModel(id: 26, nombre: "Incumplimiento al plan de acción estipulado en el reporte de fuga o derrame"))
            dataItem.append(ItemSpinnerModel(id: 27, nombre: "Inadecuada segregación de residuos"))
            dataItem.append(ItemSpinnerModel(id: 28, nombre: "No cuenta con manifiestos de disposición de residuos peligrosos"))
            dataItem.append(ItemSpinnerModel(id: 29, nombre: "Falta de evidencia de cumplimiento"))
            dataItem.append(ItemSpinnerModel(id: 30, nombre: "Agua proveniente de fuentes no autorizadas"))
            dataItem.append(ItemSpinnerModel(id: 31, nombre: "Conductores no respetan la velocidad máxima"))
            dataItem.append(ItemSpinnerModel(id: 32, nombre: "Desmonte dispuesto en lugares no autorizados"))
            dataItem.append(ItemSpinnerModel(id: 33, nombre: "Extracción de suelo o material de préstamo sin autorización"))
            dataItem.append(ItemSpinnerModel(id: 34, nombre: "Efluentes vertidos sin tratamiento"))
            dataItem.append(ItemSpinnerModel(id: 35, nombre: "Uso de recursos hídricos sin autorización"))
            dataItem.append(ItemSpinnerModel(id: 36, nombre: "Uso o apertura de accesos sin autorización"))
            dataItem.append(ItemSpinnerModel(id: 37, nombre: "Equipos conectados fuera de horario laboral"))
            dataItem.append(ItemSpinnerModel(id: 38, nombre: "Luces encendidas fuera de horario laboral"))
            dataItem.append(ItemSpinnerModel(id: 39, nombre: "Falta de mecanismos de reciclaje"))
            dataItem.append(ItemSpinnerModel(id: 40, nombre: "Daño a la fauna silvestres"))
            dataItem.append(ItemSpinnerModel(id: 41, nombre: "No reportar presencia o interacción con fauna silvestre"))
        }else{
            dataItem.append(ItemSpinnerModel(id:25, nombre:"Contenedores en rotos o en mal estado"))
            dataItem.append(ItemSpinnerModel(id:26, nombre:"Contenedores repletos (>80% de su capacidad)"))
            dataItem.append(ItemSpinnerModel(id:27, nombre:"Rótulos inadecuados o en mal estado"))
            dataItem.append(ItemSpinnerModel(id:28, nombre:"Kit antiderrames ausente o incompleto"))
            dataItem.append(ItemSpinnerModel(id:29, nombre:"Líquidos sin contención ≥ al 110% de su volumen"))
            dataItem.append(ItemSpinnerModel(id:30, nombre:"Falta de impermeabilización"))
            dataItem.append(ItemSpinnerModel(id:31, nombre:"Rótulos inadecuados o en mal estado"))
            dataItem.append(ItemSpinnerModel(id:32, nombre:"No hay un baño por cada 10 personas"))
            dataItem.append(ItemSpinnerModel(id:33, nombre:"Baños sin limpieza periódica"))
            dataItem.append(ItemSpinnerModel(id:34, nombre:"Baños con fugas o en mal estado"))
            dataItem.append(ItemSpinnerModel(id:35, nombre:"Fugas sin contención"))
            dataItem.append(ItemSpinnerModel(id:36, nombre:"Contenedores repletos (>80% de su capacidad)"))
            dataItem.append(ItemSpinnerModel(id:37, nombre:"Almacenamiento superior a un año"))
            dataItem.append(ItemSpinnerModel(id:38, nombre:"Rótulos inadecuados o en mal estado"))
            dataItem.append(ItemSpinnerModel(id:39, nombre:"Zona sin regar, en caso el estudio ambiental lo exija"))
            dataItem.append(ItemSpinnerModel(id:40, nombre:"Zona con evidente polución"))
            dataItem.append(ItemSpinnerModel(id:41, nombre:"Desmonte sin disponer al final de la etapa constructiva"))
            dataItem.append(ItemSpinnerModel(id:42, nombre:"Zona de desmonte sin delimitación y señalización"))
            dataItem.append(ItemSpinnerModel(id:43, nombre:"Zona disturbada sin revegetación"))
            dataItem.append(ItemSpinnerModel(id:44, nombre:"Actividades de Revegetación sin culminar"))
            dataItem.append(ItemSpinnerModel(id:45, nombre:"Ruido superior al ECA"))
            dataItem.append(ItemSpinnerModel(id:46, nombre:"Ruido de equipos y maquinarias por falta de mantenimiento"))
            dataItem.append(ItemSpinnerModel(id:47, nombre:"Falta de desviadores de vuelo en zonas donde son obligatorias"))
            dataItem.append(ItemSpinnerModel(id:48, nombre:"Falta o inadecuado sistema de tratamiento de efluentes"))
            dataItem.append(ItemSpinnerModel(id:49, nombre:"Fugas de agua"))
            dataItem.append(ItemSpinnerModel(id:50, nombre:"Accesos no cerrados al final su uso, en caso sea obligatorio"))
            
        }
        return dataItem
    }
    
    static func obtenerBarrera() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "No lo considera riesgoso",descripcion: "A"))
        dataItem.append(ItemSpinnerModel(id: 2, nombre: "Cansancio/Fatiga",descripcion: "B"))
        dataItem.append(ItemSpinnerModel(id: 3, nombre: "Distracción",descripcion: "C"))
        dataItem.append(ItemSpinnerModel(id: 4, nombre: "Falta de entrenamiento/capacitación",descripcion: "D"))
        dataItem.append(ItemSpinnerModel(id: 5, nombre: "Presión del tiempo y/o supervisión",descripcion: "E"))
        dataItem.append(ItemSpinnerModel(id: 6, nombre: "No quiere",descripcion: "F"))
        dataItem.append(ItemSpinnerModel(id: 7, nombre: "Procedimiento",descripcion: "G"))
        dataItem.append(ItemSpinnerModel(id: 8, nombre: "Instrucción recibido",descripcion: "H"))
        dataItem.append(ItemSpinnerModel(id: 9, nombre: "No es cómodo",descripcion: "I"))
        dataItem.append(ItemSpinnerModel(id: 10, nombre: "Falta de motivación",descripcion: "J"))
        dataItem.append(ItemSpinnerModel(id: 11, nombre: "Falta experiencia",descripcion: "K"))
        dataItem.append(ItemSpinnerModel(id: 12, nombre: "Falta de control y/o supervisión",descripcion: "L"))
        dataItem.append(ItemSpinnerModel(id: 13, nombre: "Condición del equipo/ Instalación",descripcion: "M"))
        dataItem.append(ItemSpinnerModel(id: 14, nombre: "Otros (especificar)",descripcion: "N"))
        return dataItem
    }
    
    static func obtenerVerificacionItem() -> Array<VerificacionSubItem> {
        var dataItem:Array<VerificacionSubItem> = []
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:1, descripcion: "Llenado correcto de Plan de trabajo.", idVerificacionItems: 1, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:2, descripcion: "Se realizó un pre-ensamble de seguridad.", idVerificacionItems: 1, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:3, descripcion: "Puntualidad al inicio de la reunión conforme al plan de trabajos.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:4, descripcion: "Se realizó la reunion de inicio según MANOMAS.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:5, descripcion: "Se realizó la inspección de los EPP, EPC y RT antes de iniciar la actividad.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:6, descripcion: "Elaboracion del AST de acuerdo a Listado de Eventos Peligrosos y con la indicación de los controles a implementar.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:7, descripcion: "Se cuenta con certificados de calibración vigentes de los equipos de medición a utilizar en la actividad.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:8, descripcion: "l Supervisor de trabajos  cuenta con las matrices (IPERC, IEAA's), procedimientos, guía de mantenimiento y/o instructivo de mantenimiento y/o REM.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:9, descripcion: "Verificación en el diagrama unifilar que contenga la información de las condenas, demarcaciones y puestas a tierra.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:10, descripcion: "Se tienen vigentes las pólizas del SCTR para atención médica.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:11, descripcion: "Se dieron a conocer los aspectos relevantes del plan de emergencia (Rutas de evacuación, hospitales cercanos, medios de comunicación y  zonas seguras frente a sismos).", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:12, descripcion: "Se tiene personal designado para primeros auxilios.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:13, descripcion: "El asistente de la subestación estuvo presente durante la reunión de inicio.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:14, descripcion: "Se cuenta con los equipos y personal designado para rescate en altura.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:15, descripcion: "Solicitó permiso de trabajo.", idVerificacionItems: 2, idVerificacion: 1))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:16, descripcion: "Cumplimiento del ACATESE SO ATESE (Reglas de Oro), en el orden descrito", idVerificacionItems: 3, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:17, descripcion: "Verificación de Herramientas según el codigo de colores.", idVerificacionItems: 3, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:18, descripcion: "El personal utiliza, en todo momento, los equipos de protección personal necesario para la actividad.", idVerificacionItems: 4, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:19, descripcion: "Botiquín de primeros auxilios completo con insumos vigentes.", idVerificacionItems: 5, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:20, descripcion: "Vehículo de emergencia disponible; cuenta con camilla y extintor operativo.", idVerificacionItems: 5, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:21, descripcion: "Utilización de escaleras, andamios cumpliendo los estándares de seguridad vigentes.", idVerificacionItems: 6, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:22, descripcion: "Uso del sistema para detención de caídas (arnés, ganchos de escalamiento, línea de vida, entre otros) en trabajos en altura.", idVerificacionItems: 6, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:23, descripcion: "Recipientes de productos químicos etiquetados y nominados de acuerdo a hoja de seguridad.", idVerificacionItems: 7, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:24, descripcion: "Se cuenta con hojas  de seguridad de productos quimicos a utilizar (en español).", idVerificacionItems: 7, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:25, descripcion: "Los insumos químicos se encuentran sobre bandejas antiderrame y cuentan con su kit contra derrames.", idVerificacionItems: 7, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:26, descripcion: "El personal almacena sus residuos de acuerdo al código de colores.", idVerificacionItems: 8, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:27, descripcion: "Al finalizar la actividad, el área de trabajo queda libre de residuos y derrames.", idVerificacionItems: 8, idVerificacion: 2))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:28, descripcion: "Se realizó la reunión de cierre al finalizar la actividad", idVerificacionItems: 9, idVerificacion: 3))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:29, descripcion: "Los trabajos se realizaron con puntualidad, tanto en el tiempo que deben de durar como en la cantidad de actividades programadas.", idVerificacionItems: 9, idVerificacion: 3))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:30, descripcion: "Cumplimiento de las actividades de acuerdo al plan de trabajo.", idVerificacionItems: 9, idVerificacion: 3))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:31, descripcion: "Se toman acuerdos y se registran las observaciones, designando la persona que los actualiza", idVerificacionItems: 9, idVerificacion: 3))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:32, descripcion: "Se registran los pendientes y el responsable de su gestión.", idVerificacionItems: 9, idVerificacion: 3))
        dataItem.append(VerificacionSubItem(idVerificacionSubitem:33, descripcion: "Cumplimiento de las medidas de seguridad establecidos en el plan de trabajo.", idVerificacionItems: 9, idVerificacion: 3))
        
        return dataItem
    }
    
    static func getAlert(title: String, message: String, titleAction: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: titleAction, style: UIAlertAction.Style.default, handler: nil))
        return alert
    }
    
    static func obtenerCalificacion() -> Array<ItemSpinnerModel> {
           var dataItem:Array<ItemSpinnerModel> = []
           dataItem.append(ItemSpinnerModel(id: 1, nombre: "CONFORME"))
           dataItem.append(ItemSpinnerModel(id: 2, nombre: "OBSERVACIÓN"))
           dataItem.append(ItemSpinnerModel(id: 3, nombre: "NO CONFORME"))
           dataItem.append(ItemSpinnerModel(id: 4, nombre: "NO APLICA"))
           return dataItem
       }
    
    static func obtenerLugar() -> Array<ItemSpinnerModel> {
        var dataItem:Array<ItemSpinnerModel> = []
        dataItem.append(ItemSpinnerModel(id: 1, nombre: "CAMPO"))
        dataItem.append(ItemSpinnerModel(id: 2, nombre: "GABINETE"))
        return dataItem
    }
    
    static func huboDanio() -> Array<ItemSpinnerModel> {
           var dataItem:Array<ItemSpinnerModel> = []
           dataItem.append(ItemSpinnerModel(id: 1, nombre: "SI"))
           dataItem.append(ItemSpinnerModel(id: 2, nombre: "NO"))
           return dataItem
       }
}
