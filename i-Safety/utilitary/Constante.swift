//
//  Constante.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class Constante{
    
  // static var BASE_URL : String = "http://ec2-54-189-152-83.us-west-2.compute.amazonaws.com/api_isafety/"
   //static var BASE_URL_WEB :String = "http://ec2-54-189-152-83.us-west-2.compute.amazonaws.com/repienso/";
    
   //static var BASE_URL : String = "http://extranetprb.rep.com.pe/wcffusionrepienso_pdi/"
   //static var BASE_URL_WEB :String = "https://extranetprb.rep.com.pe/fusionrepienso_pdi/";
    
    static var BASE_URL : String =  "https://extranet.rep.com.pe/WcfFusionRepiensoPDI/";
    static var BASE_URL_WEB : String =  "https://extranet.rep.com.pe/I-SAfety/";
    
    // public static final String BASE_URL = "http://extranetprb.rep.com.pe/wcffusionrepienso_pdi/";
    // public static final String BASE_URL_WEB = "https://extranetprb.rep.com.pe/fusionrepienso_pdi/";
    
    static var OBJECTICO_INSPECION_INTERNA :String = "Registrar los Actos y Condiciones Subestándar para analizar las acciones a implementar.- Verificar que las actividades a realizar cuenten con los lineamientos de Seguridad, Salud y Medio Ambiente.- Identificar y controlar los factores de riesgos, impactos ambientales o desviaciones que puedan generar accidentes de trabajo, enfermedades profesionales, daños a la propiedad o contaminación al medio ambiente.";
    
    static var RESULTADO_INSPECCION :String = "Durante la inspección se logro involucrar a la linea de mando y/o responsable del area inspeccionada en los hallazgos detectados.- Fomentar el auto cuidado y mejorar las condiciones en el proyecto.- La línea de mando y / o responsable del La línea de mando y / o responsable del área inspeccionada enriqueció más sus conocimientos respecto al cumplimiento de Estandares y Manual corporativo de  seguridad, salud en el trabajo, medio ambiente, social y calidad para contratistas.";
    
    static var CONCLUSIONES_RECOMENTACIONES :String = "Cumplimiento de los objetivos de la inspeccion de seguridad, salud en el trabajo y medio ambiente.- Concientizar a los colaboradores en la necesidad de mantener una cultura de prevencion de seguridad, salud en el trabajo y medio ambiente.";
    
    static var ACTO : Int = 1
    static var CONDICION : Int = 2
    static var SST : Int = 1
    static var MA : Int = 2
}
