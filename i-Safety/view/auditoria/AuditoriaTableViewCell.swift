//
//  AuditoriaTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class AuditoriaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtCodigo: UILabel!
    @IBOutlet weak var txtFormato: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
