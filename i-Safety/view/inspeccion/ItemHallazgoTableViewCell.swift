//
//  ItemHallazgoTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 7/7/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class ItemHallazgoTableViewCell: UITableViewCell {

    @IBOutlet weak var txtEstado: UILabel!
    @IBOutlet weak var vFranja: UIView!
    @IBOutlet weak var vCard: UIView!
    @IBOutlet weak var txtUsuario: UILabel!
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    @IBOutlet weak var txtTipoGestion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vFranja.layer.cornerRadius = 10
        vFranja.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        vCard.layer.cornerRadius = 10
        vCard.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
