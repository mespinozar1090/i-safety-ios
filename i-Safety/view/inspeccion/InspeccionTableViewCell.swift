//
//  InspeccionTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InspeccionTableViewCell: UITableViewCell {

    @IBOutlet weak var txtCodigo: UILabel!
    @IBOutlet weak var txtUsuario: UILabel!
    @IBOutlet weak var txtSede: UILabel!
    @IBOutlet weak var txtProyecto: UILabel!
    @IBOutlet weak var txtEstado: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
