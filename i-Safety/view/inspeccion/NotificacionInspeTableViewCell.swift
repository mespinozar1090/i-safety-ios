//
//  NotificacionInspeTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 10/28/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class NotificacionInspeTableViewCell: UITableViewCell {

    @IBOutlet weak var btnImagen: UIButton!
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var txtCorreo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
