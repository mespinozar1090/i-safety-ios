//
//  NotificarTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 8/5/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class NotificarTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblCorreo: UILabel!
    @IBOutlet weak var btnImagen: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
