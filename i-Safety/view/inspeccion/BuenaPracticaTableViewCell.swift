//
//  BuenaPracticaTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 7/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class BuenaPracticaTableViewCell: UITableViewCell {
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var btnImage: ButtonRadius!
    @IBOutlet weak var txtMessage: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
