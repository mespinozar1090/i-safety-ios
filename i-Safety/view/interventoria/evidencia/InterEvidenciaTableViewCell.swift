//
//  InterEvidenciaTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 12/9/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InterEvidenciaTableViewCell: UITableViewCell {

    @IBOutlet weak var btnImagen: ButtonRadius!
    @IBOutlet weak var txtDescripcion: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
