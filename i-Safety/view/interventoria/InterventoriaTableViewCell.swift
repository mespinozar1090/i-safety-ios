//
//  InterventoriaTableViewCell.swift
//  i-Safety
//
//  Created by usuario on 7/9/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InterventoriaTableViewCell: UITableViewCell {

    @IBOutlet weak var txtCodigo: UILabel!
    @IBOutlet weak var txtUsuario: UILabel!
    @IBOutlet weak var txtNombreFormato: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
