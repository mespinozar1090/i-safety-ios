//
//  SbcModel.swift
//  i-Safety
//
//  Created by usuario on 7/8/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class SbcModel{
    
    var idSbc:Int?
    var estadoSBC:Int?
    var fechaRegistro:String?
    var nombreFormato:String?
    var nombreObservador:String?
    var codigoSBC:String?
    var observacion:String?
    var idUsuario:String?
    var idEmpresaObservada:String?

}
