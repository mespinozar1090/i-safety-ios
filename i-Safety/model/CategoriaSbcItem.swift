//
//  CategoriaSbcItem.swift
//  i-Safety
//
//  Created by usuario on 9/20/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class CategoriaSbcItem {
    
    var idSbcDetalle:Int?
    var idsbcCategoriaItems:Int?
    var descripcionItem:String?
    var idsbcCategoria:Int?
    var riesgoso:Bool = false
    var seguro:Bool = false
    var idBarrera:Int = 1
    var observacionSbcDetalle:String?
    var idSbc:Int?
    
}
