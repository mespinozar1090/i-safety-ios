//
//  DetalleInspeccionModel.swift
//  i-Safety
//
//  Created by usuario on 7/2/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class DetalleInspeccionModel{
    
    var idInspecciones:Int?
    var idEmpresaContratista:Int?
    var codigoInspeccion:String?
    var idTipoUbicacion:Int?
    var idProyecto:Int?
    var idLugar:Int?
    var idUsuario:Int?
    var idEmpresaObservadora:Int?
    var tipoInspeccion:Int?
    var responsableAreaInspeccion:String?
    var torre:String?
    var areaInspecionada:String?
    var lstDetalleInspeccion:Array<ItemHallazgoModel>?
    var nombreResponsableInspeccion:String?
    var usuarioRolInspecion:Int?
    
}
 
