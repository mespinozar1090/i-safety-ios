//
//  EmpresaProyectoModel.swift
//  i-Safety
//
//  Created by usuario on 7/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class EmpresaProyectoModel {
    
    var idEmpresaMaestra:Int?
    var razonSocial:String?
    var ruc:String?
    var descripcion:String?
    var domicilioLegal:String?
    var nroTrabajadores:Int?
    var idActividadEconomica:Int?
    var idParam:Int?
    
}


