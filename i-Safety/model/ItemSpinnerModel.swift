//
//  ItemSpinnerModel.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class ItemSpinnerModel{
    var id:Int?
    var nombre:String?
    var descripcion:String?
    
    init() {
    }
    
   init(id:Int,nombre:String) {
        self.id = id
        self.nombre = nombre
    }
    
    init(id:Int,nombre:String,descripcion:String) {
        self.id = id
        self.nombre = nombre
        self.descripcion = descripcion
    }
}
