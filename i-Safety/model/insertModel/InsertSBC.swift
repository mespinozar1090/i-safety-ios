//
//  InsertSBC.swift
//  i-Safety
//
//  Created by usuario on 9/23/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class InsertSBC {
    var idSbc:Int?
    var nombreObservador:String?
    var idSedeProyecto:Int?
    var cargoObservador:String?
    var idLugarTrabajo:String?
    var fechaRegistro:String?
    var idEmpresaObservador:Int?
    var horarioObservacion:Int?
    var tiempoExpObservada:Int?
    var especialidadObservado:Int?
    var actividad:String?
    var idAreaTrabajo:Int?
    var descripcionAreaObservada:String?
    var idSedeProyectoObservado:Int?
    var estadoSBC:Bool?
    var idUsuario:Int?
}
