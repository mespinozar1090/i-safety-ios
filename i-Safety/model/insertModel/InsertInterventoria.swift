//
//  InsertInterventoria.swift
//  i-Safety
//
//  Created by usuario on 9/27/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class InsertInterventoria {
    var idInteventoria:Int?
    var actividad:String?
    var conclucion:String?
    var empresaIntervenida:Int?
    var empresaInterventor:Int?
    var estado:Bool?
    var fechaHora:String?
    var fechaRegistro:String?
    var idPlanTrabajo:Int?
    var planTrabajo:Int?
    var idUsuario:Int?
    var lineaSubEstacion:Int?
    var lugarZona:Int?
    var nroPlanTrabajo:String?
    var supervisor:String?
    var supervisorSustituto:String?
    var interventor:String?
}
