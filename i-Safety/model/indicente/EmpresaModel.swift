//
//  EmpresaModel.swift
//  i-Safety
//
//  Created by usuario on 10/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class EmpresaModel{
    var idEmpresaMaestra:Int?
    var razonsocial:String?
    var ruc:Int?
    var domicilioLegal:String?
    var nroTrabajadores:Int?
    var idActividadEconomica:Int?
    var idTamanioEmpresa:Int?
}
