//
//  EventoIncidenteModel.swift
//  i-Safety
//
//  Created by usuario on 10/7/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class EventoIncidenteModel{
    var fechaAccidente:String?
    var fechaInicioInvestigacion:String?
    var horaAccidente:String?
    var huboDanioMaterial:String?
    var idEquipoAfectado:Int?
    var nombreEquipoAfectado:String?
    var idParteAfectada:Int?
    var nombreParteAfectada:String?
    var idTipoEvento:Int?
    var nombreTipoEvento:String?
    
    var lugarExacto:String?
    var numeroTrabajadoresAfectados:String?
    var trabajoDetalleCategoriaOcupacional:String?
    var idSedeProyecto:Int?
    var nombreSedeProyecto:String?
    
    var fDescripcionIncidente:String?
    var descripcionSede:String?
    var fEventoFechaInicioInvestigacion:String?
    var fTrabajadorDetalleCategoriaOcupacional:String?
}
