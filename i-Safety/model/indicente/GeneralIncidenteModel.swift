//
//  GeneralIncidenteModel.swift
//  i-Safety
//
//  Created by usuario on 10/7/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class GeneralIncidenteModel {
    var idIncidente:Int?
    var empleadorTipo:String?
    var fEmpleadorIdActividadEcomica:Int?
    var fEmpleadorIdEmpresa:Int?
    var fEmpleadorIdTamanioEmpresa:Int?
    var fEmpleadorRazonSocial:String?
    var fEmpleadorRuc:String?
    var fTrabajadorNombresApellidos:String?
    var tipoInforme:String?
    var fEmpleadorInterIdActividadEconomica:Int?
    var fEmpleadorInterRazonSocial:String?
    var fEmpleadorInterRuc:String?
    var gDescripcionFormato:String?
    var fDescripcionIncidente:String?
    var idUsuarioCreador:Int?
    
    var nombreActividadEconomica:String?
    var fNombreTamnioEmpresa:String?
    
     var interNombreActividadEco:String?
}
