//
//  BuenaPracticaModel.swift
//  i-Safety
//
//  Created by usuario on 7/1/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class BuenaPracticaModel{
    
    var idBuenasPracticas:Int?
    var idCategoriaBuenaPractica:Int?
    var nombre:String?
    var descripcion:String?
    var imgBase64:String?
}
