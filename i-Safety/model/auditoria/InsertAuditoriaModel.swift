//
//  InsertAuditoriaModel.swift
//  i-Safety
//
//  Created by usuario on 10/6/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class InsertAuditoriaModel {
    var idAuditoria:Int?
    var estadoAuditoria:Int?
    var fechaAuditoria:String?
    var idEmpresaContratista:Int?
    var idUser:Int?
    var nombreFormato:String?
    var nroContrato:String?
    var sede:Int?
    var supervisorContrato:String?
    var responsableContratista:String?
}
