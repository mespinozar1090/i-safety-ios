//
//  LineamientoSubItem.swift
//  i-Safety
//
//  Created by usuario on 9/29/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class LineamientoSubItem{
    var idAuditLinSubItems:Int?
    var descripcionItems:String?
    var idAuditItems:Int?
    var idCalificacion:Int?
    var idLugar:Int?
    var imgbase64:String?
    var mensage:String?
    var idAuditDetalle:Int?
    var idAuditoria:Int?
}
