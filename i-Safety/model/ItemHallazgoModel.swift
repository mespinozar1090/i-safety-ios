//
//  ItemHallazgoModel.swift
//  i-Safety
//
//  Created by usuario on 7/2/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class ItemHallazgoModel{

    var id:Int?
    var idInspeccion:Int?
    var actoSubestandar:String?
    var riesgo:Int?
    var resposableAreadetalle:String?
    var plazo:String?
    var idSubFamiliaAmbiental:Int?
    var idActoSubestandar:Int?
    var idTipoGestion:Int?
    var idCondicionSubestandar:Int?
    var nombreUsuario:String?
    var idTipoUbicacion:Int?
    var idHallazgo:Int?
    var imgFotoHallazgo:String?
    var nombreTipoGestion:String?
    var nombreHallazgo:String?
    var nombreActoSubEstandar:String?
    var estadoDetalleInspeccion:Bool?
    var fechaFoto:String?
    var usuarioRol:Int?
    
    var accionMitigadora:String?
    var imgCierre:String?
    var fechaImgCierre:String?
    
}
