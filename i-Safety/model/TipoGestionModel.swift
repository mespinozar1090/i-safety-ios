//
//  TipoGestionModel.swift
//  i-Safety
//
//  Created by usuario on 7/2/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class TipoGestionModel {
    
    var id:Int?
    var nombre:String?
    
    init(id:Int,nombre:String) {
        self.id = id
        self.nombre = nombre
    }
    
}
