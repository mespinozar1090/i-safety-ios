//
//  InterventoriaModel.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class InterventoriaModel{
    
    var idInterventoria:Int?
    var estado:Int?
    var fechaRegistro:String?
    var nombreFormato:String?
    var nombreUsuario:String?
    var codigo:String?
    var idUsuario:Int?
    var idEmpresaIntervenida:Int?
    var idEmpresaInterventor:Int?
    var empresaIntervenida:String?
    
}
