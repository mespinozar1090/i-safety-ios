//
//  InspeccionModel.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class InspeccionModel{
    
    var id:Int?
    var estado:Int?
    var fechaRegistro:String?
    var nombreFormato:String?
    var nombreUsuario:String?
    var codigo:String?
    var idUsuario:Int?
    var idEmpresaInspeccionada:Int?
    var idEmpresaInspector:Int?
    var sede:String?
    var empresaContratista:String?
    var idProyecto:Int?
}



