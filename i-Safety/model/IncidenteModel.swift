//
//  IncidenteModel.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class IncidenteModel {
    
    var idIncidente:Int?
    var gFechaCreado:String?
    var gDescripcionFormato:String?
    var tipoInforme:String?
}
