//
//  ControlSubItem.swift
//  i-Safety
//
//  Created by usuario on 9/28/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class ControlSubItem {
    var idControlitem:Int?
    var idControl:Int?
    var descripcion:String?
    var idControlDetalle:Int?
    var idInterventoria:Int?
    var cantidad:Int = 0
}
