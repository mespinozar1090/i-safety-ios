//
//  VerificacionSubItem.swift
//  i-Safety
//
//  Created by usuario on 9/27/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class VerificacionSubItem{
    var idInterventoriaDetalle:Int?
    var idVerificacionSubitem:Int?
    var descripcion:String?
    var idVerificacionItems:Int?
    var idVerificacion:Int?
    var idInterventoria:Int?
    var cumple:Int = 1
    
    init() {
     }
     
    init(idVerificacionSubitem:Int,descripcion:String,idVerificacionItems:Int,idVerificacion:Int) {
         self.idVerificacionSubitem = idVerificacionSubitem
         self.descripcion = descripcion
        self.idVerificacionItems = idVerificacionItems
        self.idVerificacion = idVerificacion
     }
}
