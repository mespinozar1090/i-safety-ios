//
//  AuditoriaModel.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
class AuditoriaModel{

    var idAudit:Int?
    var idEmpresacontratista:Int?
    var sede:Int?
    var estadoAudit:Int?
    var fechaReg:String?
    var nombreFormato:String?
    var codigoAudit:String?
    
}
