//
//  SbcViewModel.swift
//  i-Safety
//
//  Created by usuario on 7/8/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class SbcViewModel{
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    func listarSBC(params: NSDictionary,callback: @escaping (Array<SbcModel>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).listarSBC(params: params,callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<SbcModel> = []
                    for index in 0...result.count-1 {
                        let item = SbcModel()
                        item.idSbc = result[index].value(forKey: "Id_sbc") as? Int
                        item.estadoSBC = result[index].value(forKey: "Estado_SBC") as? Int
                        item.fechaRegistro = result[index].value(forKey: "Fecha_Registro") as? String
                        item.nombreFormato = result[index].value(forKey: "Nombre_Formato") as? String
                        item.nombreObservador = result[index].value(forKey: "Nombre_Observador") as? String
                        item.codigoSBC = result[index].value(forKey: "Codigo_SBC") as? String
                        item.observacion = result[index].value(forKey: "Observacion") as? String
                        item.idUsuario = result[index].value(forKey: "IdUsuario") as? String
                        item.idEmpresaObservada = result[index].value(forKey: "Id_Empresa_Observada") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    
    func obtenerSede(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerSede(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "IdSede_Proyecto") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    
    func obtenerEmpresaSBSResponse(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerEmpresaSBSResponse(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Empresa_Observadora") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func obtenerEspecialidad(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerEspecialidad(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Especialidad") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func obtenerAreaInspeccion(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerAreaInspeccion(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Area_inspeccionada") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func obtenerCategoriaSBC(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerCategoriaSBC(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_sbc_Categoria") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion_Categoria") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    
    func obtenerCategoriaItemSBC(callback: @escaping (Array<CategoriaSbcItem>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerCategoriaItemSBC(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<CategoriaSbcItem> = []
                    for index in 0...result.count-1 {
                        let item = CategoriaSbcItem()
                        item.idsbcCategoriaItems = result[index].value(forKey: "Id_sbc_Categoria_Items") as? Int
                        item.descripcionItem = result[index].value(forKey: "Descripcion_Item") as? String
                        item.idsbcCategoria = result[index].value(forKey: "Id_sbc_Categoria") as? Int
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func crearRegistroSBC(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).crearRegistroSBC(params: params, notificar: notificar, evaluacion: evaluacion,callback: {(success,error) in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                        callback(estatus,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func obtenerRegistroSBC(idSBC: Int,callback: @escaping (InsertSBC?,Array<CategoriaSbcItem>?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).obtenerRegistroSBC(idSBC: idSBC, callback: {(success,error) in
            if let result = success {
                let objectSBC = InsertSBC()
                objectSBC.idSbc = result.value(forKey: "Id_sbc") as? Int
                objectSBC.nombreObservador = result.value(forKey: "Nombre_observador") as? String
                objectSBC.idLugarTrabajo = "27"
                objectSBC.fechaRegistro = result.value(forKey: "Fecha_registro") as? String
                objectSBC.idEmpresaObservador = result.value(forKey: "Id_Empresa_Observadora") as? Int
                objectSBC.horarioObservacion = Int(result.value(forKey: "Horario_Observacion") as! String)
                objectSBC.tiempoExpObservada = result.value(forKey: "Tiempo_exp_observada") as? Int
                objectSBC.especialidadObservado = result.value(forKey: "Especialidad_Observado") as? Int
                objectSBC.actividad = result.value(forKey: "Actividad_Observada") as? String
                objectSBC.idAreaTrabajo = result.value(forKey: "Id_Area_Trabajo") as? Int
                objectSBC.estadoSBC = result.value(forKey: "Estado_SBC") as? Bool
                objectSBC.idUsuario = result.value(forKey: "IdUsuario") as? Int
                objectSBC.idSedeProyecto = result.value(forKey: "IdSede_Proyecto") as? Int
                objectSBC.idSedeProyectoObservado = result.value(forKey: "IdSede_Proyecto_Observado") as? Int
                objectSBC.cargoObservador = result.value(forKey: "Cargo_Observador") as? String
               
                var response:Array<CategoriaSbcItem> = []
                if let categoria = result.value(forKey: "lstDetalleSBC") as? Array<NSDictionary> {
                    if(categoria.count>=1){
                        for index in 0...categoria.count-1 {
                            let object = CategoriaSbcItem()
                            object.idSbcDetalle = categoria[index].value(forKey: "Id_sbc_detalle") as? Int
                            object.idsbcCategoria = categoria[index].value(forKey: "Id_sbc_Categoria") as? Int
                            object.idsbcCategoriaItems = categoria[index].value(forKey: "Id_sbc_Categoria_Items") as? Int
                            object.seguro = categoria[index].value(forKey: "Seguro") as? Bool ?? false
                            object.riesgoso = categoria[index].value(forKey: "Riesgoso") as? Bool ?? false
                            object.idBarrera = categoria[index].value(forKey: "Idbarrera") as? Int ?? 0
                            object.observacionSbcDetalle = categoria[index].value(forKey: "Observacion_sbc_detalle") as? String
                            object.descripcionItem = categoria[index].value(forKey: "Descripcion_Item") as? String
                            object.idSbc = categoria[index].value(forKey: "Id_sbc") as? Int
                            response.append(object)
                        }
                        
                    }
                }
                callback(objectSBC,response,nil)
            }else{
                 callback(nil,nil,error)
            }
        })
    }
    
    
    func editarRegistroSBS(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        SbcNetWork.getInstance(parent: self.parent!).editarRegistroSBC(params: params, notificar: notificar, evaluacion: evaluacion,callback: {(success,error) in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                        callback(estatus,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func eliminarSBC(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()){
         SbcNetWork.getInstance(parent: self.parent!).eliminarSBC(params: params,  callback: {(success,error) in
             if let result = success {
                 if(result){
                     callback(true,nil)
                 }else{
                     callback(false,nil)
                 }
             }else{
                 callback(false,error)
             }
         })
     }
}
