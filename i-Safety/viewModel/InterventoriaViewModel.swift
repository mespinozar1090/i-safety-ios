//
//  InterventoriaViewModel.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InterventoriaViewModel {
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    //listarInterventoriaV2
    func listarInterventoria(callback: @escaping (Array<InterventoriaModel>?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).listarInterventoria(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<InterventoriaModel> = []
                    for index in 0...result.count-1 {
                        let item = InterventoriaModel()
                        item.idInterventoria = result[index].value(forKey: "Id_Interventoria") as? Int
                        item.estado = result[index].value(forKey: "Estado") as? Int
                        item.fechaRegistro = result[index].value(forKey: "Fecha_Registro") as? String
                        item.nombreFormato = result[index].value(forKey: "Nombre_Formato") as? String
                        item.nombreUsuario = result[index].value(forKey: "Nombre_Usuario") as? String
                        item.codigo = result[index].value(forKey: "Codigo") as? String
                        item.idUsuario = result[index].value(forKey: "IdUsuario") as? Int
                        item.idEmpresaIntervenida = result[index].value(forKey: "id_Empresa_Intervenida") as? Int
                        item.idEmpresaInterventor = result[index].value(forKey: "Id_Empresa_Interventor") as? Int
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func listarInterventoriaV2(params: NSDictionary,callback: @escaping (Array<InterventoriaModel>?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).listarInterventoriaV2(params:params,callback: {(success,error) in
            if let result = success {
                if let data = result.value(forKey: "Data") as? Array<NSDictionary> {
                    if(data.count > 0){
                        var response:Array<InterventoriaModel> = []
                        print("data countsss \(data.count)")
                        for index in 0...data.count-1 {
                            let item = InterventoriaModel()
                            item.idInterventoria = data[index].value(forKey: "Id") as? Int
                            item.estado = data[index].value(forKey: "Estado") as? Int
                            item.fechaRegistro = data[index].value(forKey: "Fecha_Registro") as? String
                            item.nombreFormato = data[index].value(forKey: "Nombre_Formato") as? String
                            item.nombreUsuario = data[index].value(forKey: "Nombre_Observador") as? String
                            item.codigo = data[index].value(forKey: "Codigo") as? String
                            item.idUsuario = data[index].value(forKey: "IdUsuario") as? Int
                            item.idEmpresaIntervenida = data[index].value(forKey: "id_Empresa_Intervenida") as? Int
                            item.idEmpresaInterventor = data[index].value(forKey: "Id_Empresa_Interventor") as? Int
                            response.append(item)
                        }
                        callback(response,nil)
                    }else{
                        callback(nil,error)
                    }
                }else{
                    callback(nil,error)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerSubEstacion(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).obtenerSubEstacion(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Linea_Subestacion") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerVerificacionSubItem(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).obtenerVerificacionSubItem(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Verificacion_Subitem") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerControlSubItem(callback: @escaping (Array<ControlSubItem>?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).obtenerControlSubItem(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ControlSubItem> = []
                    for index in 0...result.count-1 {
                        let item = ControlSubItem()
                        item.idControlitem = result[index].value(forKey: "Id_Control_item") as? Int
                        item.idControl = result[index].value(forKey: "Id_Control") as? Int
                        item.descripcion = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    
    func crearRegistroInterventoria(params: NSDictionary,notificar: Array<NSDictionary>,verficacion: Array<NSDictionary>,controlpersonal: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).crearRegistroInterventoria(params: params, notificar: notificar, verificaciones: verficacion, controlInterventoria: controlpersonal, callback: {(success,error)in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                    callback(estatus,nil)
                }else{
                    callback(nil,error)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerRegistroInterventoria(idInterventoria: Int,callback: @escaping (InsertInterventoria?,Array<VerificacionSubItem>?,Array<ControlSubItem>?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).obtenerRegistroInterventoria(idInterventoria: idInterventoria, callback: {(success,error) in
            if let result = success {
                if let data = result.value(forKey: "Data") as? NSDictionary {
                    
                    let object = InsertInterventoria()
                    object.idPlanTrabajo = Int(data.value(forKey: "Plan_Trabajo") as? String ?? "0")
                    object.supervisor = data.value(forKey: "Supervisor") as? String
                    object.supervisorSustituto = data.value(forKey: "Supervisor_Sustituto") as? String
                    object.idInteventoria = data.value(forKey: "Id_interventoria") as? Int
                    object.idUsuario = data.value(forKey: "IdUsuario") as? Int
                    object.empresaInterventor = data.value(forKey: "Empresa_Interventor") as? Int
                    object.empresaIntervenida = data.value(forKey: "Id_Empresa_Intervenida") as? Int
                    object.fechaRegistro = data.value(forKey: "Fecha_Registro") as? String
                    object.interventor = data.value(forKey: "Interventor") as? String
                    object.fechaHora = data.value(forKey: "Fecha_Hora") as? String
                    object.actividad = data.value(forKey: "Actividad") as? String
                    object.empresaIntervenida = Int(data.value(forKey: "Empresa_Intervenida") as? String ?? "0")
                    object.lineaSubEstacion = Int(data.value(forKey: "Linea_Subestacion") as? String ?? "0")
                    object.nroPlanTrabajo = data.value(forKey: "Nro_Plan_Trabajo") as? String
                    object.planTrabajo = Int(data.value(forKey: "Plan_Trabajo") as? String ?? "0")
                    object.lugarZona = Int(data.value(forKey: "Lugar_Zona") as? String ?? "0")
                    object.fechaRegistro = data.value(forKey: "Fecha_Registro_Log") as? String
                    object.conclucion = data.value(forKey: "Concluciones") as? String
                    
                    var verificacion:Array<VerificacionSubItem> = []
                    if let categoria = data.value(forKey: "LstVerificacionesDet") as? Array<NSDictionary> {
                        if(categoria.count>=1){
                            for index in 0...categoria.count-1 {
                                let object = VerificacionSubItem()
                                object.idInterventoriaDetalle = categoria[index].value(forKey: "Id_Interventoria_Detalle") as? Int
                                object.cumple = categoria[index].value(forKey: "Cumple") as? Int ?? 1
                                object.idVerificacionSubitem = categoria[index].value(forKey: "Id_Verificacion_Subitem") as? Int
                                object.idVerificacion = categoria[index].value(forKey: "Id_Verificacion_Padre") as? Int
                                object.idInterventoria = categoria[index].value(forKey: "Id_Interventoria") as? Int
                                object.descripcion = categoria[index].value(forKey: "Descripcion") as? String
                                object.idVerificacionItems = categoria[index].value(forKey: "Id_Verificacion_Item") as? Int
                                verificacion.append(object)
                            }
                            
                        }
                    }
                    
                    var controlPersonal:Array<ControlSubItem> = []
                    if let categoria = data.value(forKey: "LstControlesDet") as? Array<NSDictionary> {
                        if(categoria.count>=1){
                            for index in 0...categoria.count-1 {
                                let object = ControlSubItem()
                                object.idControlitem = categoria[index].value(forKey: "Id_Control_Item") as? Int
                                object.cantidad = categoria[index].value(forKey: "Cantidad") as? Int ?? 0
                                object.idControlDetalle = categoria[index].value(forKey: "Id_Control_Detalle") as? Int
                                object.idControl = categoria[index].value(forKey: "Id_Control") as? Int
                                object.idInterventoria = categoria[index].value(forKey: "Id_Interventoria") as? Int
                                object.descripcion = categoria[index].value(forKey: "Descripcion") as? String
                                controlPersonal.append(object)
                            }
                            
                        }
                    }
                    callback(object,verificacion,controlPersonal,nil)
                }else{
                    callback(nil,nil,nil,nil)
                }
            }else{
                callback(nil,nil,nil,nil)
            }
            
        })
    }
    
    func editarRegistroInterventoria(params: NSDictionary,notificar: Array<NSDictionary>,verficacion: Array<NSDictionary>,controlpersonal: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).editarRegistroInterventoria(params: params, notificar: notificar, verificaciones: verficacion, controlInterventoria: controlpersonal, callback: {(success,error)in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                    callback(estatus,nil)
                }else{
                    callback(nil,error)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func eliminarInterventoria(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()){
        InterventoriaNetWork.getInstance(parent: self.parent!).eliminarInterventoria(params: params,  callback: {(success,error) in
            if let result = success {
                if(result){
                    callback(true,nil)
                }else{
                    callback(false,nil)
                }
            }else{
                callback(false,error)
            }
        })
    }
    
}
