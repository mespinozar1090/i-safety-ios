//
//  AuditoriaViewModel.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit
class AuditoriaViewModel{
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    func listaInpecciones(idUsers: Int, callback: @escaping (Array<AuditoriaModel>?, NSError?) -> ()){
        
        AuditoriaNetWork.getInstance(parent: self.parent!).listarInterventoria(idUsers: idUsers, callback: {(success,error)in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<AuditoriaModel> = []
                    print("result \(result.count)")
                    for index in 0...result.count-1 {
                        let item = AuditoriaModel()
                        item.idAudit = result[index].value(forKey: "Id_Audit") as? Int
                        item.nombreFormato = result[index].value(forKey: "Nombre_Formato") as? String
                        item.codigoAudit = result[index].value(forKey: "Codigo_Audit") as? String
                        item.fechaReg = result[index].value(forKey: "Fecha_Reg") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func listaAuditoriaV2(params:NSDictionary, callback: @escaping (Array<AuditoriaModel>?, NSError?) -> ()){
        
        AuditoriaNetWork.getInstance(parent: self.parent!).listarInterventoriaV2(params: params, callback: {(success,error)in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<AuditoriaModel> = []
                    print("result \(result.count)")
                    for index in 0...result.count-1 {
                        let item = AuditoriaModel()
                        item.idAudit = result[index].value(forKey: "Id_Audit") as? Int
                        item.nombreFormato = result[index].value(forKey: "Nombre_Formato") as? String
                        item.codigoAudit = result[index].value(forKey: "Codigo_Audit") as? String
                        item.fechaReg = result[index].value(forKey: "Fecha_Reg") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    
    func obtenerLineaminetos(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        AuditoriaNetWork.getInstance(parent: self.parent!).obtenerLineaminetos(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Audit_Items") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func listarIncidente(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        AuditoriaNetWork.getInstance(parent: self.parent!).listarIncidente(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Audit_Items") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerLineaminetosSub(callback: @escaping (Array<LineamientoSubItem>?, NSError?) -> ()){
        AuditoriaNetWork.getInstance(parent: self.parent!).obtenerLineaminetosSub(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<LineamientoSubItem> = []
                    for index in 0...result.count-1 {
                        let item = LineamientoSubItem()
                        item.idAuditLinSubItems = result[index].value(forKey: "Id_Audit_Lin_SubItems") as? Int
                        item.descripcionItems = result[index].value(forKey: "Descripcion_Items") as? String
                        item.idAuditItems = result[index].value(forKey: "Id_Audit_Items") as? Int
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func crearRegistroAuditoria(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        AuditoriaNetWork.getInstance(parent: self.parent!).crearRegistroAuditoria(params: params, notificar: notificar, evaluacion: evaluacion,callback: {(success,error) in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                    callback(estatus,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func editarRegistroAuditoria(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        AuditoriaNetWork.getInstance(parent: self.parent!).editarRegistroAuditoria(params: params, notificar: notificar, evaluacion: evaluacion,callback: {(success,error) in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                    callback(estatus,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    
    func obtenerRegistroAuditoria(idAuditoria: Int,callback: @escaping (InsertAuditoriaModel?,Array<LineamientoSubItem>?, NSError?) -> ()){
        AuditoriaNetWork.getInstance(parent: self.parent!).obtenerRegistroAuditoria(idAuditoria: idAuditoria, callback: {(success,error) in
            if let result = success {
                let object = InsertAuditoriaModel()
                object.idAuditoria = result.value(forKey: "Id_Audit") as? Int
                object.idEmpresaContratista = result.value(forKey: "Id_Empresa_contratista") as? Int
                object.sede = result.value(forKey: "Sede") as? Int
                object.nroContrato = result.value(forKey: "Nro_Contrato") as? String
                object.supervisorContrato = result.value(forKey: "Supervisor_Contrato") as? String
                object.responsableContratista = result.value(forKey: "Responsable_Contratista") as? String
                object.fechaAuditoria = result.value(forKey: "Fecha_Reg") as? String
                object.estadoAuditoria = result.value(forKey: "Estado_Audit") as? Int
                object.idUser = result.value(forKey: "Id_Usuario") as? Int
    
                var response:Array<LineamientoSubItem> = []
                if let evaluacion = result.value(forKey: "lstDetalleAudit") as? Array<NSDictionary>{
                    if(evaluacion.count>=1){
                        for index in 0...evaluacion.count-1 {
                            let lineamiento = LineamientoSubItem()
                            lineamiento.idAuditoria = evaluacion[index].value(forKey: "Id_Audit") as? Int
                            lineamiento.idAuditDetalle = evaluacion[index].value(forKey: "Id_Audit_Detalle") as? Int
                            lineamiento.idAuditItems = evaluacion[index].value(forKey: "Id_Audit_Items") as? Int
                            lineamiento.idAuditLinSubItems = evaluacion[index].value(forKey: "Id_Audit_Lin_SubItems") as? Int
                            lineamiento.idCalificacion = evaluacion[index].value(forKey: "Calificacion") as? Int
                            lineamiento.mensage = evaluacion[index].value(forKey: "Notas") as? String
                            lineamiento.idLugar = evaluacion[index].value(forKey: "Lugar") as? Int
                            lineamiento.descripcionItems = evaluacion[index].value(forKey: "Descripcion_Items") as? String
                            
                            if let evidencia = evaluacion[index].value(forKey: "LstEvidencia") as? Array<NSDictionary>{
                                if(evidencia.count>=1){
                                    for index in 0...evidencia.count-1 {
                                        lineamiento.imgbase64 =  evidencia[index].value(forKey: "Imagen") as? String
                                    }
                                }
                            }
                            response.append(lineamiento)
                        }
                    }
                }
                callback(object,response,nil)
            }else{
                callback(nil,nil,error)
            }
        })
    }
    
}
