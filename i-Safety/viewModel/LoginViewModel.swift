//
//  LoginViewModel.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class LoginViewModel {
    
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    func loginAcceso(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()){
        LoginNetWork.getInstance(parent: self.parent!).loginAcceso(params: params, callback: {(success,error) in
            if let result = success {
                if let data = result.value(forKey: "Data") as? NSObject {
                    if(data.value(forKey: "IdUsers") as? Int !=  nil){
                        print("rol : \(data.value(forKey: "Id_Rol_Usuario") as? Int)")
                        let defaults = UserDefaults.standard
                        defaults.set(data.value(forKey: "IdUsers") as? Int, forKey: "idUsers")
                        defaults.set(data.value(forKey: "Usuario_Login") as? String, forKey: "usuario")
                        defaults.set(data.value(forKey: "Nombre_Usuario") as? String, forKey: "nombreUsuario")
                        defaults.set(data.value(forKey: "Descripcion_Rol") as? String, forKey: "Descripcion_Rol")
                        defaults.set(data.value(forKey: "Id_Rol_Usuario") as? Int, forKey: "Id_Rol_Usuario")
                        defaults.set(data.value(forKey: "Id_Rol_General") as? Int, forKey: "Id_Rol_General")
                        defaults.set(data.value(forKey: "IdPerfil_Usuario") as? Int, forKey: "IdPerfil_Usuario")
                        defaults.set(data.value(forKey: "Descripcion_Rol_Usuario") as? String, forKey: "Descripcion_Rol_Usuario")
                        defaults.synchronize()
                        callback(true,error)
                    }else{
                        callback(false,error)
                    }
                }
            }else{
                callback(false,error)
            }
        })
    }
}

