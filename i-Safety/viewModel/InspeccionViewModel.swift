//
//  InspeccionViewModel.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit

class InspeccionViewModel{
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    func listaInpecciones(params: NSDictionary, callback: @escaping (Array<InspeccionModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).listaInpeccionesV2(params: params,  callback: {(success,error) in
            if let result = success{
                if(result.count > 0) {
                    var response:Array<InspeccionModel> = []
                    for index in 0...result.count-1 {
                        let object = InspeccionModel()
                        object.id = result[index].value(forKey: "Id_Inspecciones") as? Int
                        object.codigo = result[index].value(forKey: "CodigoInspeccion") as? String
                        object.estado = result[index].value(forKey: "Estado_Inspeccion") as? Int
                        object.fechaRegistro = result[index].value(forKey: "Fecha_Registro_Log") as? String
                        object.nombreFormato = result[index].value(forKey: "Nombre_Formato") as? String
                        object.nombreUsuario = result[index].value(forKey: "NombreAutor") as? String
                        object.idUsuario = result[index].value(forKey: "Id_Usuario_Registro") as? Int
                        object.idEmpresaInspeccionada = result[index].value(forKey: "Id_Empresa_contratista") as? Int
                        object.idEmpresaInspector = result[index].value(forKey: "Id_Empresa_Inspector") as? Int
                        object.sede = result[index].value(forKey: "Sede") as? String
                        object.empresaContratista = result[index].value(forKey: "EmpresaContratista") as? String
                        object.idProyecto = result[index].value(forKey: "idProyecto") as? Int
                        response.append(object)
                    }
                    callback(response,nil)
                }
                
                
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerProyecto(params: NSDictionary, callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerProyecto(params: params,callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "idProyecto") as? Int
                        item.nombre = result[index].value(forKey: "Nombre") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerProyecto2(params: NSDictionary, callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerProyecto(params: params,callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    let item = ItemSpinnerModel()
                    item.id = 9999
                    item.nombre = "TODO"
                    response.append(item)
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "idProyecto") as? Int
                        item.nombre = result[index].value(forKey: "Nombre") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerContratista(idProyecto: Int, callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerContratista(idProyecto:idProyecto,callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Empresa_Observadora") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerTipoUbicacion(idProyecto: Int, callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerTipoUbicacion(idProyecto:idProyecto,callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "idTipoUbicacion") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,error)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerLugar(idTipoUbicacion: Int, callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerLugar(idTipoUbicacion:idTipoUbicacion,callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "idLugar") as? Int
                        item.nombre = result[index].value(forKey: "Nombre") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerAreaInspeccion(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerAreaInspeccion(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Area_inspeccionada") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func empresaProyecto(idProyecto: Int, callback: @escaping (EmpresaProyectoModel?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).empresaProyecto(idProyecto:idProyecto,callback: {(success,error) in
            if let result = success {
                let empresa = EmpresaProyectoModel()
                empresa.idEmpresaMaestra = result.value(forKey: "Id_Empresa_Maestra") as? Int
                empresa.razonSocial = result.value(forKey: "Razon_social") as? String
                empresa.ruc = result.value(forKey: "Ruc") as? String
                empresa.descripcion = result.value(forKey: "Descripcion") as? String
                empresa.domicilioLegal = result.value(forKey: "Descripcion") as? String
                empresa.domicilioLegal = result.value(forKey: "Domicilio_Legal") as? String
                empresa.nroTrabajadores = result.value(forKey: "Nro_Trabajadores") as? Int
                empresa.nroTrabajadores = result.value(forKey: "Nro_Trabajadores") as? Int
                empresa.idActividadEconomica = result.value(forKey: "Id_Actividad_Economica") as? Int
                empresa.idParam = result.value(forKey: "idParam") as? Int
                callback(empresa,error)
            }else{
                callback(nil,error)
            }
        })
    }
    
    
    func obtenerBuenaPractica(callback: @escaping (Array<BuenaPracticaModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerBuenaPractica(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<BuenaPracticaModel> = []
                    for index in 0...result.count-1 {
                        let item = BuenaPracticaModel()
                        item.idCategoriaBuenaPractica = result[index].value(forKey: "idCategoriaBuenaPractica") as? Int
                        item.nombre = result[index].value(forKey: "Nombre") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func insertarInspeccion(params: NSDictionary, callback: @escaping (CreateInspeccionResponse?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).crearInspeccion(params: params, callback: {(success,error) in
            if let result = success {
                let object = CreateInspeccionResponse()
                object.idInspecciones = result.value(forKey: "Id_Inspecciones") as? Int
                object.codigoInspecion = result.value(forKey: "codigoInspecion") as? String
                object.proyecto = result.value(forKey: "proyecto") as? String
                callback(object,nil)
            }else{
                callback(nil,error)
            }
        })
    }
    
    func insertarBuenaPractica(params: Array<NSDictionary>, callback: @escaping (Bool?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).crearBuenaPractica(params: params, callback: {(success,error) in
            if let result = success {
                callback(result,nil)
            }else{
                callback(false,error)
            }
        })
    }
    
    
    
    func detalleInspeccion(idInspeccion: Int, callback: @escaping (DetalleInspeccionModel?,Array<ItemHallazgoModel>?,Array<BuenaPracticaModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).detalleInspeccion(idInspeccion: idInspeccion,callback: {(success,error) in
            if let result = success {
                
                let detalle = DetalleInspeccionModel()
                detalle.idInspecciones = result.value(forKey: "Id_Inspecciones") as? Int
                detalle.idEmpresaContratista = result.value(forKey: "Id_Empresa_contratista") as? Int
                detalle.codigoInspeccion = result.value(forKey: "CodigoInspeccion") as? String
                detalle.idTipoUbicacion = result.value(forKey: "idTipoUbicacion") as? Int
                detalle.idProyecto = result.value(forKey: "idProyecto") as? Int
                detalle.idLugar = result.value(forKey: "idLugar") as? Int
                detalle.idUsuario = result.value(forKey: "Id_Usuario") as? Int
                detalle.idEmpresaObservadora = result.value(forKey: "Id_Empresa_Observadora") as? Int
                detalle.responsableAreaInspeccion = result.value(forKey: "Responsable_Area_Inspeccion") as? String
                detalle.torre = result.value(forKey: "Torre") as? String
                detalle.areaInspecionada = result.value(forKey: "Area_Inspeccionada") as? String
                detalle.tipoInspeccion = result.value(forKey: "Tipo_inspeccion") as? Int
                detalle.nombreResponsableInspeccion = result.value(forKey: "Responsable_Inspeccion") as? String
                detalle.usuarioRolInspecion = result.value(forKey: "Id_Usuario_Registro") as? Int
                
                var practica:Array<BuenaPracticaModel> = []
                if let data = result.value(forKey: "lstBuenasPracticas") as? Array<NSDictionary> {
                    if(data.count>=1){
                        for index in 0...data.count-1 {
                            let object = BuenaPracticaModel()
                            object.idBuenasPracticas = data[index].value(forKey: "idBuenasPracticas") as? Int
                            object.idCategoriaBuenaPractica = data[index].value(forKey: "idCategoriaBuenaPractica") as? Int
                            object.descripcion = data[index].value(forKey: "Descripcion") as? String
                            object.imgBase64 = data[index].value(forKey: "urlImagen") as? String
                            object.nombre = data[index].value(forKey: "categoriaBuenaPractica") as? String
                            practica.append(object)
                        }
                    }
                }
                
                var response:Array<ItemHallazgoModel> = []
                if let data = result.value(forKey: "lstDetalleInspeccion") as? Array<NSDictionary> {
                    if(data.count>=1){
                        for index in 0...data.count-1 {
                            let object = ItemHallazgoModel()
                            object.id = data[index].value(forKey: "Id_Inspecciones_reporte_detalle") as? Int
                            object.actoSubestandar = data[index].value(forKey: "Acto_Subestandar") as? String
                            
                            if(data[index].value(forKey: "Riesgo_A") as? Bool ?? false){
                                object.riesgo = 3
                            }else if(data[index].value(forKey: "Riesgo_M") as? Bool ?? false){
                                object.riesgo = 2
                            }else if(data[index].value(forKey: "Riesgo_B") as? Bool ?? false){
                                object.riesgo = 1
                            }
                            
                            object.plazo = data[index].value(forKey: "PlazoString") as? String
                            object.idSubFamiliaAmbiental = data[index].value(forKey: "id_SubFamiliaAmbiental") as? Int
                            object.idActoSubestandar = data[index].value(forKey: "id_ActoSubestandar") as? Int
                            object.idTipoGestion = data[index].value(forKey: "idTipoGestion") as? Int
                            object.idCondicionSubestandar = data[index].value(forKey: "id_CondicionSubestandar") as? Int
                            object.resposableAreadetalle = data[index].value(forKey: "Resposable_Area_detalle") as? String
                            object.nombreUsuario = detalle.responsableAreaInspeccion
                            object.idHallazgo = data[index].value(forKey: "idHallazgo") as? Int
                            object.nombreTipoGestion = data[index].value(forKey: "TipoGestion") as? String
                            object.nombreHallazgo = data[index].value(forKey: "Hallazgo") as? String
                    
                            object.nombreActoSubEstandar = data[index].value(forKey: "Descripcion_ActoSubestandar") as? String
                            object.estadoDetalleInspeccion = data[index].value(forKey: "Estado_CierreInspeccion") as? Bool
                            object.accionMitigadora = data[index].value(forKey: "Accion_Mitigadora") as? String
                            object.usuarioRol = data[index].value(forKey: "Id_Rol_Usuario") as? Int
                            
                            if let evidenciaFoto = data[index].value(forKey: "LstEvidencia_Fotografica_Observaciones") as? Array<NSDictionary> {
                                if(evidenciaFoto.count>=1){
                                    for index in 0...evidenciaFoto.count-1 {
                                        object.imgFotoHallazgo = evidenciaFoto[index].value(forKey: "urlImagen") as? String
                                        object.fechaFoto = evidenciaFoto[index].value(forKey: "fechaFoto") as? String
                                    }
                                }
                            }
                            
                            if let evidenciaCierre = data[index].value(forKey: "LstEvidencia_Cierre") as? Array<NSDictionary> {
                                if(evidenciaCierre.count>=1){
                                    for index in 0...evidenciaCierre.count-1 {
                                        object.imgCierre = evidenciaCierre[index].value(forKey: "urlImagen") as? String
                                        object.fechaImgCierre = evidenciaCierre[index].value(forKey: "fechaFoto") as? String
                                    }
                                }
                            }
                            
                            response.append(object)
                        }
                        callback(detalle,response,practica,error)
                    }else{
                        callback(detalle,response,practica,error)
                    }
                }else{
                    callback(detalle,response,practica,error)
                }
                
            }else{
                callback(nil,nil,nil,error)
            }
        })
    }
    
    func crearHallazgo(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).crearHallazgo(params: params,  callback: {(success,error) in
            if let result = success {
                if(result){
                    callback(true,nil)
                }else{
                    callback(false,nil)
                }
            }else{
                callback(false,error)
            }
        })
    }
    
    
    func obtenerUsuarioNotificar(callback: @escaping (Bool?,Array<NotificarModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerUsuarioNotificar(callback: {(success,error)in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<NotificarModel> = []
                    for index in 0...result.count-1 {
                        let object = NotificarModel()
                        object.idUsers = result[index].value(forKey: "IdUsers") as? Int
                        object.nombreUsuario = result[index].value(forKey: "Nombre_Usuario") as? String
                        object.emailCorporativo = result[index].value(forKey: "Email_Corporativo") as? String
                        object.selectEmail = false
                        response.append(object)
                    }
                    callback(true,response,nil)
                }else{
                    callback(false,nil,nil)
                }
            }else{
                callback(false,nil,nil)
            }
        })
    }
    
    func obtenerUsuarios(params: NSDictionary ,callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).obtenerUsuariosV2(params: params, callback: {(success,error)in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let object = ItemSpinnerModel()
                        object.id = result[index].value(forKey: "IdPerfil_Usuario") as? Int
                        object.nombre = result[index].value(forKey: "Nombre_Usuario") as? String
                        object.descripcion = result[index].value(forKey: "Apellido_Usuario") as? String
                        response.append(object)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func eliminarInspeccion(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).eliminarInspeccion(params: params,  callback: {(success,error) in
            if let result = success {
                if(result){
                    callback(true,nil)
                }else{
                    callback(false,nil)
                }
            }else{
                callback(false,error)
            }
        })
    }
    
    func enviarNotificacion(params: NSDictionary,notificar: Array<NSDictionary>, callback: @escaping (String?, NSError?) -> ()){
        InspeccionNetWork.getInstance(parent: self.parent!).enviarNotificacion(params: params,notificar: notificar,callback: {(success,error) in
            if let result = success {
                callback(result,error)
            }else{
                callback(nil,error)
            }
        })
    }
    
}
