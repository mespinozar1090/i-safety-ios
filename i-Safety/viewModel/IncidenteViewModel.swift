//
//  IncidenteViewModel.swift
//  i-Safety
//
//  Created by usuario on 9/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import UIKit
class IncidenteViewModel {
    var parent: UIViewController? = nil
    
    init() {
    }
    
    init(parent: UIViewController) {
        self.parent = parent
    }
    
    func listarIncidente(params: NSDictionary,callback: @escaping (Array<IncidenteModel>?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).listarIncidente(params:params,callback: {(success,error) in
            if let data = success {
                if let result = data.value(forKey: "Data") as? Array<NSDictionary> {
                    if(result.count > 0) {
                        var response:Array<IncidenteModel> = []
                        for index in 0...result.count-1 {
                            let item = IncidenteModel()
                            item.idIncidente = result[index].value(forKey: "Id_Incidente") as? Int
                            item.gFechaCreado = result[index].value(forKey: "G_Fecha_Creado") as? String
                            item.gDescripcionFormato = result[index].value(forKey: "G_DescripcionFormato") as? String
                            item.tipoInforme = result[index].value(forKey: "Tipo_InformeP_F") as? String
                            response.append(item)
                        }
                        callback(response,nil)
                    }else{
                        callback(nil,nil)
                    }
                }
                
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerEmpresa(callback: @escaping (Array<EmpresaModel>?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).obtenerEmpresa(callback:{(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<EmpresaModel> = []
                    for index in 0...result.count-1 {
                        let item = EmpresaModel()
                        item.idEmpresaMaestra = result[index].value(forKey: "Id_Empresa_Maestra") as? Int
                        item.razonsocial = result[index].value(forKey: "Razon_social") as? String
                        item.ruc = result[index].value(forKey: "Ruc") as? Int
                        item.domicilioLegal = result[index].value(forKey: "Domicilio_Legal") as? String
                        item.nroTrabajadores = result[index].value(forKey: "Nro_Trabajadores") as? Int
                        item.idActividadEconomica = result[index].value(forKey: "Id_Actividad_Economica") as? Int
                        item.idTamanioEmpresa = result[index].value(forKey: "Id_Tamanio_Empresa") as? Int
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,error)
            }
        })
    }
    
    func obtenerComboMestro(parametro:String,callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).obtenerComboMestro(parametro: parametro, callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Elemento") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion_Elemento") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func obtenerComboMaestroGenerico(nombreTable:String,callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).obtenerComboMaestroGenerico(nombreTable: nombreTable, callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Elemento") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion_Elemento") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func obtenerActividad(callback: @escaping (Array<ItemSpinnerModel>?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).obtenerActividad(callback: {(success,error) in
            if let result = success {
                if(result.count > 0) {
                    var response:Array<ItemSpinnerModel> = []
                    for index in 0...result.count-1 {
                        let item = ItemSpinnerModel()
                        item.id = result[index].value(forKey: "Id_Actividad_Economica") as? Int
                        item.nombre = result[index].value(forKey: "Descripcion") as? String
                        response.append(item)
                    }
                    callback(response,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func crearRegistroIncidente(params: NSDictionary,notificar: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).crearRegistroIncidente(params: params, notificar: notificar, callback: {(success,error)in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                    callback(estatus,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func editarRegistroIncidente(params: NSDictionary,notificar: Array<NSDictionary>,callback: @escaping (Int?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).editarRegistroIncidente(params: params, notificar: notificar, callback: {(success,error)in
            if let result = success {
                if let estatus = result.value(forKey: "Estatus") as? Int{
                    callback(estatus,nil)
                }else{
                    callback(nil,nil)
                }
            }else{
                callback(nil,nil)
            }
        })
    }
    
    func verRegistroIncidente(params: NSDictionary,callback: @escaping (GeneralIncidenteModel?,EventoIncidenteModel?, NSError?) -> ()){
        IncidenteNetWork.getInstance(parent: self.parent!).verRegistroIncidente(params: params,callback: {(success,error)in
            if let result = success {
                let general = GeneralIncidenteModel()
                general.idIncidente = result.value(forKey:"Id_Incidente") as? Int
                general.tipoInforme = result.value(forKey:"Tipo_InformeP_F") as? String
                general.gDescripcionFormato = result.value(forKey:"G_DescripcionFormato") as? String
                general.empleadorTipo = result.value(forKey:"Empleador_Tipo") as? String
                general.fEmpleadorRuc = result.value(forKey:"F_Empleador_RUC") as? String
                general.fTrabajadorNombresApellidos = result.value(forKey:"F_Trabajador_NombresApellidos") as? String
                if let gIdUsuario = result.value(forKey: "G_IdUsuario_Creado") as? NSDictionary{
                    general.idUsuarioCreador = gIdUsuario.value(forKey:"Id_Elemento") as? Int
                    
                }
                if let fEmpleadorId = result.value(forKey: "F_Empleador_IdEmpresa") as? NSDictionary{
                    general.fEmpleadorIdEmpresa = fEmpleadorId.value(forKey:"Id_Elemento") as? Int
                }
                
                general.fEmpleadorRazonSocial = result.value(forKey:"F_Empleador_RazonSocial") as? String
                
                if let fActividadEco = result.value(forKey: "F_Empleador_IdActividadEconomica") as? NSDictionary{
                    general.fEmpleadorIdActividadEcomica = fActividadEco.value(forKey:"Id_Elemento") as? Int
                    general.nombreActividadEconomica = fActividadEco.value(forKey:"Descripcion_Elemento") as? String
                }
                
                if let fIdTaminioEmpresa = result.value(forKey: "F_Empleador_IdTamanioEmpresa") as? NSDictionary{
                    
                    general.fEmpleadorIdTamanioEmpresa = fIdTaminioEmpresa.value(forKey:"Id_Elemento") as? Int
                    general.fNombreTamnioEmpresa = fIdTaminioEmpresa.value(forKey:"Descripcion_Elemento") as? String
                }
                
                general.fEmpleadorInterRazonSocial = result.value(forKey:"F_EmpleadorI_RazonSocial") as? String
                general.fDescripcionIncidente = result.value(forKey:"F_DESCRIPCION_Incidente") as? String
                general.fEmpleadorInterRuc = result.value(forKey:"F_EmpleadorI_RUC") as? String
                
                
                if let interIdActividadEco = result.value(forKey: "F_EmpleadorI_IdActividadEconomica") as? NSDictionary{
                    general.fEmpleadorInterIdActividadEconomica = interIdActividadEco.value(forKey:"Id_Elemento") as? Int
                    general.interNombreActividadEco = interIdActividadEco.value(forKey:"Descripcion_Elemento") as? String
                }
                
                let evento = EventoIncidenteModel()
                if let gSede = result.value(forKey: "G_IdProyecto_Sede") as? NSDictionary{
                    evento.idSedeProyecto = gSede.value(forKey:"Id_Elemento") as? Int
                    evento.nombreSedeProyecto = gSede.value(forKey:"Descripcion_Elemento") as? String
                }
                
                //Evento
                if let tipoEvento = result.value(forKey: "F_EVENTO_IdTipoEvento") as? NSDictionary{
                    evento.idTipoEvento = tipoEvento.value(forKey:"Id_Elemento") as? Int
                    evento.nombreTipoEvento = tipoEvento.value(forKey:"Descripcion_Elemento") as? String
                }
                evento.huboDanioMaterial = result.value(forKey:"F_EVENTO_HuboDanioMaterial") as? String
                evento.numeroTrabajadoresAfectados = result.value(forKey:"F_EVENTO_NumTrabajadoresAfectadas") as? String
                
                evento.fechaAccidente = result.value(forKey:"F_EVENTO_FechaAccidente") as? String
                evento.horaAccidente = result.value(forKey:"F_EVENTO_HoraAccidente") as? String
                evento.fechaInicioInvestigacion = result.value(forKey:"F_EVENTO_FechaInicioInvestigacion") as? String
                evento.lugarExacto = result.value(forKey:"F_EVENTO_LugarExacto") as? String
                
                if let parteAfectada = result.value(forKey: "F_EVENTO_IdParteAfectada") as? NSDictionary{
                    evento.idParteAfectada = parteAfectada.value(forKey:"Id_Elemento") as? Int
                    evento.nombreParteAfectada = parteAfectada.value(forKey:"Descripcion_Elemento") as? String
                }
                
                if let equipoAfectado = result.value(forKey: "F_EVENTO_IdEquipoAfectado") as? NSDictionary{
                    evento.idEquipoAfectado = equipoAfectado.value(forKey:"Id_Elemento") as? Int
                    evento.nombreEquipoAfectado = equipoAfectado.value(forKey:"Descripcion_Elemento") as? String
                }
                callback(general,evento,nil)
                
            }else{
                callback(nil,nil,nil)
            }
        })
    }
}



