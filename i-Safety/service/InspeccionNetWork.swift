//
//  InspeccionNetWork.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class InspeccionNetWork {
    
    static let instance = InspeccionNetWork()
    static var parent: UIViewController? = nil
    
    init() {
    }
    
    static func getInstance(parent: UIViewController) -> InspeccionNetWork {
        if(self.parent == nil) {
            self.parent = parent
        }
        return self.instance
    }
    //Id_Usuario//Id_Rol_Usuario//Id_Rol_General
    func listaInpecciones(params: NSDictionary, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/ListarInspeccionFiltrosBandeja",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func listaInpeccionesV2(params: NSDictionary, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        let parameters: [String: Int] = [
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/ListarInspeccionFiltrosBandeja",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func eliminarInspeccion(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()) {
        let parameters: [String: Int] = [
            "idInspecion": params.value(forKey: "idInspecion")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/borrarInspecion",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Bool, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerProyecto(params: NSDictionary, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/obtenerProyectos",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerContratista(idProyecto: Int, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "idParam": idProyecto]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/obtenerEmpresa_Proyecto",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerTipoUbicacion(idProyecto: Int, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "idProyecto": idProyecto]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/obtenerTipoUbicacion",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerLugar(idTipoUbicacion: Int, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "idTipoUbicacion": idTipoUbicacion]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/obtenerLugar",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerAreaInspeccion(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/ListarAreaInspeccionada",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func empresaProyecto(idProyecto: Int, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "idParam": idProyecto]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/obtenerEmpresaMaestra_Proyecto",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func detalleInspeccion(idInspeccion: Int, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        let parameters: [String: Int] = [
            "Id_Inspecciones": idInspeccion]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/ListarInspeccionDetalle",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerBuenaPractica(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/CategoriaBuenaPractica",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func crearInspeccion(params: NSDictionary, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let parameters: [String: Any] = [
            "Id_Inspecciones":params.value(forKey: "Id_Inspecciones")! as! Int,
            "idProyecto": params.value(forKey: "idProyecto")! as! Int,
            "Id_Empresa_contratista": params.value(forKey: "Id_Empresa_contratista")! as! Int,
            "idTipoUbicacion": params.value(forKey: "idTipoUbicacion")! as! Int,
            "idLugar": params.value(forKey: "idLugar")! as! Int,
            "Torre": params.value(forKey: "Torre")! as! String,
            "Id_Empresa_Observadora": params.value(forKey: "Id_Empresa_Observadora")! as! Int,
            "RUC": params.value(forKey: "RUC")! as! String,
            "Id_Actividad_Economica": params.value(forKey: "Id_Actividad_Economica")! as! Int,
            "Domicilio_Legal": params.value(forKey: "Domicilio_Legal")! as! String,
            "Nro_Trabajadores": params.value(forKey: "Nro_Trabajadores")! as! Int,
            "Tipo_inspeccion": params.value(forKey: "Tipo_inspeccion")! as! Int,
            "Fecha_Hora_Inspeccion": params.value(forKey: "Fecha_Hora_Inspeccion")! as! String,
            "Responsable_Inspeccion": params.value(forKey: "Responsable_Inspeccion")! as! String,
            "Area_Inspeccionada": params.value(forKey: "Area_Inspeccionada")! as! String,
            "Objectivo_Inspeccion_Interna": params.value(forKey: "Objectivo_Inspeccion_Interna")! as! String,
            "Resultado_Inspeccion": params.value(forKey: "Resultado_Inspeccion")! as! String,
            "Conclusiones_Recomendaciones": params.value(forKey: "Conclusiones_Recomendaciones")! as! String,
            "Origen_Registro": params.value(forKey: "Origen_Registro")! as! String,
            //  "UsuariosNotificados": notificadores,
            "Estado_Inspeccion": params.value(forKey: "Estado_Inspeccion")! as! Int,
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Responsable_Area_Inspeccion": params.value(forKey: "Responsable_Area_Inspeccion")! as! String,
            "InspeccionInterna": params.value(forKey: "InspeccionInterna")! as! Int,
            "nombreFormato": params.value(forKey: "nombreFormato")! as! String,
            "Id_Usuario_Registro": params.value(forKey: "Id_Usuario_Registro")! as! Int,
        ]
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/RegistroInspeccion",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("NSDictionary: \(response.result.value as? NSDictionary)")
           
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func crearBuenaPractica(params: Array<NSDictionary>, callback: @escaping (Bool?, NSError?) -> ()) {
        var parameterse: [[String: Any]] = []
        print("Entro a buena practica ")
        for index in 0...params.count-1 {
            let parameters: [String: Any] = [
                "idProyecto": params[index].value(forKey: "idProyecto") as! Int,
                "Id_Inspecciones": params[index].value(forKey: "Id_Inspecciones")! as! Int,
                "idCategoriaBuenaPractica": params[index].value(forKey: "idCategoriaBuenaPractica")! as! Int,
                "Id_Empresa_contratista": params[index].value(forKey: "Id_Empresa_contratista")! as! Int,
                "Descripcion": params[index].value(forKey: "Descripcion")! as! String,
                "nombreImg": params[index].value(forKey: "nombreImg")! as! String,
                "fechaImg": params[index].value(forKey: "fechaImg")! as! String,
                "ImgB64": params[index].value(forKey: "ImgB64")! as! String]
            parameterse.append(parameters)
        }
        print("parametro BP \(parameterse)")
        
        var request = URLRequest(url: NSURL(string: Constante.BASE_URL+"api/Inspeccion/insertarBuenaPractica")! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = 60 // 10 secs
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameterse, options: [[]])
        
        Alamofire.request(request as! URLRequestConvertible).responseJSON {
            response in
            print("erro buena practica : \(response.result.value as? Bool)")
            switch response.result {
            case .success:
                
                callback(response.result.value as? Bool, nil)
                break
            case .failure(let error):
                print("erro buena practica : \(error)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func crearHallazgo(params: NSDictionary, callback: @escaping (Bool?, NSError?) -> ()) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        var imgArray: [[String: Any]] = []
        var imgMitigadoraArray: [[String: Any]] = []
        
        let evidencia:String = params.value(forKey: "EvidenciaFotograficaObservacion") as! String
        if(evidencia != ""){
            let img: [String: Any] = [
                "imgB64": params.value(forKey: "EvidenciaFotograficaObservacion") as! String,
                "fecha": params.value(forKey: "fechaImgHallazgo") as! String]
            imgArray.append(img)
        }
        
        let evidenciaMitigadora:String = params.value(forKey: "Evidencia_Cierre") as! String
        if(evidenciaMitigadora != ""){
            let img: [String: Any] = [
                "imgB64": params.value(forKey: "Evidencia_Cierre") as! String,
                "fecha": params.value(forKey: "fechaImgMitigadora") as! String,]
            imgMitigadoraArray.append(img)
        }
        
        
        let parameters: [String: Any] = [
            "Id_Inspecciones_reporte_detalle":params.value(forKey: "Id_Inspecciones_reporte_detalle")! as! Int,
            "Id_Inspeccion": params.value(forKey: "Id_Inspeccion")! as! Int,
            "idTipoGestion": params.value(forKey: "idTipoGestion")! as! Int,
            "id_ActoSubestandar": params.value(forKey: "id_ActoSubestandar")! as! Int,
            "id_CondicionSubestandar": params.value(forKey: "id_CondicionSubestandar")! as! Int,
            "Acto_Subestandar": params.value(forKey: "Acto_Subestandar")! as! String,
            "Riesgo_A": params.value(forKey: "Riesgo_A")! as! Int,
            "Riesgo_M": params.value(forKey: "Riesgo_M")! as! Int,
            "Riesgo_B": params.value(forKey: "Riesgo_B")! as! Int,
            "idHallazgo": params.value(forKey: "idHallazgo")! as! Int,
            "id_NoConformidad": params.value(forKey: "id_NoConformidad")! as! Int,
            "id_SubFamiliaAmbiental": params.value(forKey: "id_SubFamiliaAmbiental")! as! Int,
            "Resposable_Area_detalle": params.value(forKey: "Resposable_Area_detalle")! as! String,
            "EvidenciaFotograficaObservacion": imgArray as Array<Any>,
            "Accion_Mitigadora": params.value(forKey: "Accion_Mitigadora")! as! String,
            "PlazoString": params.value(forKey: "PlazoString")! as! String,
            "Origen_Registro": "App",
            "Id_Usuario_Registro": params.value(forKey: "Id_Usuario_Registro")! as! Int,
            "Estado_DetalleInspeccion": params.value(forKey: "Estado_DetalleInspeccion")! as! Int,
            "Evidencia_Cierre": imgMitigadoraArray
        ]
        print("parameters:  \(parameters)")
        
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/insertarDetalleInscripcion",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                print("success:")
                callback(response.result.value as? Bool, nil)
                break
            case .failure(let error):
                print("error:  \(error)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func obtenerUsuarioNotificar(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Administracion/ListarUsuarioRol",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerUsuarios(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Usuario/obtenerTodosUsuarios",method: .post,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerUsuariosV2(params: NSDictionary, callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/Usuario/obtenerTodosUsuariosEmpresa",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func enviarNotificacion(params: NSDictionary,notificar: Array<NSDictionary>, callback: @escaping (String?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameters: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameters)
        }
        
        let parameters: [String: Any] = [
            "UsuariosNotificados": notificadores,
            "nombreproyecto": params.value(forKey: "nombreproyecto")! as! String,
            "codigoInspeccion": params.value(forKey: "codigoInspeccion")! as! String,
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Origen_Registro": "App",
            "Id_Inspecciones": params.value(forKey: "Id_Inspecciones")! as! Int]
      
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/NotificacionInspeccion",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? String, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
}
