//
//  InterventoriaNetWork.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class InterventoriaNetWork {
    static let instance = InterventoriaNetWork()
    static var parent: UIViewController? = nil
    
    init() {
    }
    
    static func getInstance(parent: UIViewController) -> InterventoriaNetWork {
        if(self.parent == nil) {
            self.parent = parent
        }
        return self.instance
    }
    
    func listarInterventoria(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/ListarRegistros",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    func listarInterventoriaV2(params: NSDictionary, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        let parameters: [String: Int] = [
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/FiltrosBandeja",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            print(" listarInterventoriaV2 : \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerSubEstacion(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Maestro/ListaLineaSubestacion",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerVerificacionSubItem(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/ListaVerificacionesSubitems",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerControlSubItem(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/ListaControlItems",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func crearRegistroInterventoria(params: NSDictionary,notificar: Array<NSDictionary>,verificaciones: Array<NSDictionary>,controlInterventoria: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        var verificacion: [[String: Any]] = []
        for index in 0...verificaciones.count-1 {
            let verificacionParameter: [String: Any] = [
                "Id_Interventoria_Detalle": verificaciones[index].value(forKey: "Id_Interventoria_Detalle")! as! Int,
                "Id_Verificacion_Subitem": verificaciones[index].value(forKey: "Id_Verificacion_Subitem")! as! Int,
                "Id_Verificacion": verificaciones[index].value(forKey: "Id_Verificacion")! as! Int,
                "Cumple": verificaciones[index].value(forKey: "Cumple")! as! Int,
                "Id_Interventoria": verificaciones[index].value(forKey: "Id_Interventoria")! as! Int]
            verificacion.append(verificacionParameter)
        }
        
        var controlPersonal: [[String: Any]] = []
        for index in 0...controlInterventoria.count-1 {
            let controlParameter: [String: Any] = [
                "Id_Control_Detalle": controlInterventoria[index].value(forKey: "Id_Control_Detalle")! as! Int,
                "Id_Control_item": controlInterventoria[index].value(forKey: "Id_Control_item")! as! Int,
                "Id_Control": controlInterventoria[index].value(forKey: "Id_Control")! as! Int,
                "Cantidad": controlInterventoria[index].value(forKey: "Cantidad")! as! Int]
            controlPersonal.append(controlParameter)
        }
        
        let parameters: [String: Any] = [
            "Id_Interventoria": params.value(forKey: "Id_Interventoria")! as! Int,
            "Actividad": params.value(forKey: "Actividad")! as! String,
            "Conclucion": params.value(forKey: "Conclucion")! as! String,
            "Empresa_Intervenida": params.value(forKey: "Empresa_Intervenida")! as! Int,
            "Empresa_Inteventor": params.value(forKey: "Empresa_Inteventor")! as! Int,
            "Estado": params.value(forKey: "Estado")! as! Bool,
            "Fecha_Hora": params.value(forKey: "Fecha_Hora")! as! String,
            "Fecha_Registro": params.value(forKey: "Fecha_Registro")! as! String,
            "Id_Plan_Trabajo": params.value(forKey: "Id_Plan_Trabajo")! as! Int,
            "PlanTrabajo": params.value(forKey: "PlanTrabajo")! as! Int,
            "IdUsuario": params.value(forKey: "IdUsuario")! as! Int,
            "Interventor": params.value(forKey: "Interventor")! as! String,
            "Linea_Subestacion": params.value(forKey: "Linea_Subestacion")! as! Int,
            "LstControles": controlPersonal,
            "LstDetalle": verificacion,
            "Lugar_Zona": params.value(forKey: "Lugar_Zona")! as! Int,
            "Nro_Plan_Trabajo": params.value(forKey: "Nro_Plan_Trabajo")! as! String,
            "Supervisor": params.value(forKey: "Supervisor")! as! String,
            "Supervisor_Sustituto": params.value(forKey: "Supervisor_Sustituto")! as! String,
            "Tipo_Plan": 1,
            "Tipo_Registro": 2,
            "OrigenRegistro": "App",
            "UsuariosNotificados": notificadores,
            "NombreFormato": params.value(forKey: "NombreFormato")! as! String]
        
        print("parametros : \(parameters) ")
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/Registro",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
           print("result: \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    
    func obtenerRegistroInterventoria(idInterventoria: Int, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/GetDetalle?Id=\(idInterventoria)",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("result : \(response.result.value as? NSDictionary) ")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func editarRegistroInterventoria(params: NSDictionary,notificar: Array<NSDictionary>,verificaciones: Array<NSDictionary>,controlInterventoria: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        var verificacion: [[String: Any]] = []
        for index in 0...verificaciones.count-1 {
            let verificacionParameter: [String: Any] = [
                "Id_Interventoria_Detalle": verificaciones[index].value(forKey: "Id_Interventoria_Detalle")! as! Int,
                "Id_Verificacion_Subitem": verificaciones[index].value(forKey: "Id_Verificacion_Subitem")! as! Int,
                "Id_Verificacion": verificaciones[index].value(forKey: "Id_Verificacion")! as! Int,
                "Cumple": verificaciones[index].value(forKey: "Cumple")! as! Int,
                "Id_Interventoria": verificaciones[index].value(forKey: "Id_Interventoria")! as! Int]
            verificacion.append(verificacionParameter)
        }
        
        var controlPersonal: [[String: Any]] = []
        for index in 0...controlInterventoria.count-1 {
            let controlParameter: [String: Any] = [
                "Id_Control_Detalle": controlInterventoria[index].value(forKey: "Id_Control_Detalle")! as! Int,
                "Id_Control_item": controlInterventoria[index].value(forKey: "Id_Control_item")! as! Int,
                "Id_Control": controlInterventoria[index].value(forKey: "Id_Control")! as! Int,
                "Cantidad": controlInterventoria[index].value(forKey: "Cantidad")! as! Int]
            controlPersonal.append(controlParameter)
        }
        
        let parameters: [String: Any] = [
            "Id_Interventoria": params.value(forKey: "Id_Interventoria")! as! Int,
            "Actividad": params.value(forKey: "Actividad")! as! String,
            "Conclucion": params.value(forKey: "Conclucion")! as! String,
            "Empresa_Intervenida": params.value(forKey: "Empresa_Intervenida")! as! Int,
            "Empresa_Inteventor": params.value(forKey: "Empresa_Inteventor")! as! Int,
            "Estado": params.value(forKey: "Estado")! as! Bool,
            "Fecha_Hora": params.value(forKey: "Fecha_Hora")! as! String,
            "Fecha_Registro": params.value(forKey: "Fecha_Registro")! as! String,
            "Id_Plan_Trabajo": params.value(forKey: "Id_Plan_Trabajo")! as! Int,
            "PlanTrabajo": params.value(forKey: "PlanTrabajo")! as! Int,
            "IdUsuario": params.value(forKey: "IdUsuario")! as! Int,
            "Interventor": params.value(forKey: "Interventor")! as! String,
            "Linea_Subestacion": params.value(forKey: "Linea_Subestacion")! as! Int,
            "LstControles": controlPersonal,
            "LstDetalle": verificacion,
            "Lugar_Zona": params.value(forKey: "Lugar_Zona")! as! Int,
            "Nro_Plan_Trabajo": params.value(forKey: "Nro_Plan_Trabajo")! as! String,
            "Supervisor": params.value(forKey: "Supervisor")! as! String,
            "Supervisor_Sustituto": params.value(forKey: "Supervisor_Sustituto")! as! String,
            "Tipo_Plan": 1,
            "Tipo_Registro": 2,
            "OrigenRegistro": "App",
            "UsuariosNotificados": notificadores,
            "NombreFormato": params.value(forKey: "NombreFormato")! as! String]
        
        Alamofire.request(Constante.BASE_URL+"api/Interventoria/Editar",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("result data editar : \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func eliminarInterventoria(params: NSDictionary,callback: @escaping (Bool?, NSError?) -> ()) {
          let parameters: [String: Int] = [
              "Id_Interventoria": params.value(forKey: "Id_Interventoria")! as! Int]
          
          Alamofire.request(Constante.BASE_URL+"api/Interventoria/borrarInterventoria",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
              print("result: \(response.result.value as? Bool)")
              switch response.result {
              case .success:
                  callback(response.result.value as? Bool, nil)
                  break
              case .failure(let error):
                  print(error)
                  callback(nil, error as NSError?)
                  break
              }
          }
      }
    
}
