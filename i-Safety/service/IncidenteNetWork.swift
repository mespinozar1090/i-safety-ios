//
//  IncidenteNetWork.swift
//  i-Safety
//
//  Created by usuario on 9/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class IncidenteNetWork{
    
    static let instance = IncidenteNetWork()
    static var parent: UIViewController? = nil
    
    init() {
    }
    
    static func getInstance(parent: UIViewController) -> IncidenteNetWork {
        if(self.parent == nil) {
            self.parent = parent
        }
        return self.instance
    }
    
    
    func listarIncidente(params: NSDictionary,callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let parameters: [String: Any] = [
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int,
            "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Tipo_Formato": params.value(forKey: "Tipo_Formato")! as! String]
        
        Alamofire.request(Constante.BASE_URL+"/api/Incidente/FiltrosBandeja",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func obtenerEmpresa(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/Empresa",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerComboMaestroGenerico(nombreTable:String,callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        let parameters: [String: Any] = [
            "Tabla": nombreTable,
            "Estado": "1"]
        
        Alamofire.request(Constante.BASE_URL+"/api/Incidente/GetCargarComboMestroGenerico",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerComboMestro(parametro:String,callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        let parameters: [String: Any] = [
            "TipoElemento": parametro,
            "Estado": "1"]
        
        Alamofire.request(Constante.BASE_URL+"/api/Incidente/GetCargarComboMestro",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerActividad(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/Actividad",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func crearRegistroIncidente(params: NSDictionary,notificar: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        let parameters: [String: Any] = [
            "Id_Incidente":params.value(forKey: "Id_Incidente")! as! Int,
            "Empleador_Tipo": params.value(forKey: "Empleador_Tipo")! as! String,
            "F_DESCRIPCION_Incidente": params.value(forKey: "F_DESCRIPCION_Incidente")! as! String,
            "F_Empleador_IdActividadEconomica": params.value(forKey: "F_Empleador_IdActividadEconomica")! as! Int,
            "F_Empleador_IdEmpresa": params.value(forKey: "F_Empleador_IdEmpresa")! as! Int,
            "F_Empleador_IdTamanioEmpresa": params.value(forKey: "F_Empleador_IdTamanioEmpresa")! as! Int,
            "F_EmpleadorI_IdActividadEconomica": params.value(forKey: "F_EmpleadorI_IdActividadEconomica")! as! Int,
            "F_EmpleadorI_RazonSocial": params.value(forKey: "F_EmpleadorI_RazonSocial")! as! String,
            "F_EmpleadorI_RUC": params.value(forKey: "F_EmpleadorI_RUC")! as! String,
            "F_Empleador_RazonSocial": params.value(forKey: "F_Empleador_RazonSocial")! as! String,
            "F_Empleador_RUC": params.value(forKey: "F_Empleador_RUC")! as! String,
            "F_EVENTO_FechaAccidente": params.value(forKey: "F_EVENTO_FechaAccidente")! as! String,
            "F_EVENTO_FechaInicioInvestigacion": params.value(forKey: "F_EVENTO_FechaInicioInvestigacion")! as! String,
            "F_EVENTO_HoraAccidente": params.value(forKey: "F_EVENTO_HoraAccidente")! as! String,
            "F_EVENTO_HuboDanioMaterial": params.value(forKey: "F_EVENTO_HuboDanioMaterial")! as! String,
            "F_EVENTO_IdEquipoAfectado": params.value(forKey: "F_EVENTO_IdEquipoAfectado")! as! Int,
            "F_EVENTO_IdParteAfectada": params.value(forKey: "F_EVENTO_IdParteAfectada")! as! Int,
            "F_EVENTO_LugarExacto":  params.value(forKey: "F_EVENTO_LugarExacto")! as! String,
            "F_EVENTO_NumTrabajadoresAfectadas":params.value(forKey: "F_EVENTO_NumTrabajadoresAfectadas")! as! String,
            "F_Trabajador_DetalleCategoriaOcupacional":  params.value(forKey: "F_Trabajador_DetalleCategoriaOcupacional")! as! String,
            "F_Trabajador_NombresApellidos":  params.value(forKey: "F_Trabajador_NombresApellidos")! as! String,
            "G_Fecha_Creado":  params.value(forKey: "G_Fecha_Creado")! as! String,
            "G_Fecha_Modifica":  params.value(forKey: "G_Fecha_Modifica")! as! String,
            "G_IdProyecto_Sede":  params.value(forKey: "G_IdProyecto_Sede")! as! Int,
            "G_IdUsuario_Creado":  params.value(forKey: "G_IdUsuario_Creado")! as! Int,
            "G_IdUsuario_Modifica":  params.value(forKey: "G_IdUsuario_Modifica")! as! Int,
            "F_EVENTO_IdTipoEvento":  params.value(forKey: "F_EVENTO_IdTipoEvento")! as! Int,
            "P_EVENTO_IdTipoEvento":  params.value(forKey: "F_EVENTO_IdTipoEvento")! as! Int,
            "F_IdEstato_Formato": 3,
            "Tipo_InformeP_F":  params.value(forKey: "Tipo_InformeP_F")! as! String,
            "Origen_Registro": "App",
            "UsuariosNotificados": notificadores,
            "G_DescripcionFormato": params.value(forKey: "G_DescripcionFormato")! as! String]
        
        print("parametros : \(parameters)")
        
        Alamofire.request(Constante.BASE_URL+"api/Incidente/Registro",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("value registar : \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func verRegistroIncidente(params: NSDictionary,callback: @escaping (NSDictionary?, NSError?) -> ()) {
        let parameters: [String: Any] = [
            "idIncidente": params.value(forKey: "idIncidente")! as! Int,
            "tipoInforme": params.value(forKey: "tipoInforme")! as! String]
        
        Alamofire.request(Constante.BASE_URL+"/api/Incidente/PostVerIncidente",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func editarRegistroIncidente(params: NSDictionary,notificar: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        let parameters: [String: Any] = [
            "Id_Incidente":params.value(forKey: "Id_Incidente")! as! Int,
            "Empleador_Tipo": params.value(forKey: "Empleador_Tipo")! as! String,
            "F_DESCRIPCION_Incidente": params.value(forKey: "F_DESCRIPCION_Incidente")! as! String,
            "F_Empleador_IdActividadEconomica": params.value(forKey: "F_Empleador_IdActividadEconomica")! as! Int,
            "F_Empleador_IdEmpresa": params.value(forKey: "F_Empleador_IdEmpresa")! as! Int,
            "F_Empleador_IdTamanioEmpresa": params.value(forKey: "F_Empleador_IdTamanioEmpresa")! as! Int,
            "F_EmpleadorI_IdActividadEconomica": params.value(forKey: "F_EmpleadorI_IdActividadEconomica")! as! Int,
            "F_EmpleadorI_RazonSocial": params.value(forKey: "F_EmpleadorI_RazonSocial")! as! String,
            "F_EmpleadorI_RUC": params.value(forKey: "F_EmpleadorI_RUC")! as! String,
            "F_Empleador_RazonSocial": params.value(forKey: "F_Empleador_RazonSocial")! as! String,
            "F_Empleador_RUC": params.value(forKey: "F_Empleador_RUC")! as! String,
            "F_EVENTO_FechaAccidente": params.value(forKey: "F_EVENTO_FechaAccidente")! as! String,
            "F_EVENTO_FechaInicioInvestigacion": params.value(forKey: "F_EVENTO_FechaInicioInvestigacion")! as! String,
            "F_EVENTO_HoraAccidente": params.value(forKey: "F_EVENTO_HoraAccidente")! as! String,
            "F_EVENTO_HuboDanioMaterial": params.value(forKey: "F_EVENTO_HuboDanioMaterial")! as! String,
            "F_EVENTO_IdEquipoAfectado": params.value(forKey: "F_EVENTO_IdEquipoAfectado")! as! Int,
            "F_EVENTO_IdParteAfectada": params.value(forKey: "F_EVENTO_IdParteAfectada")! as! Int,
            "F_EVENTO_LugarExacto":  params.value(forKey: "F_EVENTO_LugarExacto")! as! String,
            "F_EVENTO_NumTrabajadoresAfectadas":params.value(forKey: "F_EVENTO_NumTrabajadoresAfectadas")! as! String,
            "F_Trabajador_DetalleCategoriaOcupacional":  params.value(forKey: "F_Trabajador_DetalleCategoriaOcupacional")! as! String,
            "F_Trabajador_NombresApellidos":  params.value(forKey: "F_Trabajador_NombresApellidos")! as! String,
            "G_Fecha_Creado":  params.value(forKey: "G_Fecha_Creado")! as! String,
            "G_Fecha_Modifica":  params.value(forKey: "G_Fecha_Modifica")! as! String,
            "G_IdProyecto_Sede":  params.value(forKey: "G_IdProyecto_Sede")! as! Int,
            "G_IdUsuario_Creado":  params.value(forKey: "G_IdUsuario_Creado")! as! Int,
            "G_IdUsuario_Modifica":  params.value(forKey: "G_IdUsuario_Modifica")! as! Int,
            "F_EVENTO_IdTipoEvento":  params.value(forKey: "F_EVENTO_IdTipoEvento")! as! Int,
            "P_EVENTO_IdTipoEvento":  params.value(forKey: "F_EVENTO_IdTipoEvento")! as! Int,
            "F_IdEstato_Formato": 3,
            "Tipo_InformeP_F":  params.value(forKey: "Tipo_InformeP_F")! as! String,
            "Origen_Registro": "App",
            "UsuariosNotificados": notificadores,
            "G_DescripcionFormato": params.value(forKey: "G_DescripcionFormato")! as! String]
        
        print("parametros : \(parameters)")
        
        Alamofire.request(Constante.BASE_URL+"api/Incidente/Editar",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
}
