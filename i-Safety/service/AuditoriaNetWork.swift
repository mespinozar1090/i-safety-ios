//
//  AuditoriaNetWork.swift
//  i-Safety
//
//  Created by usuario on 7/10/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuditoriaNetWork {
    
    static let instance = AuditoriaNetWork()
      static var parent: UIViewController? = nil
      
      init() {
      }
      
      static func getInstance(parent: UIViewController) -> AuditoriaNetWork {
          if(self.parent == nil) {
              self.parent = parent
          }
          return self.instance
      }
    
    func listarInterventoria(idUsers: Int,callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {

        let parameters: [String: Int] = [
            "IdUsers": idUsers]
        
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/ListarAuditoria",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func listarInterventoriaV2(params:NSDictionary,callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {

        let parameters: [String: Int] = [
        "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
        "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
        "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/ListarAuditoriaFiltrosBandeja",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerLineaminetos(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/ListaLineamientosItems",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func listarIncidente(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Incidente/ListarRegistros",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerLineaminetosSub(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/ListaLineamientosSubItems",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func crearRegistroAuditoria(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        
        var evaluaciones: [[String:Any]] = []
        for index in 0...evaluacion.count-1 {
            var lstEvidenciaFoto: [String] = []
            if let imagen:String = evaluacion[index].value(forKey: "Imagen") as? String, !imagen.isEmpty{
               lstEvidenciaFoto.append(imagen)
            }
            
            let parametersEvaluaciones: [String: Any] = [
                "Id_Audit_Detalle": evaluacion[index].value(forKey: "Id_Audit_Detalle")! as! Int,
                "Id_Audit_Items": evaluacion[index].value(forKey: "Id_Audit_Items")! as! Int,
                "Id_Audit_Lin_SubItems": evaluacion[index].value(forKey: "Id_Audit_Lin_SubItems")! as! Int,
                "Calificacion": evaluacion[index].value(forKey: "Calificacion")! as! Int,
                "LstFoto": lstEvidenciaFoto,
                "Evidencia": "cas",
                "Lugar": evaluacion[index].value(forKey: "Lugar")! as! Int,
                "Notas": evaluacion[index].value(forKey: "Notas")! as! String,
                "TextEvidencia": evaluacion[index].value(forKey: "Notas")! as! String]
            evaluaciones.append(parametersEvaluaciones)
        }
           print("enviar parametros \(evaluaciones)")
        
        let parameters: [String: Any] = [
            "Id_Audit":params.value(forKey: "Id_Audit")! as! Int,
            "Estado_Audit": params.value(forKey: "Estado_Audit")! as! Int,
            "Fecha_Audit": params.value(forKey: "Fecha_Audit")! as! String,
            "Id_Empresa_contratista": params.value(forKey: "Id_Empresa_contratista")! as! Int,
            "IdUsers": params.value(forKey: "IdUsers")! as! Int,
            "LstDetalleAudit": evaluaciones,
            "Nombre_Formato": params.value(forKey: "Nombre_Formato")! as! String,
            "Nro_Contrato": params.value(forKey: "Nro_Contrato")! as! String,
            "Sede": params.value(forKey: "Sede")! as! Int,
            "Supervisor_Contrato": params.value(forKey: "Supervisor_Contrato")! as! String,
            "Responsable_Contratista": params.value(forKey: "Responsable_Contratista")! as! String,
            "Origen_Registro": "App",
            "UsuariosNotificados":notificar]
        
           print("enviar parametros \(params)")
        
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/RegistroAuditoria",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func editarRegistroAuditoria(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        
        var evaluaciones: [[String: Any]] = []
        for index in 0...evaluacion.count-1 {
            var lstEvidenciaFoto: [String] = []
            if let imagen:String = evaluacion[index].value(forKey: "Imagen") as? String, !imagen.isEmpty{
             //  let img: [String: Any] = ["Imagen": imagen]
                 lstEvidenciaFoto.append(imagen)
            }
            
            let parametersEvaluaciones: [String: Any] = [
                "Id_Audit_Detalle": evaluacion[index].value(forKey: "Id_Audit_Detalle")! as! Int,
                "Id_Audit_Items": evaluacion[index].value(forKey: "Id_Audit_Items")! as! Int,
                "Id_Audit_Lin_SubItems": evaluacion[index].value(forKey: "Id_Audit_Lin_SubItems")! as! Int,
                "Calificacion": evaluacion[index].value(forKey: "Calificacion")! as! Int,
                "LstFoto": lstEvidenciaFoto,
                "Evidencia": "cas",
                "Lugar": evaluacion[index].value(forKey: "Lugar")! as! Int,
                "Notas": evaluacion[index].value(forKey: "Notas")! as! String,
                "TextEvidencia": evaluacion[index].value(forKey: "Notas")! as! String]
            evaluaciones.append(parametersEvaluaciones)
        }
        
        
        let parameters: [String: Any] = [
            "Id_Audit":params.value(forKey: "Id_Audit")! as! Int,
            "Estado_Audit": params.value(forKey: "Estado_Audit")! as! Int,
            "Fecha_Audit": params.value(forKey: "Fecha_Audit")! as! String,
            "Id_Empresa_contratista": params.value(forKey: "Id_Empresa_contratista")! as! Int,
            "IdUsers": params.value(forKey: "IdUsers")! as! Int,
            "LstDetalleAudit": evaluaciones,
            "Nombre_Formato": params.value(forKey: "Nombre_Formato")! as! String,
            "Nro_Contrato": params.value(forKey: "Nro_Contrato")! as! String,
            "Sede": params.value(forKey: "Sede")! as! Int,
            "Supervisor_Contrato": params.value(forKey: "Supervisor_Contrato")! as! String,
            "Responsable_Contratista": params.value(forKey: "Responsable_Contratista")! as! String,
            "Origen_Registro": "App",
            "UsuariosNotificados":notificar]
        
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/EditarAuditoria",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerRegistroAuditoria(idAuditoria:Int,callback: @escaping (NSDictionary?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Auditoria/GetObtenerRegistroAuditoria?Id=\(idAuditoria)",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("detalle auditoria : \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
}
