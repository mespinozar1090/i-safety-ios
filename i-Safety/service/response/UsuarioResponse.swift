//
//  UsuarioResponse.swift
//  i-Safety
//
//  Created by usuario on 8/5/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class UsuarioResponse{
    var idPerfilUsuario:Int?
    var nombreUsuario:String?
    var apellidoUsuario:String?
}
