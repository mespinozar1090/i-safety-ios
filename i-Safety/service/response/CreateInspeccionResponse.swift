//
//  CreateInspeccionResponse.swift
//  i-Safety
//
//  Created by usuario on 8/5/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation

class CreateInspeccionResponse {
    var idInspecciones:Int?
    var codigoInspecion:String?
    var proyecto:String?
}
