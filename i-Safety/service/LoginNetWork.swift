//
//  LoginNetWork.swift
//  i-Safety
//
//  Created by usuario on 6/30/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import Alamofire

class LoginNetWork{
    
    static let instance = LoginNetWork()
    static var parent: UIViewController? = nil
    
    init() {
    }
    
    static func getInstance(parent: UIViewController) -> LoginNetWork {
        if(self.parent == nil) {
            self.parent = parent
        }
        return self.instance
    }
    
    func loginAcceso(params: NSDictionary, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let parameters: [String: String] = [
            "User": params.value(forKey: "User")! as! String,
            "Pass": params.value(forKey: "Pass")! as! String]
        
        Alamofire.request(Constante.BASE_URL+"api/Login/Acceso",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
        
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
}
