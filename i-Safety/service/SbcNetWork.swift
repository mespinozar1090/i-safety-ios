//
//  SbcNetWork.swift
//  i-Safety
//
//  Created by usuario on 7/8/20.
//  Copyright © 2020 MDP Consulting. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class SbcNetWork {
    
    static let instance = SbcNetWork()
    static var parent: UIViewController? = nil
    
    init() {
    }
    
    static func getInstance(parent: UIViewController) -> SbcNetWork {
        if(self.parent == nil) {
            self.parent = parent
        }
        return self.instance
    }
    
    //MARK: - Servicios para datos generales
    func listarSBC(params: NSDictionary,callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        
        let parameters: [String: Int] = [
            "Id_Usuario": params.value(forKey: "Id_Usuario")! as! Int,
            "Id_Rol_Usuario": params.value(forKey: "Id_Rol_Usuario")! as! Int,
            "Id_Rol_General": params.value(forKey: "Id_Rol_General")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/SBC/ListarSBCFiltrosBandeja",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func eliminarSBC(params: NSDictionary,callback: @escaping (Bool?, NSError?) -> ()) {
        let parameters: [String: Int] = [
            "idSBC": params.value(forKey: "idSBC")! as! Int]
        
        Alamofire.request(Constante.BASE_URL+"api/SBC/borrarSBC",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("result: \(response.result.value as? Bool)")
            switch response.result {
            case .success:
                callback(response.result.value as? Bool, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerSede(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/Sede",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerEmpresaSBSResponse(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/SBC/ListaEmpresas",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerEspecialidad(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/SBC/ListaEspecialidades",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func obtenerAreaInspeccion(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/Inspeccion/ListarAreaInspeccionada",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    //MARK: - Servicios para datos evaluaciones
    func obtenerCategoriaSBC(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/SBC/ListaCategoria",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerCategoriaItemSBC(callback: @escaping (Array<NSDictionary>?, NSError?) -> ()) {
        Alamofire.request(Constante.BASE_URL+"api/SBC/ListaItemcategoria",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success:
                callback(response.result.value as? Array<NSDictionary>, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func crearRegistroSBC(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        var evaluaciones: [[String: Any]] = []
        for index in 0...evaluacion.count-1 {
            let parametersEvaluaciones: [String: Any] = [
                "Id_sbc_detalle": evaluacion[index].value(forKey: "Id_sbc_detalle")! as! Int,
                "Id_sbc_Categoria": evaluacion[index].value(forKey: "Id_sbc_Categoria")! as! Int,
                "Id_sbc_Categoria_Items": evaluacion[index].value(forKey: "Id_sbc_Categoria_Items")! as! Int,
                "Seguro": evaluacion[index].value(forKey: "Seguro")! as! Bool,
                "Riesgoso": evaluacion[index].value(forKey: "Riesgoso")! as! Bool,
                "Idbarrera": evaluacion[index].value(forKey: "Idbarrera")! as! Int,
                "Observacion_sbc_detalle": evaluacion[index].value(forKey: "Observacion_sbc_detalle")! as! String,
                "Id_sbc": evaluacion[index].value(forKey: "Id_sbc")! as! Int]
            evaluaciones.append(parametersEvaluaciones)
        }
        
        let parameters: [String: Any] = [
            "Id_sbc":params.value(forKey: "Id_sbc")! as! Int,
            "Nombre_observador": params.value(forKey: "Nombre_observador")! as! String,
            "IdSede_Proyecto": params.value(forKey: "IdSede_Proyecto")! as! Int,
            "Cargo_Observador": params.value(forKey: "Cargo_Observador")! as! String,
            "IdLugar_trabajo": params.value(forKey: "IdLugar_trabajo")! as! Int,
            "Fecha_Registro_SBC": params.value(forKey: "Fecha_Registro_SBC")! as! String,
            "Id_Empresa_Observadora": params.value(forKey: "Id_Empresa_Observadora")! as! Int,
            "Horario_Observacion": params.value(forKey: "Horario_Observacion")! as! String,
            "Tiempo_exp_observada": params.value(forKey: "Tiempo_exp_observada")! as! Int,
            "Especialidad_Observado": params.value(forKey: "Especialidad_Observado")! as! Int,
            "Actividad_Observada": params.value(forKey: "Actividad_Observada")! as! String,
            "Id_Area_Trabajo": params.value(forKey: "Id_Area_Trabajo")! as! Int,
            "Fecha_registro": params.value(forKey: "Fecha_registro")! as! String,
            "Descripcion_Area_Observada": params.value(forKey: "Descripcion_Area_Observada")! as! String,
            "lstDetalleSBC": evaluaciones,
            "IdSede_Proyecto_Observado": params.value(forKey: "IdSede_Proyecto_Observado")! as! Int,
            "IdUsuario": params.value(forKey: "IdUsuario")! as! Int,
            "Tipo_Registro": 2,
            "Origen_Registro": "App",
            "UsuariosNotificados": notificadores,
            "NombreFormato": params.value(forKey: "NombreFormato")! as! String]
        
        print("parametros : \(parameters)")
        
        Alamofire.request(Constante.BASE_URL+"api/SBC/RegistroSBC",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("result: \(response.result.value as? NSDictionary)")
            switch response.result {
                
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print("error: \(error as NSError)")
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    func obtenerRegistroSBC(idSBC: Int, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        Alamofire.request(Constante.BASE_URL+"api/SBC/GetObtenerRegistroSBC?Id=\(idSBC)",method: .get,encoding: JSONEncoding.default).validate().responseJSON { response in
            print("result : \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                print(error)
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    func editarRegistroSBC(params: NSDictionary,notificar: Array<NSDictionary>,evaluacion: Array<NSDictionary>, callback: @escaping (NSDictionary?, NSError?) -> ()) {
        
        var notificadores: [[String: Any]] = []
        for index in 0...notificar.count-1 {
            let parameterNotificar: [String: Any] = [
                "IdUser": notificar[index].value(forKey: "IdUser")! as! Int,
                "Nombre": notificar[index].value(forKey: "Nombre")! as! String,
                "Email": notificar[index].value(forKey: "Email")! as! String]
            notificadores.append(parameterNotificar)
        }
        
        var evaluaciones: [[String: Any]] = []
        for index in 0...evaluacion.count-1 {
            let parametersEvaluaciones: [String: Any] = [
                "Id_sbc_detalle": evaluacion[index].value(forKey: "Id_sbc_detalle")! as! Int,
                "Id_sbc_Categoria": evaluacion[index].value(forKey: "Id_sbc_Categoria")! as! Int,
                "Id_sbc_Categoria_Items": evaluacion[index].value(forKey: "Id_sbc_Categoria_Items")! as! Int,
                "Seguro": evaluacion[index].value(forKey: "Seguro")! as! Bool,
                "Riesgoso": evaluacion[index].value(forKey: "Riesgoso")! as! Bool,
                "Idbarrera": evaluacion[index].value(forKey: "Idbarrera")! as! Int,
                "Observacion_sbc_detalle": evaluacion[index].value(forKey: "Observacion_sbc_detalle")! as! String,
                "Id_sbc": evaluacion[index].value(forKey: "Id_sbc")! as! Int]
            evaluaciones.append(parametersEvaluaciones)
        }
        
        let parameters: [String: Any] = [
            "Id_sbc":params.value(forKey: "Id_sbc")! as! Int,
            "Nombre_observador": params.value(forKey: "Nombre_observador")! as! String,
            "IdSede_Proyecto": params.value(forKey: "IdSede_Proyecto")! as! Int,
            "Cargo_Observador": params.value(forKey: "Cargo_Observador")! as! String,
            "IdLugar_trabajo": params.value(forKey: "IdLugar_trabajo")! as! Int,
            "Fecha_Registro_SBC": params.value(forKey: "Fecha_Registro_SBC")! as! String,
            "Id_Empresa_Observadora": params.value(forKey: "Id_Empresa_Observadora")! as! Int,
            "Horario_Observacion": params.value(forKey: "Horario_Observacion")! as! String,
            "Tiempo_exp_observada": params.value(forKey: "Tiempo_exp_observada")! as! Int,
            "Especialidad_Observado": params.value(forKey: "Especialidad_Observado")! as! Int,
            "Actividad_Observada": params.value(forKey: "Actividad_Observada")! as! String,
            "Id_Area_Trabajo": params.value(forKey: "Id_Area_Trabajo")! as! Int,
            "Fecha_registro": params.value(forKey: "Fecha_registro")! as! String,
            "Descripcion_Area_Observada": params.value(forKey: "Descripcion_Area_Observada")! as! String,
            "lstDetalleSBC": evaluaciones,
            "IdSede_Proyecto_Observado": params.value(forKey: "IdSede_Proyecto_Observado")! as! Int,
            "IdUsuario": params.value(forKey: "IdUsuario")! as! Int,
            "Tipo_Registro": 2,
            "Origen_Registro": "App",
            "UsuariosNotificados": notificadores,
            "NombreFormato": params.value(forKey: "NombreFormato")! as! String]
        
        
        print("parameters : ",parameters)
        Alamofire.request(Constante.BASE_URL+"api/SBC/EditarSBC",method: .post,parameters: parameters,encoding: JSONEncoding.default).validate().responseJSON { response in
            
            print("result data : \(response.result.value as? NSDictionary)")
            switch response.result {
            case .success:
                callback(response.result.value as? NSDictionary, nil)
                break
            case .failure(let error):
                callback(nil, error as NSError?)
                break
            }
        }
    }
    
    
    
}
